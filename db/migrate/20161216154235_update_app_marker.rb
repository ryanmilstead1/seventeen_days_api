class UpdateAppMarker < ActiveRecord::Migration[5.0]
  def change
    add_column :app_markers, :marker_type, :string
    add_column :app_markers, :detail, :string
    add_column :viewer_logs, :app_type, :string
  end
end
