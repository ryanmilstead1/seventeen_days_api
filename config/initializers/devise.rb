
module Devise
  Devise.setup do |config|
    config.secret_key = ENV['DEVISE_SECRET_KEY'] if Rails.env.production?
    config.mailer_sender = 'Seventeen Days <info@seventeendays.org>'
  end

  class Mapping
    def self.find_by_path!(path, path_type=:fullpath)
      Devise.mappings.each_value do |m|
        if %w(facebook instagram twitter).any? {|oauth_type| path.include?(oauth_type)}
          return m
        else
          return m if path.include?(m.send(path_type))
        end
      end
      raise "Could not find a valid mapping for path #{path.inspect}"
    end
  end
end
