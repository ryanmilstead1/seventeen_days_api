Rails.application.config.middleware.use OmniAuth::Builder do
  provider :instagram,
    Figaro.env.instagram_api_key,
    Figaro.env.instagram_api_secret,
    scope: 'basic',
    callback_url: Figaro.env.instagram_callback_url
  provider :facebook,
    Figaro.env.facebook_api_key,
    Figaro.env.facebook_api_secret,
    scope: 'email,public_profile',
    callback_url: Figaro.env.facebook_callback_url
  provider :twitter,
    Figaro.env.twitter_api_key,
    Figaro.env.twitter_api_secret,
    scope: 'email,profile',
    callback_url: Figaro.env.twitter_callback_url
end
