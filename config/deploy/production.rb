

server 'ec2-52-35-252-74.us-west-2.compute.amazonaws.com',
  user: 'deploy',
  keys: %w(bin/seventeen_days_api.pem),
  roles: %w{web app db},
  ssh_options: {
    keys: %w(bin/seventeen_days_api.pem),
    forward_agent: false,
    auth_methods: %w(publickey)
  }