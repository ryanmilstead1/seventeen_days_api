Rails.application.routes.draw do

   mount_devise_token_auth_for 'User', at: 'api/auth',
      controllers: {
        sessions: 'sessions',
        omniauth_callbacks: 'omniauth_callbacks',
        token_validations:  'token_validations',
        registrations: 'registrations',
        passwords: 'passwords'
      },
      defaults: { format: 'json' }


  scope '/api' do
    resources :users, only: [:show]
    resources :viewer_logs, only: [:index, :create, :show]
    resources :activation_codes, only: [:show, :create]
    resources :surveys, only: [:show]
    resources :responses, only: [:create]
  end

end
