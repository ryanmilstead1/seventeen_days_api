# config valid only for current version of Capistrano
lock '3.6.0'

set :stages,          %w{remote_development staging production}
set :default_stage, 'remote_development'
set :application,     'seventeen_days_api'
set :repo_url,        'git@bitbucket.org:ryanmilstead1/seventeen_days_api.git'
set :user,            'deploy'
set :pty,             true
set :deploy_to,       "/seventeen_days/api"
set :linked_files,    %w{config/database.yml config/application.yml config/secrets.yml}
set :linked_dirs,     %w{log tmp/pids tmp/sockets tmp/cache }


namespace :deploy do

  desc 'Make sure local git is in sync with remote.'
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts 'WARNING: HEAD is not the same as origin/master'
        puts 'Run `git push` to sync changes.'
        exit
      end
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do

    end
  end

  before :starting,     :check_revision
  after  :finishing,    :cleanup
  after  :finishing,    :restart
end
