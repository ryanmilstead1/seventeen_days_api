-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: bitnami_dreamfactory
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app`
--
CREATE DATABASE IF NOT EXISTS dreamfactory;
use dreamfactory;

DROP TABLE IF EXISTS `app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `api_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `type` int(10) unsigned NOT NULL DEFAULT '0',
  `path` text COLLATE utf8_unicode_ci,
  `url` text COLLATE utf8_unicode_ci,
  `storage_service_id` int(10) unsigned DEFAULT NULL,
  `storage_container` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requires_fullscreen` tinyint(1) NOT NULL DEFAULT '0',
  `allow_fullscreen_toggle` tinyint(1) NOT NULL DEFAULT '1',
  `toggle_location` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'top',
  `role_id` int(10) unsigned DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_name_unique` (`name`),
  KEY `app_storage_service_id_foreign` (`storage_service_id`),
  KEY `app_role_id_foreign` (`role_id`),
  KEY `app_created_by_id_foreign` (`created_by_id`),
  KEY `app_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `app_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `app_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `app_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE SET NULL,
  CONSTRAINT `app_storage_service_id_foreign` FOREIGN KEY (`storage_service_id`) REFERENCES `service` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app`
--

LOCK TABLES `app` WRITE;
/*!40000 ALTER TABLE `app` DISABLE KEYS */;
INSERT INTO `app` VALUES (1,'admin','6498a8ad1beb9d84d63035c5d1120c007fad6de706734db9689f8996707e0f7d','An application for administering this instance.',1,3,'dreamfactory/dist/index.html',NULL,NULL,NULL,0,1,'top',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL),(2,'swagger','36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88','A Swagger-base application allowing viewing and testing API documentation.',1,3,'swagger/index.html',NULL,NULL,NULL,0,1,'top',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL),(3,'filemanager','b5cb82af7b5d4130f36149f90aa2746782e59a872ac70454ac188743cb55b0ba','An application for managing file services.',1,3,'filemanager/index.html',NULL,NULL,NULL,0,1,'top',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL),(4,'seventeendaysreg','efa184b711b4f54ad818534abb21a12cf4523803a840380d7c8af0d3edfa9d10','Seventeen Days',1,0,NULL,NULL,NULL,NULL,0,1,'top',1,'2015-11-13 20:43:52','2015-11-13 20:43:52',1,NULL),(6,'Address Book for AngularJS','89fe30820d6f9e5fbd5021e30770af13964cacadee48dfe9af1e08c6492a837c','An address book app for AngularJS showing user registration, user login, and CRUD.',1,0,'/add_angular/index.html','index.html',3,'AddressBookForAngularJS',0,1,'top',1,'2015-11-13 22:04:47','2015-11-13 22:17:42',1,1);
/*!40000 ALTER TABLE `app` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_group`
--

DROP TABLE IF EXISTS `app_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_group_name_unique` (`name`),
  KEY `app_group_created_by_id_foreign` (`created_by_id`),
  KEY `app_group_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `app_group_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `app_group_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_group`
--

LOCK TABLES `app_group` WRITE;
/*!40000 ALTER TABLE `app_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_lookup`
--

DROP TABLE IF EXISTS `app_lookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_lookup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_lookup_app_id_foreign` (`app_id`),
  KEY `app_lookup_created_by_id_foreign` (`created_by_id`),
  KEY `app_lookup_last_modified_by_id_foreign` (`last_modified_by_id`),
  KEY `app_lookup_name_index` (`name`),
  CONSTRAINT `app_lookup_app_id_foreign` FOREIGN KEY (`app_id`) REFERENCES `app` (`id`) ON DELETE CASCADE,
  CONSTRAINT `app_lookup_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `app_lookup_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_lookup`
--

LOCK TABLES `app_lookup` WRITE;
/*!40000 ALTER TABLE `app_lookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_lookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_to_app_group`
--

DROP TABLE IF EXISTS `app_to_app_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_to_app_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `app_to_app_group_app_id_foreign` (`app_id`),
  KEY `app_to_app_group_group_id_foreign` (`group_id`),
  CONSTRAINT `app_to_app_group_app_id_foreign` FOREIGN KEY (`app_id`) REFERENCES `app` (`id`) ON DELETE CASCADE,
  CONSTRAINT `app_to_app_group_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `app_group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_to_app_group`
--

LOCK TABLES `app_to_app_group` WRITE;
/*!40000 ALTER TABLE `app_to_app_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_to_app_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aws_config`
--

DROP TABLE IF EXISTS `aws_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aws_config` (
  `service_id` int(10) unsigned NOT NULL,
  `key` longtext COLLATE utf8_unicode_ci,
  `secret` longtext COLLATE utf8_unicode_ci,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`service_id`),
  CONSTRAINT `aws_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aws_config`
--

LOCK TABLES `aws_config` WRITE;
/*!40000 ALTER TABLE `aws_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `aws_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cloud_email_config`
--

DROP TABLE IF EXISTS `cloud_email_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cloud_email_config` (
  `service_id` int(10) unsigned NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`service_id`),
  CONSTRAINT `cloud_email_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cloud_email_config`
--

LOCK TABLES `cloud_email_config` WRITE;
/*!40000 ALTER TABLE `cloud_email_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `cloud_email_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cors_config`
--

DROP TABLE IF EXISTS `cors_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cors_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `origin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `header` longtext COLLATE utf8_unicode_ci NOT NULL,
  `method` int(11) NOT NULL DEFAULT '0',
  `max_age` int(11) NOT NULL DEFAULT '3600',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cors_config_path_unique` (`path`),
  KEY `cors_config_created_by_id_foreign` (`created_by_id`),
  KEY `cors_config_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `cors_config_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `cors_config_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cors_config`
--

LOCK TABLES `cors_config` WRITE;
/*!40000 ALTER TABLE `cors_config` DISABLE KEYS */;
INSERT INTO `cors_config` VALUES (1,'*','*','*',31,0,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',1,1);
/*!40000 ALTER TABLE `cors_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `couchdb_config`
--

DROP TABLE IF EXISTS `couchdb_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `couchdb_config` (
  `service_id` int(10) unsigned NOT NULL,
  `dsn` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `options` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`service_id`),
  CONSTRAINT `couchdb_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `couchdb_config`
--

LOCK TABLES `couchdb_config` WRITE;
/*!40000 ALTER TABLE `couchdb_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `couchdb_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_field_extras`
--

DROP TABLE IF EXISTS `db_field_extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_field_extras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(10) unsigned DEFAULT NULL,
  `table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extra_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `picklist` text COLLATE utf8_unicode_ci,
  `validation` text COLLATE utf8_unicode_ci,
  `client_info` text COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_field_extras_service_id_foreign` (`service_id`),
  KEY `db_field_extras_created_by_id_foreign` (`created_by_id`),
  KEY `db_field_extras_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `db_field_extras_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `db_field_extras_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `db_field_extras_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_field_extras`
--

LOCK TABLES `db_field_extras` WRITE;
/*!40000 ALTER TABLE `db_field_extras` DISABLE KEYS */;
INSERT INTO `db_field_extras` VALUES (1,1,'sql_db_config','dsn','Connection String (DSN)',NULL,'Specify the connection string for the database you\'re connecting to.',NULL,NULL,NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(2,1,'sql_db_config','options','Driver Options',NULL,'A key=>value array of driver-specific connection options.',NULL,NULL,NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(3,1,'sql_db_config','attributes','Driver Attributes',NULL,'A key=>value array of driver-specific attributes.',NULL,NULL,NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(4,4,'contact','id','Contact Id',NULL,NULL,NULL,NULL,NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(5,4,'contact','first_name',NULL,NULL,NULL,NULL,'{\"not_empty\":{\"on_fail\":\"First name value must not be empty.\"}}',NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(6,4,'contact','last_name',NULL,NULL,NULL,NULL,'{\"not_empty\":{\"on_fail\":\"Last name value must not be empty.\"}}',NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(7,4,'contact','image_url','image_url',NULL,NULL,NULL,'{\"url\":{\"on_fail\":\"Not a valid URL value.\"}}',NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(8,4,'contact','twitter','Twitter Handle',NULL,NULL,NULL,NULL,NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(9,4,'contact','skype','Skype Account',NULL,NULL,NULL,NULL,NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(10,4,'contact','notes','notes',NULL,NULL,NULL,NULL,NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(11,4,'contact_info','id','Info Id',NULL,NULL,NULL,NULL,NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(12,4,'contact_info','info_type',NULL,NULL,NULL,'[\"work\",\"home\",\"mobile\",\"other\"]','{\"not_empty\":{\"on_fail\":\"Information type can not be empty.\"},\"picklist\":{\"on_fail\":\"Not a valid information type.\"}}',NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(13,4,'contact_info','phone','Phone Number',NULL,NULL,NULL,NULL,NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(14,4,'contact_info','email','Email Address',NULL,NULL,NULL,'{\"email\":{\"on_fail\":\"Not a valid email address.\"}}',NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(15,4,'contact_info','address','Street Address',NULL,NULL,NULL,NULL,NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(16,4,'contact_info','city','city',NULL,NULL,NULL,NULL,NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(17,4,'contact_info','state','state',NULL,NULL,NULL,NULL,NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(18,4,'contact_info','zip','zip',NULL,NULL,NULL,NULL,NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(19,4,'contact_info','country','country',NULL,NULL,NULL,NULL,NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(20,4,'contact_group','name',NULL,NULL,NULL,NULL,'{\"not_empty\":{\"on_fail\":\"Group name value must not be empty.\"}}',NULL,'2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(23,9,'lessons','name','Name',NULL,NULL,NULL,NULL,NULL,'2016-03-24 18:27:20','2016-03-24 18:27:20',NULL,NULL,NULL),(24,9,'lessons','access_code','Access Code',NULL,NULL,NULL,NULL,NULL,'2016-03-29 01:39:26','2016-03-29 01:39:26',NULL,NULL,NULL);
/*!40000 ALTER TABLE `db_field_extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_table_extras`
--

DROP TABLE IF EXISTS `db_table_extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_table_extras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(10) unsigned NOT NULL,
  `table` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plural` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_field` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_table_extras_service_id_foreign` (`service_id`),
  KEY `db_table_extras_created_by_id_foreign` (`created_by_id`),
  KEY `db_table_extras_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `db_table_extras_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `db_table_extras_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `db_table_extras_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_table_extras`
--

LOCK TABLES `db_table_extras` WRITE;
/*!40000 ALTER TABLE `db_table_extras` DISABLE KEYS */;
INSERT INTO `db_table_extras` VALUES (1,1,'user',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\User',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(2,1,'user_lookup',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\UserLookup',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(3,1,'user_to_app_to_role',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\UserAppRole',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(4,1,'service',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\Service',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(5,1,'service_type',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\ServiceType',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(6,1,'service_doc',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\ServiceDoc',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(7,1,'role',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\Role',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(8,1,'role_service_access',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\RoleServiceAccess',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(9,1,'role_lookup',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\RoleLookup',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(10,1,'app',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\App',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(11,1,'app_lookup',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\AppLookup',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(12,1,'app_group',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\AppGroup',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(13,1,'app_to_app_group',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\AppToAppGroup',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(14,1,'system_resource',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\SystemResource',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(15,1,'script_type',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\ScriptType',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(16,1,'event_script',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\EventScript',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(17,1,'event_subscriber',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\EventSubscriber',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(18,1,'email_template',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\EmailTemplate',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(19,1,'system_setting',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\Setting',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(20,1,'system_lookup',NULL,NULL,NULL,'DreamFactory\\Core\\Models\\Lookup',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(21,1,'sql_db_config',NULL,NULL,NULL,'DreamFactory\\Core\\SqlDb\\Models\\SqlDbConfig',NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL,NULL),(22,4,'contact',NULL,NULL,NULL,NULL,'The main table for tracking contacts.','2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(23,4,'contact_info',NULL,NULL,NULL,NULL,'The contact details sub-table, owned by contact table row.','2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(24,4,'contact_group',NULL,NULL,NULL,NULL,'The main table for tracking groups of contact.','2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(25,4,'contact_group_relationship',NULL,NULL,NULL,NULL,'The join table for tracking contacts in groups.','2015-11-13 21:04:05','2015-11-13 21:04:05',NULL,NULL,NULL),(27,9,'lessons','Lessons','Lessons',NULL,NULL,NULL,'2016-03-21 20:34:00','2016-03-21 20:34:00',NULL,NULL,NULL),(28,9,'users','Users','Users',NULL,NULL,NULL,'2016-05-10 18:39:44','2016-05-10 18:39:44',NULL,NULL,NULL);
/*!40000 ALTER TABLE `db_table_extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_parameters_config`
--

DROP TABLE IF EXISTS `email_parameters_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_parameters_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `email_parameters_config_service_id_foreign` (`service_id`),
  CONSTRAINT `email_parameters_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_parameters_config`
--

LOCK TABLES `email_parameters_config` WRITE;
/*!40000 ALTER TABLE `email_parameters_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_parameters_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_template`
--

DROP TABLE IF EXISTS `email_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to` text COLLATE utf8_unicode_ci,
  `cc` text COLLATE utf8_unicode_ci,
  `bcc` text COLLATE utf8_unicode_ci,
  `subject` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body_text` text COLLATE utf8_unicode_ci,
  `body_html` text COLLATE utf8_unicode_ci,
  `from_name` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reply_to_name` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reply_to_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defaults` text COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_template_name_unique` (`name`),
  KEY `email_template_created_by_id_foreign` (`created_by_id`),
  KEY `email_template_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `email_template_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `email_template_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_template`
--

LOCK TABLES `email_template` WRITE;
/*!40000 ALTER TABLE `email_template` DISABLE KEYS */;
INSERT INTO `email_template` VALUES (1,'User Invite Default','Email sent to invite new users to your DreamFactory instance.',NULL,NULL,NULL,'[DF] New User Invitation',NULL,'emails.invite','DO NOT REPLY','no-reply@dreamfactory.com',NULL,NULL,NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL),(2,'User Registration Default','Email sent to new users to complete registration.',NULL,NULL,NULL,'[DF] Registration Confirmation',NULL,'emails.register','DO NOT REPLY','no-reply@dreamfactory.com',NULL,NULL,NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL),(3,'Password Reset Default','Email sent to users following a request to reset their password.',NULL,NULL,NULL,'[DF] Password Reset',NULL,'emails.password','DO NOT REPLY','no-reply@dreamfactory.com',NULL,NULL,NULL,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL);
/*!40000 ALTER TABLE `email_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_script`
--

DROP TABLE IF EXISTS `event_script`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_script` (
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `affects_process` tinyint(1) NOT NULL DEFAULT '0',
  `content` text COLLATE utf8_unicode_ci,
  `config` text COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `event_script_type_foreign` (`type`),
  KEY `event_script_created_by_id_foreign` (`created_by_id`),
  KEY `event_script_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `event_script_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `event_script_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `event_script_type_foreign` FOREIGN KEY (`type`) REFERENCES `script_type` (`name`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_script`
--

LOCK TABLES `event_script` WRITE;
/*!40000 ALTER TABLE `event_script` DISABLE KEYS */;
INSERT INTO `event_script` VALUES ('user.register.post.pre_process','v8js',1,1,'var_dump(event.request); // outputs to file in storage/log of dreamfactory install directory\r\n\r\nvar lodash = require(\"lodash.min.js\");\r\n\r\nvar record = event.request.payload;\r\n\r\nif (record.oauth_token) {\r\n    //Facebook Access Token validation\r\n    //Wyss\r\n    // var _fb_app_id = \'986792924696317\';\r\n    // var _fb_app_secret = \'c3c00fcf73f023f7170949ca886dd8c0\';\r\n    \r\n    //Duff\r\n    var _fb_app_id = \'1505093779789265\';\r\n    var _fb_app_secret = \'f63b48e516d05eea109bd7c5f7801aff\';\r\n\r\n    var _fb_acc_token_validation_result = platform.api.get(\'https://graph.facebook.com/v2.2/debug_token?input_token=\' + record.oauth_token + \'&access_token=\' + _fb_app_id + \'|\' + _fb_app_secret);\r\n    if (!_fb_acc_token_validation_result.content.data || !_fb_acc_token_validation_result.content.data.is_valid) {\r\n        throw \'Facebook Access Token is invalid\';\r\n    }\r\n    \r\n    var _old_user_find_query = \"system/user?filter=email%3D\'\" + record.email + \"\'\";\r\n    var _old_user_result = platform.api.get(_old_user_find_query);\r\n    \r\n    if (_old_user_result.content.resource && _old_user_result.content.resource.length > 0) {\r\n        platform.api.delete(\"system/user?ids=\" + _old_user_result.content.resource[0].id);\r\n    }\r\n    \r\n    record.password = record.oauth_token;\r\n}',NULL,'2015-11-24 11:48:01','2015-12-03 07:32:20',5,5);
/*!40000 ALTER TABLE `event_script` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_subscriber`
--

DROP TABLE IF EXISTS `event_subscriber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_subscriber` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `config` text COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `event_subscriber_name_unique` (`name`),
  KEY `event_subscriber_created_by_id_foreign` (`created_by_id`),
  KEY `event_subscriber_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `event_subscriber_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `event_subscriber_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_subscriber`
--

LOCK TABLES `event_subscriber` WRITE;
/*!40000 ALTER TABLE `event_subscriber` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_subscriber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_service_config`
--

DROP TABLE IF EXISTS `file_service_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_service_config` (
  `service_id` int(10) unsigned NOT NULL,
  `public_path` text COLLATE utf8_unicode_ci,
  `container` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`service_id`),
  CONSTRAINT `file_service_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_service_config`
--

LOCK TABLES `file_service_config` WRITE;
/*!40000 ALTER TABLE `file_service_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_service_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ldap_config`
--

DROP TABLE IF EXISTS `ldap_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ldap_config` (
  `service_id` int(10) unsigned NOT NULL,
  `default_role` int(10) unsigned NOT NULL,
  `host` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `base_dn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_suffix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`service_id`),
  KEY `ldap_config_default_role_index` (`default_role`),
  CONSTRAINT `ldap_config_default_role_foreign` FOREIGN KEY (`default_role`) REFERENCES `role` (`id`),
  CONSTRAINT `ldap_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ldap_config`
--

LOCK TABLES `ldap_config` WRITE;
/*!40000 ALTER TABLE `ldap_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `ldap_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2015_01_27_190908_create_system_tables',1),('2015_02_03_161456_create_sqldb_tables',1),('2015_02_03_161457_create_couchdb_tables',1),('2015_02_03_161457_create_mongodb_tables',1),('2015_02_03_161457_create_salesforce_tables',1),('2015_03_10_135522_create_aws_tables',1),('2015_03_11_143913_create_rackspace_tables',1),('2015_03_20_205504_create_remote_web_service_tables',1),('2015_05_02_134911_update_user_table_for_oauth_support',1),('2015_05_04_034605_create_adldap_tables',1),('2015_05_21_190727_create_user_config_table',1),('2015_07_10_161839_create_user_custom_table',1),('2015_08_24_180219_default_schema_only',1),('2015_08_25_202632_db_alias',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mongodb_config`
--

DROP TABLE IF EXISTS `mongodb_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mongodb_config` (
  `service_id` int(10) unsigned NOT NULL,
  `dsn` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `options` text COLLATE utf8_unicode_ci,
  `driver_options` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`service_id`),
  CONSTRAINT `mongodb_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mongodb_config`
--

LOCK TABLES `mongodb_config` WRITE;
/*!40000 ALTER TABLE `mongodb_config` DISABLE KEYS */;
INSERT INTO `mongodb_config` VALUES (8,'mongodb://localhost:27017/bitnami_df','{\"username\":\"bn_df\",\"password\":\"81b13a9880\"}',NULL);
/*!40000 ALTER TABLE `mongodb_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_config`
--

DROP TABLE IF EXISTS `oauth_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_config` (
  `service_id` int(10) unsigned NOT NULL,
  `default_role` int(10) unsigned NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_secret` longtext COLLATE utf8_unicode_ci NOT NULL,
  `redirect_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`service_id`),
  KEY `oauth_config_default_role_index` (`default_role`),
  CONSTRAINT `oauth_config_default_role_foreign` FOREIGN KEY (`default_role`) REFERENCES `role` (`id`),
  CONSTRAINT `oauth_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_config`
--

LOCK TABLES `oauth_config` WRITE;
/*!40000 ALTER TABLE `oauth_config` DISABLE KEYS */;
INSERT INTO `oauth_config` VALUES (10,1,'1505093779789265','eyJpdiI6IlZCNk1kUExlYlpnYjlPU0xcL1FSeUpBPT0iLCJ2YWx1ZSI6InZGXC9tbnlENjJaWGsyamcwXC9lXC94WldNZTRXcCs0VDlzb0JVbzV3NDFhVzA5TjdFODNCd0hYdmxFQTdHaDhlQnkiLCJtYWMiOiJkYWE4ZTVlYzhlNzU0MzM3ZGU2NjA3NWI0OGYwYmM3ZGM5NmQ4Y2FiYjJiOWEzNTc1YzhkNDU4OTU0MDQzMjdhIn0=','http://dreamfactory.ybclients.com:8080/dreamfactory/dist/?service=facebook_auth','test');
/*!40000 ALTER TABLE `oauth_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rackspace_config`
--

DROP TABLE IF EXISTS `rackspace_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rackspace_config` (
  `service_id` int(10) unsigned NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` longtext COLLATE utf8_unicode_ci,
  `tenant_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_key` longtext COLLATE utf8_unicode_ci,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `storage_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`service_id`),
  CONSTRAINT `rackspace_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rackspace_config`
--

LOCK TABLES `rackspace_config` WRITE;
/*!40000 ALTER TABLE `rackspace_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `rackspace_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_name_unique` (`name`),
  KEY `role_created_by_id_foreign` (`created_by_id`),
  KEY `role_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `role_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `role_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'student','Student',1,'2015-11-13 20:42:01','2015-11-25 01:03:20',1,5);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_lookup`
--

DROP TABLE IF EXISTS `role_lookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_lookup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_lookup_role_id_foreign` (`role_id`),
  KEY `role_lookup_created_by_id_foreign` (`created_by_id`),
  KEY `role_lookup_last_modified_by_id_foreign` (`last_modified_by_id`),
  KEY `role_lookup_name_index` (`name`),
  CONSTRAINT `role_lookup_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `role_lookup_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `role_lookup_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_lookup`
--

LOCK TABLES `role_lookup` WRITE;
/*!40000 ALTER TABLE `role_lookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_lookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_service_access`
--

DROP TABLE IF EXISTS `role_service_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_service_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `component` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verb_mask` int(10) unsigned NOT NULL DEFAULT '0',
  `requestor_mask` int(10) unsigned NOT NULL DEFAULT '0',
  `filters` text COLLATE utf8_unicode_ci,
  `filter_op` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'and',
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_service_access_role_id_foreign` (`role_id`),
  KEY `role_service_access_service_id_foreign` (`service_id`),
  KEY `role_service_access_created_by_id_foreign` (`created_by_id`),
  KEY `role_service_access_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `role_service_access_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `role_service_access_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `role_service_access_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_service_access_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_service_access`
--

LOCK TABLES `role_service_access` WRITE;
/*!40000 ALTER TABLE `role_service_access` DISABLE KEYS */;
INSERT INTO `role_service_access` VALUES (3,1,NULL,'*',31,3,'[]','AND','2015-11-13 22:17:31','2015-11-25 01:01:30',NULL,NULL);
/*!40000 ALTER TABLE `role_service_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rws_config`
--

DROP TABLE IF EXISTS `rws_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rws_config` (
  `service_id` int(10) unsigned NOT NULL,
  `base_url` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`service_id`),
  CONSTRAINT `rws_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rws_config`
--

LOCK TABLES `rws_config` WRITE;
/*!40000 ALTER TABLE `rws_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `rws_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rws_headers_config`
--

DROP TABLE IF EXISTS `rws_headers_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rws_headers_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci,
  `pass_from_client` tinyint(1) NOT NULL DEFAULT '0',
  `action` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rws_headers_config_service_id_foreign` (`service_id`),
  CONSTRAINT `rws_headers_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rws_headers_config`
--

LOCK TABLES `rws_headers_config` WRITE;
/*!40000 ALTER TABLE `rws_headers_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `rws_headers_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rws_parameters_config`
--

DROP TABLE IF EXISTS `rws_parameters_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rws_parameters_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci,
  `exclude` tinyint(1) NOT NULL DEFAULT '0',
  `outbound` tinyint(1) NOT NULL DEFAULT '0',
  `cache_key` tinyint(1) NOT NULL DEFAULT '0',
  `action` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rws_parameters_config_service_id_foreign` (`service_id`),
  CONSTRAINT `rws_parameters_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rws_parameters_config`
--

LOCK TABLES `rws_parameters_config` WRITE;
/*!40000 ALTER TABLE `rws_parameters_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `rws_parameters_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesforce_db_config`
--

DROP TABLE IF EXISTS `salesforce_db_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesforce_db_config` (
  `service_id` int(10) unsigned NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` longtext COLLATE utf8_unicode_ci,
  `security_token` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`service_id`),
  CONSTRAINT `salesforce_db_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesforce_db_config`
--

LOCK TABLES `salesforce_db_config` WRITE;
/*!40000 ALTER TABLE `salesforce_db_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesforce_db_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `script_config`
--

DROP TABLE IF EXISTS `script_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `script_config` (
  `service_id` int(10) unsigned NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `config` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`service_id`),
  KEY `script_config_type_foreign` (`type`),
  CONSTRAINT `script_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE,
  CONSTRAINT `script_config_type_foreign` FOREIGN KEY (`type`) REFERENCES `script_type` (`name`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `script_config`
--

LOCK TABLES `script_config` WRITE;
/*!40000 ALTER TABLE `script_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `script_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `script_type`
--

DROP TABLE IF EXISTS `script_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `script_type` (
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `class_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sandboxed` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `script_type`
--

LOCK TABLES `script_type` WRITE;
/*!40000 ALTER TABLE `script_type` DISABLE KEYS */;
INSERT INTO `script_type` VALUES ('nodejs','DreamFactory\\Core\\Scripting\\Engines\\NodeJs','Node.js','Server-side JavaScript handler using the Node.js engine.',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('php','DreamFactory\\Core\\Scripting\\Engines\\Php','PHP','Script handler using native PHP.',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('v8js','DreamFactory\\Core\\Scripting\\Engines\\V8Js','V8js','Server-side JavaScript handler using the V8js engine.',1,'2015-11-13 19:49:51','2015-11-13 19:49:51');
/*!40000 ALTER TABLE `script_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mutable` tinyint(1) NOT NULL DEFAULT '1',
  `deletable` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_name_unique` (`name`),
  KEY `service_type_foreign` (`type`),
  KEY `service_created_by_id_foreign` (`created_by_id`),
  KEY `service_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `service_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `service_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `service_type_foreign` FOREIGN KEY (`type`) REFERENCES `service_type` (`name`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'system','System Management','Service for managing system resources.',1,'system',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL),(2,'api_docs','Live API Docs','API documenting and testing service.',1,'swagger',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL),(3,'files','Local File Storage','Service for accessing local file storage.',1,'local_file',1,1,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL),(4,'db','Local SQL Database','Service for accessing local SQLite database.',1,'sql_db',1,1,'2015-11-13 19:49:51','2015-11-13 19:49:51',NULL,NULL),(6,'user','User Management','Service for managing system users.',1,'user',1,0,'2015-11-13 19:49:51','2015-12-03 11:16:14',NULL,5),(7,'mysql','MySQL Database','A MySQL database service.',1,'sql_db',1,1,'2015-11-13 19:49:52','2015-11-13 19:49:52',NULL,NULL),(8,'mongodb','MongoDB','A MongoDB database service.',1,'mongodb',1,1,'2015-11-13 19:49:52','2015-11-13 19:49:52',NULL,NULL),(9,'backend','seventeendays_db','Seventeen Days',1,'sql_db',1,1,'2015-11-13 19:54:44','2016-03-23 22:26:27',1,107),(10,'facebook_auth','facebook_auth','Facebook Auth',1,'oauth_facebook',1,1,'2015-11-13 23:37:08','2015-12-03 07:34:14',1,5),(11,'mailer','Email Service','Email utility access',1,'smtp_email',1,1,'2015-12-03 05:18:10','2015-12-03 11:10:30',5,5);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_cache_config`
--

DROP TABLE IF EXISTS `service_cache_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_cache_config` (
  `service_id` int(10) unsigned NOT NULL,
  `cache_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `cache_ttl` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`service_id`),
  CONSTRAINT `service_cache_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_cache_config`
--

LOCK TABLES `service_cache_config` WRITE;
/*!40000 ALTER TABLE `service_cache_config` DISABLE KEYS */;
INSERT INTO `service_cache_config` VALUES (9,0,0);
/*!40000 ALTER TABLE `service_cache_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_doc`
--

DROP TABLE IF EXISTS `service_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_doc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(10) unsigned NOT NULL,
  `format` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `service_doc_service_id_foreign` (`service_id`),
  CONSTRAINT `service_doc_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_doc`
--

LOCK TABLES `service_doc` WRITE;
/*!40000 ALTER TABLE `service_doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_type`
--

DROP TABLE IF EXISTS `service_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_type` (
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `class_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `config_handler` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `singleton` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_type`
--

LOCK TABLES `service_type` WRITE;
/*!40000 ALTER TABLE `service_type` DISABLE KEYS */;
INSERT INTO `service_type` VALUES ('adldap','DreamFactory\\Core\\ADLdap\\Services\\ADLdap','DreamFactory\\Core\\ADLdap\\Models\\LDAPConfig','Active Directory LDAP','A service for supporting Active Directory integration','LDAP',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('aws_dynamodb','DreamFactory\\Core\\Aws\\Services\\DynamoDb','DreamFactory\\Core\\Aws\\Models\\AwsConfig','AWS DynamoDB','A database service supporting the AWS DynamoDB system.','Database',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('aws_s3','DreamFactory\\Core\\Aws\\Services\\S3','DreamFactory\\Core\\Aws\\Components\\AwsS3Config','AWS S3','File storage service supporting the AWS S3 file system.','File',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('aws_ses','DreamFactory\\Core\\Aws\\Services\\Ses','DreamFactory\\Core\\Aws\\Models\\AwsConfig','AWS SES','Email service supporting the AWS SES system.','Email',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('aws_sns','DreamFactory\\Core\\Aws\\Services\\Sns','DreamFactory\\Core\\Aws\\Models\\AwsConfig','AWS SNS','Push notification service supporting the AWS SNS system.','Notification',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('couchdb','DreamFactory\\Core\\CouchDb\\Services\\CouchDb','DreamFactory\\Core\\CouchDb\\Models\\CouchDbConfig','CouchDB','Database service for CouchDB connections.','Database',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('event','DreamFactory\\Core\\Services\\Event',NULL,'Event Service','Service that allows clients to subscribe to system broadcast events.','Event',1,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('ldap','DreamFactory\\Core\\ADLdap\\Services\\LDAP','DreamFactory\\Core\\ADLdap\\Models\\LDAPConfig','Standard LDAP','A service for supporting Open LDAP integration','LDAP',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('local_email','DreamFactory\\Core\\Services\\Email\\Local',NULL,'Local Email Service','Local email service using system configuration.','Email',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('local_file','DreamFactory\\Core\\Services\\LocalFileService','DreamFactory\\Core\\Models\\FilePublicPath','Local File Service','File service supporting the local file system.','File',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('mailgun_email','DreamFactory\\Core\\Services\\Email\\MailGun','DreamFactory\\Core\\Models\\MailGunConfig','Mailgun Email Service','Mailgun email service','Email',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('mandrill_email','DreamFactory\\Core\\Services\\Email\\Mandrill','DreamFactory\\Core\\Models\\MandrillConfig','Mandrill Email Service','Mandrill email service','Email',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('mongodb','DreamFactory\\Core\\MongoDb\\Services\\MongoDb','DreamFactory\\Core\\MongoDb\\Models\\MongoDbConfig','MongoDB','Database service for MongoDB connections.','Database',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('oauth_facebook','DreamFactory\\Core\\OAuth\\Services\\Facebook','DreamFactory\\Core\\OAuth\\Models\\OAuthConfig','Facebook OAuth','OAuth service for supporting Facebook authentication and API access.','OAuth',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('oauth_github','DreamFactory\\Core\\OAuth\\Services\\Github','DreamFactory\\Core\\OAuth\\Models\\OAuthConfig','GitHub OAuth','OAuth service for supporting GitHub authentication and API access.','OAuth',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('oauth_google','DreamFactory\\Core\\OAuth\\Services\\Google','DreamFactory\\Core\\OAuth\\Models\\OAuthConfig','Google OAuth','OAuth service for supporting Google authentication and API access.','OAuth',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('oauth_twitter','DreamFactory\\Core\\OAuth\\Services\\Twitter','DreamFactory\\Core\\OAuth\\Models\\OAuthConfig','Twitter OAuth','OAuth service for supporting Twitter authentication and API access.','OAuth',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('openstack_obect_storage','DreamFactory\\Core\\Rackspace\\Services\\OpenStackObjectStore','DreamFactory\\Core\\Rackspace\\Components\\OpenStackObjectStorageConfig','OpenStack Object Storage','File service supporting OpenStack Object Storage system.','File',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('rackspace_cloud_files','DreamFactory\\Core\\Rackspace\\Services\\OpenStackObjectStore','DreamFactory\\Core\\Rackspace\\Components\\RackspaceCloudFilesConfig','Rackspace Cloud Files','File service supporting Rackspace Cloud Files Storage system.','File',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('rws','DreamFactory\\Core\\Rws\\Services\\RemoteWeb','DreamFactory\\Core\\Rws\\Models\\RwsConfig','Remote Web Service','A service to handle Remote Web Services','Custom',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('salesforce_db','DreamFactory\\Core\\Salesforce\\Services\\SalesforceDb','DreamFactory\\Core\\Salesforce\\Models\\SalesforceConfig','SalesforceDB','Database service for Salesforce connections.','Database',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('script','DreamFactory\\Core\\Services\\Script','DreamFactory\\Core\\Models\\ScriptConfig','Custom Scripting Service','Service that allows client-callable scripts utilizing the system scripting.','Custom',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('smtp_email','DreamFactory\\Core\\Services\\Email\\Smtp','DreamFactory\\Core\\Models\\SmtpConfig','SMTP Email Service','SMTP-based email service','Email',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('sql_db','DreamFactory\\Core\\SqlDb\\Services\\SqlDb','DreamFactory\\Core\\SqlDb\\Models\\SqlDbConfig','SQL DB','Database service supporting SQL connections.','Database',0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('swagger','DreamFactory\\Core\\Services\\Swagger',NULL,'Swagger API Docs','API documenting and testing service using Swagger specifications.','API Doc',1,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('system','DreamFactory\\Core\\Services\\System',NULL,'System Management Service','Service supporting management of the system.','System',1,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('user','DreamFactory\\Core\\User\\Services\\User','DreamFactory\\Core\\User\\Models\\UserConfig','User service','User service to allow user management.','User',1,'2015-11-13 19:49:51','2015-11-13 19:49:51');
/*!40000 ALTER TABLE `service_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `smtp_config`
--

DROP TABLE IF EXISTS `smtp_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `smtp_config` (
  `service_id` int(10) unsigned NOT NULL,
  `host` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `port` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '587',
  `encryption` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'tls',
  `username` longtext COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`service_id`),
  CONSTRAINT `smtp_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `smtp_config`
--

LOCK TABLES `smtp_config` WRITE;
/*!40000 ALTER TABLE `smtp_config` DISABLE KEYS */;
INSERT INTO `smtp_config` VALUES (11,'smtp.mailgun.org','587','tls','eyJpdiI6InJIMndicjdEcmF4ZE1ydnAxWWtocVE9PSIsInZhbHVlIjoiY3VhQ1VSYThXa2ppMGtJb3QxVCttZTZUWDE2eXdTeHY2OFIrWlRmSVgzQWNqUXFUNytjWjcwd0pCd3Znd0ZtTCIsIm1hYyI6IjU1YWU3MzJkMDFiY2M0OTk3ZWIwYjI5NzhiZWNjNTFhNTBmZDI3YzEyMDczZGNmN2U2YzkxNzY0NzYxNTcxNTcifQ==','eyJpdiI6Ikt2blpzajVmdG9jSUQweml0eDZmdVE9PSIsInZhbHVlIjoiVGRcL2QybzMzMUczb3l3cjZIY25xQWRJNTVQSm5obTFidkRJWUlXMnk5TlpaQzFqc3JSWHhwcnpIQ3lxRFVLNG8iLCJtYWMiOiIzM2JkZjNmMTQxZTc4MmUyN2Q0MWRkZTM1NmZlMjVmMmY4MmEyYTBiZGM2YjFkOTNlYjBkMjIwYTEyNmU3MDdmIn0=');
/*!40000 ALTER TABLE `smtp_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sql_db_config`
--

DROP TABLE IF EXISTS `sql_db_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sql_db_config` (
  `service_id` int(10) unsigned NOT NULL,
  `driver` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `dsn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` longtext COLLATE utf8_unicode_ci,
  `password` longtext COLLATE utf8_unicode_ci,
  `options` text COLLATE utf8_unicode_ci,
  `attributes` text COLLATE utf8_unicode_ci,
  `default_schema_only` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`service_id`),
  CONSTRAINT `sql_db_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sql_db_config`
--

LOCK TABLES `sql_db_config` WRITE;
/*!40000 ALTER TABLE `sql_db_config` DISABLE KEYS */;
INSERT INTO `sql_db_config` VALUES (4,'sqlite','sqlite:db.sqlite',NULL,NULL,NULL,NULL,0),(7,'mysql','mysql:host=localhost;port=3306;dbname=bitnami_df','eyJpdiI6Im9MdjVqY25FM0Jzdzl3T0xrVW44MkE9PSIsInZhbHVlIjoiTlFYRGJSV1J1Z3JCVFlLNlwvR0F4cVE9PSIsIm1hYyI6ImViYjczNzFjMWE2YTkzMzkzZDc4NTNmZjQxZmVlYTBhYjI0MzgxNDY1NWVmY2U3ODI3ODBkNzY0MmVkNmM3NDcifQ==','eyJpdiI6Im1lVDdyOVNTR0FOcFUySHdYZUxxRVE9PSIsInZhbHVlIjoidWlyOUxrY21JNWtHQVRMOUoxYTVhR2FzYzB5cGtjcEJBaVNTUEhVdzM0bz0iLCJtYWMiOiIwZGExNmRkY2I1N2I2M2IzZmFlNTBhMTJhNTBmMTZkMGY3NDk2OTc3NmJjNDRjZjIzMDJhNjg5NTRmNWVkMTBlIn0=',NULL,NULL,0),(9,'mysql','mysql:host=10.209.129.95;port=3306;dbname=seventeendays','eyJpdiI6IlB0ektZa0lcL2NxVVhhV0ttT0xueXBBPT0iLCJ2YWx1ZSI6Ilp3a0FlWUNpeVhIRUN2TDRjQ0Zxd0E9PSIsIm1hYyI6Ijk3NjIyNTViMDQ4MjM5ODAwOWE0MWVhMWUyOTFiNTFkNzE4NGIwZDdkOWNjNGFhMWQyM2ZlZWM2MTQ3ZjYyNWMifQ==','eyJpdiI6IkhoT051ZEJ3alpabjRmQWNzbUx1VEE9PSIsInZhbHVlIjoiMDJlN2Jzc0liblZtNnl4Mm1zZ295RmIrMDk0SlwvRzBQTms5Q1MrcGlPMm89IiwibWFjIjoiYWUzMWU4YjdjNDIxZmI1ZmRiNTdkM2JmMTQxZDJjOGY2MGQzNWU5Y2QzNGQyZGExNTRkM2E0M2MyY2QzYjlmMiJ9',NULL,NULL,0);
/*!40000 ALTER TABLE `sql_db_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_config`
--

DROP TABLE IF EXISTS `system_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_config` (
  `db_version` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `login_with_user_name` tinyint(1) NOT NULL DEFAULT '0',
  `default_app_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`db_version`),
  KEY `system_config_created_by_id_foreign` (`created_by_id`),
  KEY `system_config_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `system_config_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `system_config_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_config`
--

LOCK TABLES `system_config` WRITE;
/*!40000 ALTER TABLE `system_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_custom`
--

DROP TABLE IF EXISTS `system_custom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_custom` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `system_custom_created_by_id_foreign` (`created_by_id`),
  KEY `system_custom_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `system_custom_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `system_custom_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_custom`
--

LOCK TABLES `system_custom` WRITE;
/*!40000 ALTER TABLE `system_custom` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_custom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_lookup`
--

DROP TABLE IF EXISTS `system_lookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_lookup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `system_lookup_name_unique` (`name`),
  KEY `system_lookup_created_by_id_foreign` (`created_by_id`),
  KEY `system_lookup_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `system_lookup_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `system_lookup_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_lookup`
--

LOCK TABLES `system_lookup` WRITE;
/*!40000 ALTER TABLE `system_lookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_lookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_resource`
--

DROP TABLE IF EXISTS `system_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_resource` (
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `class_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `singleton` tinyint(1) NOT NULL DEFAULT '0',
  `read_only` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_resource`
--

LOCK TABLES `system_resource` WRITE;
/*!40000 ALTER TABLE `system_resource` DISABLE KEYS */;
INSERT INTO `system_resource` VALUES ('admin','DreamFactory\\Core\\Resources\\System\\Admin','Administrators','Allows configuration of system administrators.','DreamFactory\\Core\\Models\\User',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('app','DreamFactory\\Core\\Resources\\System\\App','Apps','Allows management of user application(s)','DreamFactory\\Core\\Models\\App',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('app_group','DreamFactory\\Core\\Resources\\System\\AppGroup','App Groups','Allows grouping of user application(s)','DreamFactory\\Core\\Models\\AppGroup',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('cache','DreamFactory\\Core\\Resources\\System\\Cache','Cache Administration','Allows administration of system-wide and service cache.',NULL,0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('config','DreamFactory\\Core\\Resources\\System\\Config','Configuration','Global system configuration.',NULL,1,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('constant','DreamFactory\\Core\\Resources\\System\\Constant','Constants','Read-only listing of constants available for client use.',NULL,0,1,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('cors','DreamFactory\\Core\\Resources\\System\\Cors','CORS Configuration','Allows configuration of CORS system settings.','DreamFactory\\Core\\Models\\CorsConfig',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('custom','DreamFactory\\Core\\Resources\\System\\Custom','Custom Settings','Allows for creating system-wide custom settings','DreamFactory\\Core\\Models\\SystemCustom',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('email_template','DreamFactory\\Core\\Resources\\System\\EmailTemplate','Email Templates','Allows configuration of email templates.','DreamFactory\\Core\\Models\\EmailTemplate',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('environment','DreamFactory\\Core\\Resources\\System\\Environment','Environment','Read-only system environment configuration.',NULL,1,1,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('event','DreamFactory\\Core\\Resources\\System\\Event','Events','Allows registering server-side scripts to system generated events.',NULL,0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('lookup','DreamFactory\\Core\\Resources\\System\\Lookup','Lookup Keys','Allows configuration of lookup keys.','DreamFactory\\Core\\Models\\Lookup',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('role','DreamFactory\\Core\\Resources\\System\\Role','Roles','Allows role configuration.','DreamFactory\\Core\\Models\\Role',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('script_type','DreamFactory\\Core\\Resources\\System\\ScriptType','Script Types','Read-only system scripting types.','DreamFactory\\Core\\Models\\ScriptType',0,1,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('service','DreamFactory\\Core\\Resources\\System\\Service','Services','Allows configuration of services.','DreamFactory\\Core\\Models\\Service',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('service_type','DreamFactory\\Core\\Resources\\System\\ServiceType','Service Types','Read-only system service types.','DreamFactory\\Core\\Models\\ServiceType',0,1,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('setting','DreamFactory\\Core\\Resources\\System\\Setting','Custom Settings','Allows configuration of system-wide custom settings.','DreamFactory\\Core\\Models\\Setting',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51'),('user','DreamFactory\\Core\\User\\Resources\\System\\User','User Management','Allows user management capability.','DreamFactory\\Core\\Models\\User',0,0,'2015-11-13 19:49:51','2015-11-13 19:49:51');
/*!40000 ALTER TABLE `system_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_setting`
--

DROP TABLE IF EXISTS `system_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `system_setting_name_unique` (`name`),
  KEY `system_setting_created_by_id_foreign` (`created_by_id`),
  KEY `system_setting_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `system_setting_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `system_setting_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_setting`
--

LOCK TABLES `system_setting` WRITE;
/*!40000 ALTER TABLE `system_setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token_map`
--

DROP TABLE IF EXISTS `token_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token_map` (
  `user_id` int(10) unsigned NOT NULL,
  `token` text COLLATE utf8_unicode_ci NOT NULL,
  `iat` int(10) unsigned NOT NULL,
  `exp` int(10) unsigned NOT NULL,
  KEY `token_map_user_id_foreign` (`user_id`),
  CONSTRAINT `token_map_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token_map`
--

LOCK TABLES `token_map` WRITE;
/*!40000 ALTER TABLE `token_map` DISABLE KEYS */;
INSERT INTO `token_map` VALUES (587,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjU4NywidXNlcl9pZCI6NTg3LCJlbWFpbCI6Im1pY2hhZWxxdWFjaEBvcGVuYXJjLm5ldCIsImZvcmV2ZXIiOmZhbHNlLCJpc3MiOiJodHRwOlwvXC9kcmVhbWZhY3Rvcnkuc2V2ZW50ZWVuZGF5cy5vcGVuYXJjLm5ldFwvcmVzdFwvdXNlclwvc2Vzc2lvbiIsImlhdCI6IjE0NjYwMjE0NzUiLCJleHAiOiIxNDY2MDI1MDc1IiwibmJmIjoiMTQ2NjAyMTQ3NSIsImp0aSI6IjZhYjM2ZWZlZDIwMjY3MzBjMWUzMzg5YWIwNGE5YTE1In0.i-nGwVhJ1Q0U2EbN236FwUZSVG-rjFobkMVhyp0xQs4',1466021475,1466025075),(344,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM0NCwidXNlcl9pZCI6MzQ0LCJlbWFpbCI6ImNjaGVwYW5vQGFuZHJldy5jbXUuZWR1IiwiZm9yZXZlciI6ZmFsc2UsImlzcyI6Imh0dHA6XC9cL2RyZWFtZmFjdG9yeS5zZXZlbnRlZW5kYXlzLm9wZW5hcmMubmV0XC9yZXN0XC91c2VyXC9zZXNzaW9uIiwiaWF0IjoiMTQ2NjQ0MTg5NSIsImV4cCI6IjE0NjY0NDU0OTUiLCJuYmYiOiIxNDY2NDQxODk1IiwianRpIjoiZDkwNDRiMmZlNjMyOWEwNzcxNzQ0ZjYzZjk3MWUxZTYifQ.L-cnKRBCuywbKRbLdRlOrKAKh4lBFvhQSyKbaTFO20k',1466441895,1466445495),(588,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjU4OCwidXNlcl9pZCI6NTg4LCJlbWFpbCI6Impvc2h3QG9wZW5hcmMubmV0IiwiZm9yZXZlciI6ZmFsc2UsImlzcyI6Imh0dHA6XC9cL2RyZWFtZmFjdG9yeS5zZXZlbnRlZW5kYXlzLm9wZW5hcmMubmV0XC9yZXN0XC91c2VyXC9zZXNzaW9uIiwiaWF0IjoiMTQ2NjQ0NTc5MSIsImV4cCI6IjE0NjY0NDkzOTEiLCJuYmYiOiIxNDY2NDQ1NzkxIiwianRpIjoiNTQ0NmJkYjdkNGUyZjYyMTM4MmY0MDllYzAzZDY3NjUifQ.7oJqoDmSEkQ9uioQuBXozSsV1zSmdr4wqpxz9qspCME',1466445791,1466449391);
/*!40000 ALTER TABLE `token_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci,
  `is_sys_admin` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `phone` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `security_question` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `security_answer` longtext COLLATE utf8_unicode_ci,
  `confirm_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_app_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adldap` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_provider` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email_unique` (`email`),
  KEY `user_created_by_id_foreign` (`created_by_id`),
  KEY `user_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `user_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `user_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=589 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'yb admin','Mark','Duffy','2015-12-15 20:54:36','mark@ybinteractive.com','$2y$10$ntnXjrvXSkk9ryOc6Aq6/uMxBe7iXGgncrtFjFmxNQRdys1XkuS.y',1,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-13 19:52:25','2015-12-16 01:54:36',NULL,NULL),(5,'Brian','Brian','Wyss','2015-12-04 17:30:31','merqtycoon@gmail.com','$2y$10$lC1GdwZ4RvF6CoVC/Z.dduobPZoxUHqdfMtsLrZ1PgJL4FYNgoYGy',1,1,'123-123-1234',NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-20 23:23:50','2015-12-04 22:30:31',1,5),(10,'example1','abc','abc',NULL,'abc@example.com','$2y$10$17rnuGHL84x/OADhRlCjwekOddrY/E5j00XQfhrLBChEVjW6zMuBW',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-23 03:47:01','2015-11-23 03:47:01',NULL,NULL),(12,'Brian Wyss','surfyst1','surfyst1',NULL,'surfyst1@gmail.com','$2y$10$2RuXOINoVzmvQpdJCdOFdu/4o/6omCjeOS52Jy9HVWWsxbCZ63.cu',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-23 04:03:26','2015-11-23 04:03:26',NULL,NULL),(13,'Brian Wyss','surfyst2','surfyst2','2015-11-23 21:46:36','surfyst2@gmail.com','$2y$10$b59f2/uCMI440cSfniuf4e54kduXYIlXeytEwxB2Pk9J70vRQdQ4q',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-23 04:04:27','2015-11-24 02:46:36',NULL,NULL),(14,'abc11 abc11','abc11','abc11',NULL,'abc11@example.com','$2y$10$/bpBZYtfkj2TOw9iNkV8uOcQu8UYHODfK9xm/YDJnXrHLi.OqjmVG',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24 11:47:34','2015-11-24 11:47:34',NULL,NULL),(15,'abc112 abc112','abc112','abc112',NULL,'abc112@example.com','$2y$10$sLnzXhZWZ89eGlknDq.0muD80Yfyh8bhXb3W5QtUxSqADSzIe5HWu',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24 11:49:02','2015-11-24 11:49:03',NULL,NULL),(16,'abc113 abc113','abc113','abc113',NULL,'abc113@example.com','$2y$10$pWFGSoambYUvi2KFqiJGYu0f.5tmWG0Xm9MxyzB3OfljImeDMZRsC',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24 11:49:35','2015-11-24 11:49:36',NULL,NULL),(17,'abc1231 abc1231','abc1231','abc1231',NULL,'abc1231@example.com','$2y$10$vTevhfbRXl705rPYrDr3aO.NPrWpT54h5.MZrDmj3qoGF4qY2eP3i',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24 12:05:11','2015-11-24 12:05:11',NULL,NULL),(18,'abc12231 abc12231','abc12231','abc12231',NULL,'abc12231@example.com','$2y$10$zGHGTox0S/ED62bRlhriWuVi2ZSBUwNcw/EHrdqJ3G3kBqwo0H/kK',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24 12:23:42','2015-11-24 12:23:43',NULL,NULL),(19,'abc132231 abc132231','abc132231','abc132231',NULL,'abc132231@example.com','$2y$10$AGrNqwMSxYjnzrhrueKZVuiBtumBKtY4FjwomjIlHBnJftpSNjQlC',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24 12:23:59','2015-11-24 12:23:59',NULL,NULL),(20,'abc1322231 abc1322231','abc1322231','abc1322231',NULL,'abc1322231@example.com','$2y$10$2lc6iPMipTqVBbVP3iZqwOW9AMGb8DwrMN.g/IX.1RAMh/oQzCds2',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24 12:25:51','2015-11-24 12:25:51',NULL,NULL),(21,'abc133222331 abc133222331','abc133222331','abc133222331',NULL,'abc133222331@example.com','$2y$10$EOsQvnsekVh9.hE/oDU9L.Mo/uFqMVH6QiXMrgrHUrp/0bjigdgwS',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24 12:34:38','2015-11-24 12:34:38',NULL,NULL),(24,'abc13123 abc13123','abc13123','abc13123',NULL,'abc13123@example.com','$2y$10$tYhqW85pSeX8zp.nLCiuN.0pk2A.PIYEeJBYwaIARljsUhAC7NKZC',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24 13:12:15','2015-11-24 13:12:15',NULL,NULL),(25,'p p','p','p','2015-11-24 14:29:30','p@p.com','$2y$10$Y1D8aWXX4MCbx8rr//QtQeKDgj0tvDLE/hhd9Qurb34yybEPAQLK2',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-24 19:27:54','2015-11-24 19:29:30',NULL,NULL),(26,'o o','o','o',NULL,'o@o.com','$2y$10$WfaKLeq8peqbVQ4f9Fp68.zKsJtfA8rwCoxap3Pja1QT23jbfDnWG',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24 19:31:58','2015-11-24 19:31:58',NULL,NULL),(27,'r r','r','r',NULL,'r@r.com','$2y$10$Y1aGmdoFtWKPzakJ2kR75eGf5KH6srwz1P2LORyo0TUWBec5Pa1OG',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24 19:33:09','2015-11-24 19:33:09',NULL,NULL),(28,'e e','e','e',NULL,'e@e.com','$2y$10$dHL6zHIFcltH91DZ7eUADe0ThoXzK8v7ydQ5ElaydcodzhbpWa4Z6',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-24 19:35:35','2015-11-24 19:35:35',NULL,NULL),(29,'d d','d','d','2015-11-24 14:44:39','d@d.com','$2y$10$L8F0j/KxwmXgoVaheziZ/uYFJJK0eAKiWR3p0K8A9zFZoEAj/9BcG',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-24 19:44:38','2015-11-24 19:44:39',NULL,NULL),(30,'s s','s','s','2015-11-24 14:47:41','s@s.com','$2y$10$ust3ZtqHfxwYbzeiyg2cAOvcQhpxiM6r51ydP2pHa2S1Fg.QuRNZ6',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-24 19:45:38','2015-11-24 19:47:41',29,NULL),(31,'w w','w','w','2015-11-24 14:48:26','w@w.com','$2y$10$9ZU2RlfWvKlJorko.LcfJe6gPGg6TeZTxcnt34rOigqxIucqFHbVC',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-24 19:48:24','2015-11-24 19:48:26',NULL,NULL),(32,'i i','i','i','2015-11-24 14:49:47','i@i.com','$2y$10$EoQ8e0VVarL7pPRF4ATtmOKbDumXJg.2SfTxteYEnUboLDorRd7nC',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-24 19:48:55','2015-11-24 19:49:47',31,NULL),(41,'v v','v','v','2015-11-24 21:31:41','v@v.com','$2y$10$v8JgjJ.yU5BGmwHQZlxnv.mrVLqH03qhcgQ28daEnJmdoKYAWePG6',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-25 02:31:41','2015-11-25 02:31:41',NULL,NULL),(42,'vv vv','vv','vv','2015-11-24 21:37:03','vv@vv.com','$2y$10$Z00yF/gcu2d.8UlITeHCOOmYCw7LDjwl11wwaZAy5HelyWTLR9HSi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-25 02:35:35','2015-11-25 02:37:03',NULL,NULL),(43,'bb bb','bb','bb','2015-11-24 21:46:17','bb@bb.com','$2y$10$REPk/MyeCR8Xq0nELmr0OeWgB5xp9fycEOIbqljnnpLCL5ohKt/Fq',0,1,NULL,NULL,NULL,'JDJ5JDEwJGpTWmVoRHdab0pWZzMxWWtzdmFmVC5HaG5JajZFSk5rdUpmTXFFd05sWkJBY0hSRnl4U0gu',NULL,NULL,NULL,NULL,'2015-11-25 02:46:12','2015-11-26 04:25:32',NULL,NULL),(44,'n n','n','n','2015-12-07 19:58:37','n@n.com','$2y$10$Urbvo1F6i9PavsJKisT.1O2O6rQZTuKwfmxe/0g4LeiELynD175da',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-25 02:54:08','2015-12-08 00:58:37',NULL,NULL),(47,'f f','f','f','2015-11-24 22:52:19','f@f.com','$2y$10$S98F3BWdnKiYxkO1A06oiOqplB747zGF4X5EjAGFr.0MKqfINKWRW',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-25 03:52:18','2015-11-25 03:52:19',NULL,NULL),(56,'jjj jjj','jjj','jjj','2015-11-24 23:29:11','jjj@jjj.com','$2y$10$yL2ksmoSzCka3MI1aLKpIe5pJ44JSwAbGr5Llj3b7y93ywyHLnAhi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-25 04:29:10','2015-11-25 04:29:11',NULL,NULL),(57,'Brian Wyss','surfyst','surfyst','2015-12-03 06:20:29','surfyst@gmail.com','$2y$10$D9R530sFZ6UPgTf6V/W7euWnNHOpXd7jV8LKpZjfRWvMe5ngEwxdm',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-25 04:47:42','2015-12-03 11:20:29',NULL,NULL),(58,'c c','c','c','2015-11-24 23:51:34','c@c.com','$2y$10$blCArFXoszN0tT3LvtBgCenHc8d8SK1LR68TExsysLobOVhzp4Bse',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-25 04:51:34','2015-11-25 04:51:34',NULL,NULL),(60,'Surfyst Wyss','tycoon77555','tycoon77555',NULL,'tycoon77555@gmail.com','$2y$10$fhnEESw.SyllCYvJzzZINe4X.FeVQtie4CcBfLNpw8neeAjT8EDfW',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-11-25 18:13:30','2015-11-25 18:13:30',NULL,NULL),(63,'markaduffy+67 markaduffy+67','markaduffy+67','markaduffy+67','2015-11-25 14:25:32','markaduffy+67@gmail.com','$2y$10$pMYeavtKl4PTwzag0ihNDOg02L66ut0mji1upVADcintvAWNODUfK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-11-25 19:25:31','2015-11-25 19:25:32',NULL,NULL),(65,'markaduffy+test1212 markaduffy+test1212','markaduffy+test1212','markaduffy+test1212','2015-12-02 19:43:09','markaduffy+test1212@gmail.com','$2y$10$6iiWnyxzRK9i8R7hJcXo6.sW0mGIwvO2y7UQQJ1dRNVFKeWwgwQGi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-03 00:43:09','2015-12-03 00:43:09',NULL,NULL),(66,'abc131223 abc131223','abc131223','abc131223',NULL,'abc131223@example.com','$2y$10$jhoZfv5ppVUWQQ0vJfiIfeVIpg6sGCsIdhr..4i7YiBwTl0MuUfkS',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-03 03:09:55','2015-12-03 03:09:55',NULL,NULL),(67,'abc1 abc1','abc1','abc1',NULL,'abc1@example.com','$2y$10$e5igniCnoFgRi4EWxahqdeeXWXVOrr1Fj5boKdY867f0vVuG.dMfG',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-03 05:11:03','2015-12-03 05:11:03',NULL,NULL),(69,'markaduffy+test76 markaduffy+test76','markaduffy+test76','markaduffy+test76','2015-12-03 03:21:01','markaduffy+test76@gmail.com','$2y$10$/vXnDqwtOtjjNdZt7vUS0eA9a82Bb81.v9PP8LEWTaizEIJkr65Xu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-03 08:20:45','2015-12-03 08:21:01',NULL,NULL),(73,'markaduffy+test89 markaduffy+test89','markaduffy+test89','markaduffy+test89','2015-12-03 05:12:45','markaduffy+test89@gmail.com','$2y$10$pBuCXGwMsqH0ibUwcFZLA.8JqsS.y31.tOXnLoGKuWMDcnUnaj7Wu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-03 10:12:44','2015-12-03 10:12:45',NULL,NULL),(74,'markaduffy+test456 markaduffy+test456','markaduffy+test456','markaduffy+test456','2015-12-03 14:52:12','markaduffy+test456@gmail.com','$2y$10$tyhM87iP2To4XWauoFZJt.0y6nZjnFCzKyPgLDG/uIGHplC8sKN6a',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-03 19:08:48','2015-12-03 19:52:12',NULL,NULL),(76,'kkk kkk','kkk','kkk',NULL,'kkk@kkkk.com','$2y$10$nGTt9HwTb6dmSdsuRygyiezuheuwLgOxNcLqwTn.Axty/bLfCcj4q',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-04 05:29:52','2015-12-04 05:29:52',NULL,NULL),(77,'nnnn nnnn','nnnn','nnnn','2015-12-04 00:46:49','nnnn@n.com','$2y$10$LpqXfuCXrbPoGTDt6oAWvO6aixSzIcpo2ILs85QqchAZSIiNYc9IS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-04 05:46:48','2015-12-04 05:46:49',NULL,NULL),(82,'nsecworkshop1 nsecworkshop1','nsecworkshop1','nsecworkshop1','2015-12-10 13:25:30','nsecworkshop1@gmail.com','$2y$10$fnLjjiq1eKW857nUzC3BTe5pw5qNiaPYKcEESBzkAurBabgRJb9..',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-08 20:23:36','2015-12-10 18:25:30',NULL,NULL),(83,'necworkshop2 necworkshop2','necworkshop2','necworkshop2','2015-12-08 15:30:27','necworkshop2@gmail.com','$2y$10$ZgKRZKVJUoTBzBpBp1houOqmO3BtAw.LpSffW9.zKVH4LgxOqOxIm',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-08 20:30:27','2015-12-08 20:30:27',NULL,NULL),(84,'necworkshop3 necworkshop3','necworkshop3','necworkshop3','2015-12-08 15:38:53','necworkshop3@gmail.com','$2y$10$3W8xdXTcCsbbHacyVQWZ1OEzA3t/YrTk9nqWbXyKvLnGCLGJ22PTe',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-08 20:38:53','2015-12-08 20:38:53',NULL,NULL),(85,'nsecworkshop5 nsecworkshop5','nsecworkshop5','nsecworkshop5','2015-12-10 13:44:54','nsecworkshop5@gmail.com','$2y$10$d0dAW17fY1tREc1Dl0kT2uTcqjY.4tXAxw5jaSFKIkqzjGZRfTnOa',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-08 21:00:33','2015-12-10 18:44:54',NULL,NULL),(86,'necworkshop6 necworkshop6','necworkshop6','necworkshop6','2015-12-08 17:18:42','necworkshop6@gmail.com','$2y$10$aFTVW75JMs.4Y3Sj3508XOXILu391F0mHlYEAmR8kc8PC6YxUasEq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-08 22:18:42','2015-12-08 22:18:42',NULL,NULL),(89,'ncesworkshop2 ncesworkshop2','ncesworkshop2','ncesworkshop2','2015-12-10 13:26:52','ncesworkshop2@gmail.com','$2y$10$fh7XQ1962CojDgT07SBXTu7NIof8ojNTADtJ2uDl/kajEW7XMzsp2',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-10 18:21:42','2015-12-10 18:26:52',NULL,NULL),(90,'nsecworkshop3 nsecworkshop3','nsecworkshop3','nsecworkshop3','2015-12-10 13:33:44','nsecworkshop3@gmail.com','$2y$10$E/jeKEw518z.kSMcol7krudyZUifPkoje7OBLKBEgZRngUU4JV7Ta',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-10 18:31:21','2015-12-10 18:33:44',NULL,NULL),(91,'ncesworkshop4 ncesworkshop4','ncesworkshop4','ncesworkshop4','2015-12-10 13:37:07','ncesworkshop4@gmail.com','$2y$10$q7rKIBnS6Q21/DmUpCaI.O9sg4KgwWJsIwGoisbyY5KZ0irfFDTQW',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-10 18:37:07','2015-12-10 18:37:07',NULL,NULL),(92,'nsecworkshop4 nsecworkshop4','nsecworkshop4','nsecworkshop4','2015-12-10 13:39:37','nsecworkshop4@gmail.com','$2y$10$9WDeUXWwESnkY73ocLajxOrxglvo7XRCJCVDfLGz6QvCoIm9TBCQ6',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-10 18:39:37','2015-12-10 18:39:37',NULL,NULL),(93,'nsecworkshop6 nsecworkshop6','nsecworkshop6','nsecworkshop6','2015-12-10 13:51:34','nsecworkshop6@gmail.com','$2y$10$Fa7Plm37bStuG4cpK/ttx.t7Z/Y7NXBLT7U2/bstZk6hvDV8JjbIq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-10 18:49:05','2015-12-10 18:51:34',NULL,NULL),(94,'nsecworkshop8 nsecworkshop8','nsecworkshop8','nsecworkshop8','2015-12-10 16:33:48','nsecworkshop8@gmail.com','$2y$10$hfYcCimSV0sNRVdFX6CfU.yg48ld8VeC.mykOPg.GV1RgQoUm/kC6',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-10 21:33:48','2015-12-10 21:33:48',NULL,NULL),(95,'nakitah nakitah','nakitah','nakitah','2015-12-10 16:34:19','nakitah@pipeline.com','$2y$10$CO4BOTbuHhm24RI6piPXiO4Nl3qhnnqs2OXsxzB9WqSs9ar/ayHtK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-10 21:34:19','2015-12-10 21:34:19',NULL,NULL),(96,'erin erin','erin','erin','2015-12-10 16:36:53','erin@thecsph.org','$2y$10$JTnLXmFtID9AX/hlkQZoJuRnokv2QLKwEG2Qzn2z8YcccHEIG8hk2',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-10 21:36:52','2015-12-10 21:36:53',NULL,NULL),(97,'alyons alyons','alyons','alyons','2015-12-10 16:37:17','alyons@coloradoyouthmatter.org','$2y$10$CdBXjdNGfw1HHkp01GJHn.YQ01t2ea7tOedsl9bMfMf9GmQkBG2ci',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-10 21:37:17','2015-12-10 21:37:17',NULL,NULL),(98,'nsecworkshop11 nsecworkshop11','nsecworkshop11','nsecworkshop11','2015-12-10 16:38:09','nsecworkshop11@gmail.com','$2y$10$Y7PRpwphswoYdzRtzLVLfuRm/uydcpYkVuRcnngj2da3GNlMUZUkK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-10 21:38:08','2015-12-10 21:38:09',NULL,NULL),(99,'mallen mallen','mallen','mallen','2015-12-10 16:40:47','mallen@tiogaopp.org','$2y$10$0zqlEfHMYiArZJx07GFv9.v6Larf.Td3OWyeuEhdneBhwLlbmeDl6',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-10 21:40:47','2015-12-10 21:40:47',NULL,NULL),(100,'harryhood2112 harryhood2112','harryhood2112','harryhood2112','2015-12-10 20:42:39','harryhood2112@gmail.com','$2y$10$UXJ81bgiinOW6m1rZj1Rm.B.X5zDHdYrb/AKTAsBALlCrmBt7ZH6C',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-11 01:42:39','2015-12-11 01:42:39',NULL,NULL),(102,'pmurray pmurray','pmurray','pmurray','2016-01-25 02:38:20','pmurray@hsc.wvu.edu','$2y$10$doIIwMgS4I9WCBbQm8ES1ueX71lXaaJbtBs3XqxooxA635dSjc6Pi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-11 02:36:59','2016-01-25 07:38:20',NULL,NULL),(104,'Mark Duffy','markaduffy','markaduffy',NULL,'markaduffy@gmail.com','$2y$10$Wjtta7wF.HohvOAc7HIoZeZEHCaqv4Z8iz0N0qdcuWoPUg/6dhQ8u',0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-15 20:06:51','2015-12-15 20:06:51',NULL,NULL),(105,'charlotte boyd','charlotte','boyd','2015-12-15 17:25:13','charlotte.boyd@opportunitycharter.org','$2y$10$xcNiQTDJkz22ZXHs7E/8z.rlFAmf9yFwClEl4mjQ4rOcyHmuD5pkK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-15 22:25:13','2015-12-15 22:25:13',NULL,NULL),(106,'travis travis','travis','travis','2015-12-24 20:37:02','travis@openarc.net','$2y$10$ugbcKeslB./BPRXMrWRvS.0viONK.nWvwdRzEep9r3pxGPVNwlI7O',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-16 01:50:06','2015-12-25 01:37:02',NULL,NULL),(107,'Travis','Travis','Loncar','2016-05-31 13:22:42','travisloncar@openarc.net','$2y$10$t3nwSp09WZ.NG0VBWiat2.7BbCrt5m27Hjuha19mx0YCG0HJ85CAy',1,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-16 01:55:41','2016-05-31 17:22:42',1,107),(108,'mquinn media','mquinn','media','2016-02-21 02:27:04','mquinn.media@gmail.com','$2y$10$/hPRVijuH4I8JwzzUHBvbekFJ4tSg.hCRW3sEi/AWl2yq2IU5.wEK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-21 08:57:25','2016-02-21 07:27:04',NULL,NULL),(109,'jrealin jrealin','jrealin','jrealin','2015-12-21 15:43:53','jrealin@hf-tx.org','$2y$10$2mK5fTnOWshMclssRkpTauRUZMXyQmqoqaBc5utU8ojZaBe5yVzmq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-21 20:43:53','2015-12-21 20:43:53',NULL,NULL),(110,'cskismith cskismith','cskismith','cskismith','2016-01-31 20:29:54','cskismith@gmail.com','$2y$10$xOYh.sB9LwOKGRO/qFTSh.7uz7tDLEjruU6bjd3rzuMlh6MnmqWEa',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-26 20:52:17','2016-02-01 01:29:54',NULL,NULL),(111,'smwatson914 smwatson914','smwatson914','smwatson914','2015-12-30 16:55:16','smwatson914@aol.com','$2y$10$IUBckjBy2JW8vDbmAK77p.s9IGDpd3LqWvtC39Lf6JymVG7Vqug.W',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2015-12-30 21:55:15','2015-12-30 21:55:16',NULL,NULL),(112,'kimdebaldo kimdebaldo','kimdebaldo','kimdebaldo','2016-01-03 00:45:30','kimdebaldo@gmail.com','$2y$10$pD8Ooy7ChvbnSrQh7YjdR.spjpyn/J5OBkVlC/NWYA4mftkUrQHFq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-03 05:45:29','2016-01-03 05:45:30',NULL,NULL),(113,'beth vazquez','beth','vazquez','2016-01-05 19:50:24','beth.vazquez@gmail.com','$2y$10$5QSDAZvm1/sxv4PeoVRwz.Inz9PdZ9inxqxfhIOkrDi0Ecywp3Mcm',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-06 00:50:24','2016-01-06 00:50:24',NULL,NULL),(114,'bmommy bmommy','bmommy','bmommy','2016-01-06 03:58:50','bmommy@gmail.com','$2y$10$uFfHoZl8OvyFrZZ0vHbFKOmsk0PDM5cUWtmOTKxybW1bOp9Mbqvz.',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-06 08:58:49','2016-01-06 08:58:50',NULL,NULL),(115,'pia manna','pia','manna','2016-01-06 18:08:17','pia.manna@danahall.org','$2y$10$C8agzE/VpckTB56tg1nl/.rgefE2OQpd8KVQjvcgk4An/Bs6W7nOi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-06 23:08:15','2016-01-06 23:08:17',NULL,NULL),(116,'eric eric','eric','eric','2016-01-09 05:07:04','eric@etr.org','$2y$10$QdbX2d9l8c/4ZKM54FKw.O2Om6avNN.eQYN3fyaQgnZCVEaUu1Daq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-08 23:09:31','2016-01-09 10:07:04',NULL,NULL),(117,'nlopez nlopez','nlopez','nlopez','2016-01-27 21:00:52','nlopez@chshel.org','$2y$10$lbZzH0UVJzmgZByRMzeRLuEQDVZasRGniSkH8EG8.GMOQomjUA95e',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-12 23:49:32','2016-01-28 02:00:52',NULL,NULL),(118,'elyse gruttadauria','elyse','gruttadauria','2016-01-12 21:39:48','elyse.gruttadauria@bcc.cuny.edu','$2y$10$Z2Re8xxAhYabvltTj1dgAenFgd7S/kizIfkQgvHtP8PM2yge/HRLy',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-13 02:13:29','2016-01-13 02:39:48',NULL,NULL),(119,'caroline horrigan29','caroline','horrigan29','2016-01-12 22:12:49','caroline.horrigan29@gmail.com','$2y$10$yXCz4dm9P.piUM4kcq3Freli7ycvP.P4K.hWwJKZpRm1poZwlq7Xq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-13 03:12:49','2016-01-13 03:12:49',NULL,NULL),(120,'forerol forerol','forerol','forerol','2016-01-13 21:15:01','forerol@uthscsa.edu','$2y$10$dNw2wlr7U5I1tVQnOG9E2u96ljJVTLachlfGzMthMg23sWUFsICsy',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-14 02:15:00','2016-01-14 02:15:01',NULL,NULL),(121,'gonzalezb4 gonzalezb4','gonzalezb4','gonzalezb4','2016-01-14 18:55:47','gonzalezb4@uthscsa.edu','$2y$10$AvAVfXUCZqFbPVMnKdW/r.hE1J4Xaq.nsMhzexHTkwoTMCTgj8n9C',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-14 20:27:58','2016-01-14 23:55:47',NULL,NULL),(122,'clewis clewis','clewis','clewis','2016-01-27 19:33:27','clewis@chshel.org','$2y$10$05TPiK6T2V2QhuXQIkGPx.RwIeNQPherAdce3aM0LkRcilxFVQoTu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-14 21:32:59','2016-01-28 00:33:27',NULL,NULL),(123,'mcornelius mcornelius','mcornelius','mcornelius','2016-01-20 18:54:49','mcornelius@health.nyc.gov','$2y$10$Rlu25CknrE7lpdqG/Od9UuYPX64nuPZsKnQJ9ZozScUoIqGAILuAW',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-14 23:36:06','2016-01-20 23:54:49',NULL,NULL),(124,'slosoya slosoya','slosoya','slosoya','2016-01-27 19:34:41','slosoya@chshel.org','$2y$10$X0HzaTa46ykvaqNO6qS.wO/mPQ50FEK8W8Rw3hU0cZZ8bBX8xKBF6',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-15 22:29:24','2016-01-28 00:34:41',NULL,NULL),(125,'claredisalvo claredisalvo','claredisalvo','claredisalvo','2016-01-18 14:54:18','claredisalvo@gmail.com','$2y$10$VSD3WCwaxxwTUIDEr.u1Su.JSUgiVhA9yVPBxn6akiK8dBrnzpe0y',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-16 01:59:16','2016-01-18 19:54:18',NULL,NULL),(126,'Rcasteel Rcasteel','Rcasteel','Rcasteel','2016-02-12 00:16:39','Rcasteel@nvhealthcenters.org','$2y$10$EOm3YjWcgXBjNBgEdvS4Z.FKGFQ9RxQiPVwByUrcMG84cOg6hI06O',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-16 02:31:54','2016-02-12 05:16:39',NULL,NULL),(127,'rburdine rburdine','rburdine','rburdine','2016-05-09 18:04:18','rburdine@nvhealthcenters.org','$2y$10$hI0Dei3mjeJ8r1NiU94abOStg3IRJRK9XPfSFc5EOk.9roMX8nnOu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-16 02:56:08','2016-05-09 22:04:18',NULL,NULL),(128,'garzagaribay garzagaribay','garzagaribay','garzagaribay','2016-01-21 19:58:14','garzagaribay@uthscsa.edu','$2y$10$pAAe7n.tsqRMh4.NDvlymeGiCuAQMaW80R5XH8.6jkMTbTrZzPSdG',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-19 23:06:17','2016-01-22 00:58:14',NULL,NULL),(129,'offfwhite offfwhite','offfwhite','offfwhite','2016-01-20 15:32:35','offfwhite@gmail.com','$2y$10$G.tldnEv4fZ/fJ46/Z6mU.fpeteF3mdT543KTn9r.cI4JUB3ZVTpy',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-20 20:32:35','2016-01-20 20:32:35',NULL,NULL),(130,'meredith cornelius','meredith','cornelius','2016-05-05 15:43:27','meredith.cornelius@gmail.com','$2y$10$.7a1z3iQvU0wvdgQUp/fXuEvOF60XJNBRrO4S7ezlEladC8nsxuRa',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-20 23:26:09','2016-05-05 19:43:27',NULL,NULL),(131,'riosa3 riosa3','riosa3','riosa3','2016-01-26 21:38:23','riosa3@uthscsa.edu','$2y$10$rbf6oTHIXnwnbMf9pswuyu2Oneb1ZESs5I69tQ7s66F2Z64jNYm4G',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-21 00:48:12','2016-01-27 02:38:23',NULL,NULL),(132,'janetrh1 janetrh1','janetrh1','janetrh1','2016-01-21 17:35:22','janetrh1@optonline.net','$2y$10$cObUsn0aHdN2f/Dgvz0f/OH1LJnRpxVulwEMXJ97mviTNRw0uOfLi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-21 22:35:22','2016-01-21 22:35:22',NULL,NULL),(133,'teodosioa teodosioa','teodosioa','teodosioa','2016-01-21 20:07:12','teodosioa@uthscsa.edu','$2y$10$6Q25ar0jbaVHrsS.fWgJAej9ZuokGCTg6vupV02lJ9Mmnvd1xgMV.',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-22 00:59:21','2016-01-22 01:07:12',NULL,NULL),(134,'laforero laforero','laforero','laforero','2016-02-04 22:00:28','laforero@gmail.com','$2y$10$KZu5rAeUn9tNd6nbLrB4A.Qim265u65F6NuxyJgBnOKvKoCcGpw4e',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-22 01:07:44','2016-02-05 03:00:28',NULL,NULL),(135,'travis+1 travis+1','travis+1','travis+1','2016-02-18 21:36:10','travis+1@example.com','$2y$10$dYJlYOvK4nQQY46yzE6Pk.uX5Ve3P3Uh1DS/vowrsTpE/I7qTU8RG',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-22 02:43:52','2016-02-19 02:36:10',NULL,NULL),(136,'Kmiller6422 Kmiller6422','Kmiller6422','Kmiller6422','2016-05-11 03:03:57','Kmiller6422@gmail.com','$2y$10$DpHxB3U7RP26ugN.jACmXe/0LLwiWaVk7qHXo7o/Ax.G8lRmDAlE.',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-22 08:58:33','2016-05-11 07:03:57',NULL,NULL),(137,'gribblet gribblet','gribblet','gribblet','2016-01-26 17:52:32','gribblet@gmail.com','$2y$10$JXnKOtFaKRic4p1Ycbz.4uxwL5ccnjI4I/nUVV5KE4ncuStZnNcei',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-26 05:27:21','2016-01-26 22:52:32',NULL,NULL),(138,'arh8760 arh8760','arh8760','arh8760','2016-01-26 17:17:56','arh8760@gmail.com','$2y$10$M/mml1frxf6EXMpBbdXsbuHukAvcqZ.G7rCGnb2ptyDv6wsGzgl3K',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-26 22:17:55','2016-01-26 22:17:56',NULL,NULL),(139,'nlgonzales nlgonzales','nlgonzales','nlgonzales','2016-01-27 19:33:40','nlgonzales@chshel.org','$2y$10$D8vkmeKw7g.2dYJIqxaOcO24ZuWn5M11r3LkoQtfeT60AjtbLrgGy',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-26 23:28:18','2016-01-28 00:33:40',NULL,NULL),(140,'renesm renesm','renesm','renesm','2016-01-26 19:51:22','renesm@battlers.ab.edu','$2y$10$zPcEkcfozAQD.ZOkQ6cYWu6Khl7fbk4Ox7w5GmpZBLIgNDo/bzhRG',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 00:51:22','2016-01-27 00:51:22',NULL,NULL),(141,'mandylanyon mandylanyon','mandylanyon','mandylanyon','2016-01-26 20:15:25','mandylanyon@gmail.com','$2y$10$HRcnBPgBpjR3DLve8w2/ee1ppQ4Kxiv57bRirr1OZsfijXpl3qGUq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 01:15:25','2016-01-27 01:15:25',NULL,NULL),(142,'dgarza dgarza','dgarza','dgarza','2016-01-27 17:04:31','dgarza@chshel.org','$2y$10$VRMsVVyhvKyay7Hsd./.s.5cGnBnvLlgpGbV4O88oaQgPgVn5OGwC',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 01:46:26','2016-01-27 22:04:31',NULL,NULL),(143,'jeffriese jeffriese','jeffriese','jeffriese','2016-01-26 21:58:27','jeffriese@uthscsa.edu','$2y$10$7WFmGlOE3JxsDSyzNL2aXuazK12LqQajO18fkuoYNQmiZpgKlnM4u',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 02:56:58','2016-01-27 02:58:27',NULL,NULL),(144,'amontez amontez','amontez','amontez','2016-01-27 02:01:20','amontez@chshel.org','$2y$10$1FyP4Hp/p11KfeLJ.aiLNuGJiOYl4zcpn5rB3lz1.emT9tAwkRxJG',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 07:01:19','2016-01-27 07:01:20',NULL,NULL),(145,'nvelasquez nvelasquez','nvelasquez','nvelasquez','2016-01-27 19:34:59','nvelasquez@chshel.org','$2y$10$Gumw4pjZQQBW7eOE73s1purlw0qh5zM9U9bWtVqK8CKFamlKp7oTu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 09:17:03','2016-01-28 00:34:59',NULL,NULL),(146,'malvarado malvarado','malvarado','malvarado','2016-01-27 19:35:41','malvarado@chshel.org','$2y$10$.MXN8r0u5/vvQ3uFGN.AVuGzIDJg1BjRkM90ffd1m77ID2eVMYY9y',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 09:20:44','2016-01-28 00:35:41',NULL,NULL),(147,'tmiller tmiller','tmiller','tmiller','2016-01-27 19:36:09','tmiller@chshel.org','$2y$10$aPbT.Dy/6d4fUjFHsaz5.evC2qkW3HUaBOyADzu.5R61mzup190L2',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 09:43:18','2016-01-28 00:36:09',NULL,NULL),(148,'sspratt sspratt','sspratt','sspratt','2016-01-27 17:03:57','sspratt@chshel.org','$2y$10$N9FLLTlHWTZQWd8ZZ8DZReU0Fw.qzztMMf02BYM3iKu/Y5zSpXGTq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 12:22:07','2016-01-27 22:03:57',NULL,NULL),(149,'jmunguia jmunguia','jmunguia','jmunguia','2016-01-27 19:35:07','jmunguia@chshel.org','$2y$10$iDxfd6VqXujeuYuaghk9YOLy5CpxdSiz2lbDtRHtJVhUm0NfMEG8i',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 12:51:32','2016-01-28 00:35:07',NULL,NULL),(150,'sspratt sspratt','sspratt','sspratt','2016-01-27 08:13:54','sspratt@chshl.org','$2y$10$tx1nIOeZDbL4A.yWuVV4Tui/1V/HwnJerp8qfyI4JXjq0A6DjRM4G',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 13:13:54','2016-01-27 13:13:54',NULL,NULL),(151,'cweise cweise','cweise','cweise','2016-01-27 19:38:30','cweise@chshel.org','$2y$10$vrE9G1AfZi/hD2lGgBCzousK7XEWVrTdRTGFhh3ejC.4CAsACv7xy',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 19:34:58','2016-01-28 00:38:30',NULL,NULL),(152,'smedina smedina','smedina','smedina','2016-01-27 14:51:02','smedina@chshel.org','$2y$10$7hu7kjzXwFs5u/EmMX5AuO0k4l2tplAhrSxUOk1CgjkJkehtxevje',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 19:50:21','2016-01-27 19:51:02',NULL,NULL),(153,'lreyes lreyes','lreyes','lreyes','2016-01-27 19:36:30','lreyes@chshel.org','$2y$10$hK4.tWyo23gLvacmFS95Z.VacMKNsDdMwMbWc4ipaMxirU8Y6CWVi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 19:58:02','2016-01-28 00:36:30',NULL,NULL),(154,'mlgarcia mlgarcia','mlgarcia','mlgarcia','2016-01-27 15:11:19','mlgarcia@chshel.org','$2y$10$MkgbRD9Hl2XflFaYILZbVOwGprX6g0zFi5XCskxQeIq7bOGmnCMVS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-27 20:11:19','2016-01-27 20:11:19',NULL,NULL),(155,'mattra mattra','mattra','mattra','2016-01-27 19:36:35','mattra@bcfs.net','$2y$10$1506byz9gt7DTvKYyG/dIO1c5wDdqIG0EExrv4bT6zUpYg2k0cWJO',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-28 00:31:55','2016-01-28 00:36:35',NULL,NULL),(156,'stacy lee','stacy','lee','2016-01-27 19:37:19','stacy.lee@bcfs.net','$2y$10$v1HSt4H4gpgSywMhe1ZLl.4Wj5W4.5ZRwiiS..vdfHWNvDqNncEoy',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-28 00:35:07','2016-01-28 00:37:19',NULL,NULL),(157,'vickievibes6188 vickievibes6188','vickievibes6188','vickievibes6188','2016-01-28 02:15:22','vickievibes6188@gmail.com','$2y$10$OzvEIfpjrekSCqfYtBd/7OyuJu/YT7LX5EFDF3ud.umH1p0ndYtuK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-28 07:15:22','2016-01-28 07:15:22',NULL,NULL),(158,'mariselasotelo mariselasotelo','mariselasotelo','mariselasotelo','2016-01-28 03:10:11','mariselasotelo@hotmail.com','$2y$10$QqPR.ud92DaudYnoDnNMK.AMK598BFbYhhOyWkBvrqxG9mSp0SX9C',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-28 08:10:10','2016-01-28 08:10:11',NULL,NULL),(159,'spach spach','spach','spach','2016-01-28 14:50:04','spach@jhu.edu','$2y$10$wMVBXLFmu.Gw51ZzDJNNKeYLVwYvf0y8HDQnngZrfBoQ8D.a/pnh6',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-28 19:50:03','2016-01-28 19:50:04',NULL,NULL),(160,'hobgood a','hobgood','a','2016-01-29 01:01:51','hobgood.a@vmail.com','$2y$10$YVMgiZtuUhCkq0Uk7LzPBuEqxRRKimgtSAg6CZ5PwoYkX3SB2UsKy',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-29 06:01:51','2016-01-29 06:01:51',NULL,NULL),(161,'maritza051991 maritza051991','maritza051991','maritza051991','2016-01-30 20:50:28','maritza051991@icloud.com','$2y$10$7UCyVN3Ga1V9bIMT.8awbOILVKlSLGMHUDyaGEKhuQ4YsTIDoRq/u',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-01-31 01:50:28','2016-01-31 01:50:28',NULL,NULL),(162,'denisem357 denisem357','denisem357','denisem357','2016-01-31 21:15:41','denisem357@gmail.com','$2y$10$lp94.v0N2OibkXRBUceF2eSgjOpNBP0Y.3Ga.tXS/g8AxxJWDga0q',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-01 02:15:41','2016-02-01 02:15:41',NULL,NULL),(163,'krens krens','krens','krens','2016-02-01 19:21:02','krens@embarqmail.com','$2y$10$lANJzBWDVvQt9RWKAAGktelit4m5Y/FE5tza8FLPxHdm.7VdKU.5e',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-02 00:21:02','2016-02-02 00:21:02',NULL,NULL),(164,'linnie dobbins','linnie','dobbins','2016-02-01 20:48:00','linnie.dobbins@yahoo.com','$2y$10$mv3HC/osfKf/eK.Q8wkEbeWiUGWrIpBkF4W1f/RtUgDMs7HWfKFUi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-02 01:47:59','2016-02-02 01:48:00',NULL,NULL),(165,'georgealonzo2006 georgealonzo2006','georgealonzo2006','georgealonzo2006','2016-02-12 19:16:33','georgealonzo2006@yahoo.com','$2y$10$l7a5kWqn1u4ZJ1fGtXwQ.O4VF/Of5UlyREf2GcPEmzMkZdHPuUA9S',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-02 23:55:48','2016-02-13 00:16:33',NULL,NULL),(166,'haleydenny92 haleydenny92','haleydenny92','haleydenny92','2016-05-11 19:32:33','haleydenny92@gmail.com','$2y$10$ZAwdBBTOw3PaupgGeteIMOWoNpU8FkVL7F7h7t3RMR6OonOIgjBte',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-03 01:00:23','2016-05-11 23:32:33',NULL,NULL),(167,'sarahkiser sarahkiser','sarahkiser','sarahkiser','2016-02-04 16:39:11','sarahkiser@gmail.com','$2y$10$1ZvhTMpQ1AQgZTRhVu5Lj.RqLjs2rOX5Zo7nWIb3tmi9AHvUq9CO2',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-03 18:28:15','2016-02-04 21:39:11',NULL,NULL),(168,'crcantu crcantu','crcantu','crcantu','2016-02-12 19:21:08','crcantu@chshel.org','$2y$10$kQschTZDfYiDUFImhYRtnetvqlsB/CTl8pEWv7XXwRI7eMkFEfJFa',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-03 21:57:40','2016-02-13 00:21:08',NULL,NULL),(169,'vdenoon vdenoon','vdenoon','vdenoon','2016-02-04 15:02:19','vdenoon@myrhc.org','$2y$10$2m25Q5WE5jmasmByxq8UmOr8p53sYUBNjU5EFFyL1tWBXVdaZomEq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-03 23:09:58','2016-02-04 20:02:19',NULL,NULL),(170,'klambrecht klambrecht','klambrecht','klambrecht','2016-02-03 20:24:07','klambrecht@nvpca.org','$2y$10$MoH.kLGvc4W6nEyvDgVOkugA/9aj1RzZ52ZXm1NzVDnuNbo0OQdgK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-04 01:24:06','2016-02-04 01:24:07',NULL,NULL),(171,'jfsoxfenway8 jfsoxfenway8','jfsoxfenway8','jfsoxfenway8','2016-02-06 22:57:08','jfsoxfenway8@gmail.con','$2y$10$9Glfuw.qQcdN2VBCnT6p7ep4BeeOJJI0hDUle.NBw4aey23/zP.fC',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-07 03:57:08','2016-02-07 03:57:08',NULL,NULL),(172,'x x','x','x','2016-02-10 20:27:17','x@x.com','$2y$10$OW4PKBAMELImsrI5mvKEA.qSpWeDFT6zilTGJ929B2z50SecBlyKa',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-11 01:27:16','2016-02-11 01:27:17',NULL,NULL),(173,'sz0914 sz0914','sz0914','sz0914','2016-02-12 19:18:17','sz0914@bcfs.net','$2y$10$hrybJ13BaoEAVzQxFc4wlefXbAHvRVztJfmcwNgae6OMMObn1tURq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-11 20:58:40','2016-02-13 00:18:17',NULL,NULL),(174,'bs0116 bs0116','bs0116','bs0116','2016-02-13 17:52:37','bs0116@bcfs.net','$2y$10$aHPM1yc2HIzpifqnGSmyluMCCl9wj7ckNiNmC9zKne15AD169Qiwy',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-11 21:00:56','2016-02-13 22:52:37',NULL,NULL),(175,'tgonzalez tgonzalez','tgonzalez','tgonzalez','2016-02-12 19:21:18','tgonzalez@chshel.org','$2y$10$gbNyiUKjwgr2yonOlFXcy.OKSCtQ4BLDMGsEoE19983gpSLnnfwNu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-11 23:30:56','2016-02-13 00:21:18',NULL,NULL),(176,'aisabelle aisabelle','aisabelle','aisabelle','2016-02-12 19:19:53','aisabelle@chshel.org','$2y$10$ezS3YsJIpOCNhHE71Pd24.m.sZRsGrGbnuUt.Y3EUTwbt8wE1Uiuu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-12 06:21:20','2016-02-13 00:19:53',NULL,NULL),(177,'park26sec park26sec','park26sec','park26sec','2016-02-27 00:33:41','park26sec@hotmail.com','$2y$10$8IRn/UJ11Gssu1xUpOMsR.I8KoqYqdOcBgBk/FyFS6iLhMhaKIPzy',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-12 07:08:58','2016-02-27 05:33:41',NULL,NULL),(178,'sr0114 sr0114','sr0114','sr0114','2016-02-12 19:19:21','sr0114@bcfs.net','$2y$10$mIFuy32ox3NRmpFZPurXVuBTEg2nYPZ5d0oF6.LKUZhXsULylcipi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-12 08:23:03','2016-02-13 00:19:21',NULL,NULL),(179,'victorious414 victorious414','victorious414','victorious414','2016-02-12 19:23:19','victorious414@gmail.com','$2y$10$08.Ujthhe.mPwD156xThoOmy/MYRAS4JbAzudvHZoGNATXLtnfbtS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-12 18:21:49','2016-02-13 00:23:19',NULL,NULL),(180,'leawalberg leawalberg','leawalberg','leawalberg','2016-02-12 19:15:59','leawalberg@gmail.com','$2y$10$Rg7nsMueCPa9lQAfYtqoL.azBpIGUWVG82FD6r.0e9yMKiuF5RcYS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-12 19:24:19','2016-02-13 00:15:59',NULL,NULL),(181,'bcfs01 bcfs01','bcfs01','bcfs01','2016-02-12 16:31:46','bcfs01@icloud.com','$2y$10$GMSE9RCAbHp6XZusBrSfhOaXUpwgOIuAievNSds2ksMdVoAqUgDzS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-12 21:31:45','2016-02-12 21:31:46',NULL,NULL),(182,'etorres etorres','etorres','etorres','2016-02-12 17:36:35','etorres@nvpca.org','$2y$10$fxJ5D9ADQM54fje7jz2LOehrerZpq8CqB/NJqSO7B/6kPWaVDdssS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-12 22:36:35','2016-02-12 22:36:35',NULL,NULL),(183,'hbrown hbrown','hbrown','hbrown','2016-02-12 19:16:01','hbrown@chshel.org','$2y$10$r7IU0WDXnbhpTBX5.ZCBSuejNOFTzHqy85e2d9/O9BimC7uojIaZ2',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-13 00:16:01','2016-02-13 00:16:01',NULL,NULL),(184,'rachael fletcher','rachael','fletcher','2016-02-12 19:16:22','rachael.fletcher@bcfs.net','$2y$10$jbvt9Izut13.uX0iyd1GYO/fIDHKgQgsROabF4iYyOuQEJHnz.TSG',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-13 00:16:21','2016-02-13 00:16:22',NULL,NULL),(185,'vsilva vsilva','vsilva','vsilva','2016-02-12 19:16:40','vsilva@bcfs.net','$2y$10$MmwfAyluGMTQ0L0d0Q12sOUX4WtLZpmOL3FhQ2upccFW5oAcL4dQC',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-13 00:16:40','2016-02-13 00:16:40',NULL,NULL),(186,'bethaneybailey bethaneybailey','bethaneybailey','bethaneybailey','2016-02-12 19:17:01','bethaneybailey@gmail.com','$2y$10$5d7q22tpzQ0a7rleTV6f..EeiNSqtVm8xPudW4I2ygA6cMyE0G0Rm',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-13 00:17:01','2016-02-13 00:17:01',NULL,NULL),(187,'jaiyagirl jaiyagirl','jaiyagirl','jaiyagirl','2016-02-13 20:47:46','jaiyagirl@mac.com','$2y$10$iFBRiK0Nxq0OwUoT.F4PJe1uJmath1qWpcybD0uH60JNGo1gS26da',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-14 01:47:46','2016-02-14 01:47:46',NULL,NULL),(188,'jleovy jleovy','jleovy','jleovy','2016-02-13 22:51:51','jleovy@nvhealthcenters.org','$2y$10$9jRhFS3BELv.7fXPbilD6u42HJH91XFvKHQiCJcjb0F3hHbBJ7yfa',0,1,NULL,NULL,NULL,'JDJ5JDEwJDZlaVB4RlhLRDZuVUw3cnZDVGhnMmVGc21WaU5aZEg3N3dxeG9saXNMbzJJa2suWGFGMnlT',NULL,NULL,NULL,NULL,'2016-02-14 03:51:51','2016-02-20 21:59:11',NULL,NULL),(189,'marcyk marcyk','marcyk','marcyk','2016-02-16 15:52:11','marcyk@austinpcc.org','$2y$10$HnW0kEVj44gy1x6JvIYCCuLl.2fhY4XapejD.mq6GjKC49pY.U9Ua',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-14 20:55:09','2016-02-16 20:52:11',NULL,NULL),(190,'klcotter klcotter','klcotter','klcotter','2016-02-17 18:12:58','klcotter@buffalo.edu','$2y$10$94kcehPRCZLmKiFL/cn03eu1EaPrh0HN7XXR6KOa9C3CDOg7tdnty',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-15 07:51:21','2016-02-17 23:12:58',NULL,NULL),(191,'craig lecroy','craig','lecroy','2016-02-24 23:25:03','craig.lecroy@asu.edu','$2y$10$4Z.dxqK0BZSzRB6V7GWyiumLqltLYsgg62KHnTO2Z/iMJrPrmnc6a',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-16 03:54:48','2016-02-25 04:25:03',NULL,NULL),(192,'corbinjohnadam corbinjohnadam','corbinjohnadam','corbinjohnadam','2016-02-15 23:51:20','corbinjohnadam@yahoo.com','$2y$10$1FL3iTa1gPBDenxlfYj2X.Kgi6emaYaywPq8cx9CQZ7HhVLxDfYfi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-16 04:51:19','2016-02-16 04:51:20',NULL,NULL),(193,'submulocsam submulocsam','submulocsam','submulocsam','2016-02-17 01:31:42','submulocsam@hotmail.com','$2y$10$4ENbCdRp8UIBPCb7mDSJzecrRbNN2.yNTEuW12L7GGEUOtGEuXv/.',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-17 06:31:41','2016-02-17 06:31:42',NULL,NULL),(194,'lacroix_ali lacroix_ali','lacroix_ali','lacroix_ali','2016-02-17 02:33:13','lacroix_ali@yahoo.com','$2y$10$lEuftQUaDHQgaD7cZ.n6fud8K/twVdjQ3oTGqkTNVgr9eDGt44fpu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-17 07:33:12','2016-02-17 07:33:13',NULL,NULL),(195,'chelseachall chelseachall','chelseachall','chelseachall','2016-02-17 02:53:35','chelseachall@gmail.com','$2y$10$bwUUymvq5vzI2xbbFeqW.eozKoatqeKouhFGyG5u4t3QVoZ0noJHC',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-17 07:53:35','2016-02-17 07:53:35',NULL,NULL),(197,'debbie_nance debbie_nance','debbie_nance','debbie_nance','2016-02-17 17:45:13','debbie_nance@hotmail.com','$2y$10$E4.m6.WSE0VX5ndw8Sz83.NKBNXHft3dH/EOE3pT3H/Q9vaqMUm8e',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-17 22:45:12','2016-02-17 22:45:13',NULL,NULL),(198,'cehall8 cehall8','cehall8','cehall8','2016-02-17 18:08:07','cehall8@asu.edu','$2y$10$FsM9NTMWRsM/Pa60QP4ca.xcb89LE7clU4lVtyTytV.BNlnEcldsO',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-17 23:08:06','2016-02-17 23:08:07',NULL,NULL),(199,'lux lux','lux','lux','2016-02-18 16:42:06','lux@thatstrangegirl.com','$2y$10$E5Pp7MnzuisE4zu.HLlTluQr0IujbJYWsCxv643iX6xi5Y78liAVO',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-18 21:06:08','2016-02-18 21:42:06',NULL,NULL),(201,'eeeemail eeeemail','eeeemail','eeeemail','2016-02-19 00:36:24','eeeemail@mac.com','$2y$10$js7SMv1YTXD0Vioy.w7ciOd6EYbb4aOw6wGQlcitgSePBHVNu/vHe',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-19 05:36:24','2016-02-19 05:36:24',NULL,NULL),(202,'jgleovy jgleovy','jgleovy','jgleovy','2016-02-21 14:42:30','jgleovy@yahoo.com','$2y$10$HyXQC8zTXWIb029Q.ZQ9beK86wPm9fQ9cEnD0VW3YrMDA0uIoaB0m',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-20 22:11:18','2016-02-21 19:42:30',NULL,NULL),(203,'wmongan wmongan','wmongan','wmongan','2016-02-23 13:33:48','wmongan@students.fairmontstate.edu','$2y$10$ah0GN6DM7mXCZtYcCYLgiOre5v/bLWDkHDloe4DKX05tQlmQlXGnC',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-23 18:33:48','2016-02-23 18:33:48',NULL,NULL),(204,'learys2000 learys2000','learys2000','learys2000','2016-02-23 13:53:06','learys2000@gmail.com','$2y$10$3kTUtT3183RBMEzMpaMvl.ZGlk/RfRQTeqS7gTqAOY0HURCsX.IRC',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-23 18:53:06','2016-02-23 18:53:06',NULL,NULL),(205,'downs downs','downs','downs','2016-05-18 13:17:09','downs@cmu.edu','$2y$10$BW6S7mLY2AXKpfw7etYJlOhIrjYIcngkO/glHpfaeoTb9Fnzda1dK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-24 01:30:23','2016-05-18 17:17:09',NULL,NULL),(206,'tinylittletiney tinylittletiney','tinylittletiney','tinylittletiney','2016-02-27 01:19:56','tinylittletiney@yahoo.com','$2y$10$ZKoBxWr/FWAdMF2wsy10De1oqL/egWKCMLqAteHHmWeBFb.pj1XCq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-25 18:03:12','2016-02-27 06:19:56',NULL,NULL),(207,'chelle chelle','chelle','chelle','2016-02-25 16:44:51','chelle@birth-matters.org','$2y$10$njCW4pRSBVqPtibCfnJ66.G3sItnv6cFUl.FS/Rc76GSKFWlq8B3u',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-25 21:38:30','2016-02-25 21:44:51',NULL,NULL),(208,'charlie charlie','charlie','charlie','2016-02-26 14:39:39','charlie@openarc.net','$2y$10$AhCDIFO2iJq2ksGihTlRVe.TwpqYzcpuNT6qtMYF2Oe5WzGvFPZk2',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-26 19:39:39','2016-02-26 19:39:39',NULL,NULL),(209,'whitedb whitedb','whitedb','whitedb','2016-02-26 15:20:12','whitedb@upmc.edu','$2y$10$hoEDHrNn.MNy171rr1tJ3.oeek.0YFLJjrL1XJYxHhL8tsvl9.WwW',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-26 20:20:12','2016-02-26 20:20:12',NULL,NULL),(210,'kristopher yoon','kristopher','yoon','2016-02-26 20:29:19','kristopher.yoon@gmail.com','$2y$10$1dHki1IBhsnCoQBktG8En.6p4W0ssTOHwaDDyi0DA9gtuRK1lpF.6',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-27 01:29:18','2016-02-27 01:29:19',NULL,NULL),(211,'robertkeet robertkeet','robertkeet','robertkeet','2016-03-01 15:56:12','robertkeet@yahoo.com','$2y$10$whzKEkP2T3/8ztoZyPeDIOaRVVmqnMJ1geDOiEpIqs5nqNqC3oNAO',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-02-27 05:09:39','2016-03-01 20:56:12',NULL,NULL),(212,'amberleighanne1995 amberleighanne1995','amberleighanne1995','amberleighanne1995','2016-03-01 00:06:58','amberleighanne1995@gmail.com','$2y$10$tUan.i/gGaszlyea/EYESOxWuUsIuMyvaiwk7XaID.MBX35IPQd.W',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-03-01 05:01:49','2016-03-01 05:06:58',NULL,NULL),(213,'jstivason jstivason','jstivason','jstivason','2016-03-02 23:55:40','jstivason@ymail.com','$2y$10$fTXLScA/uDse06Vu0uQz/ud8pZCy8wpm1JNHotvqzIquxZIP01jOi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-03-02 08:14:44','2016-03-03 04:55:40',NULL,NULL),(214,'testadmin','Test','Admin',NULL,'test@example.com','$2y$10$Q5ayhA2En22rAWD2K2GXNufUS2ffBIi9IRCOZS768kYh3DWJN.ye6',1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-03-03 22:52:08','2016-03-03 22:52:08',NULL,NULL),(215,'Travis','Travis','Loncar',NULL,'travis+1@openarc.net','$2y$10$y/eLN.7pSpwMQjgsdrtV/uLhRDObjj3r/6pLB9JxXuGsi51yBtzMS',1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-03-03 22:52:51','2016-03-03 22:52:51',NULL,NULL),(216,'travis','Travis','Loncar','2016-03-03 18:11:36','travis+2@openarc.net','$2y$10$f4fxn4ihhEkN5Uq1iIryRegey4aOOc2K0QJn5ijgMBd3J7kncRZRq',1,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-03-03 22:58:42','2016-03-03 23:11:36',NULL,NULL),(217,'travis','Travis','Loncar',NULL,'travis+3@openarc.net','$2y$10$Z0JJjGt1BvxWt8lJySVlCeJP7SS3oaWBcynXM5TaNoVZYR2/Htwz6',1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-03-03 23:00:33','2016-03-03 23:00:33',NULL,NULL),(218,'travis','Travis','Loncar',NULL,'travis+4@example.com','$2y$10$Cw1R4qNB9gLmWO4tlnEpjel/Jyd75eMJnXy9UkhE0afDDoUqGO8g2',1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-03-03 23:03:07','2016-03-03 23:03:07',NULL,NULL),(219,'test test','test','test','2016-06-13 20:17:53','test@test.com','$2y$10$S7KVn7u4.lC1aRA6FlkypO4DdlrdMaOBho39WkqW8gxr87N93luEi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-03-10 00:47:35','2016-06-14 00:17:53',NULL,NULL),(220,'ryanmilstead ryanmilstead','Ryan','Milstead','2016-03-22 19:45:56','ryanmilstead@openarc.net','$2y$10$DqqwuylspjEHomrb/yj2duCZ54k8EzyIVehSJNRKTo1TfqfxcUxrC',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-03-21 22:24:18','2016-03-22 23:45:56',NULL,220),(221,'michaelquach michaelquach','michaelquach','michaelquach','2016-05-24 21:51:38','michaelquach@openarc.net','$2y$10$9iKDLQ.RJ887SFgGNhMspezZNwkGPQGVOsOoZ4ryJ2zRWAqcmeYRS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-03-22 23:27:13','2016-05-25 01:51:38',NULL,NULL),(222,'mandy mandy','mandy','mandy','2016-05-20 15:56:21','mandy@cmu.edu','$2y$10$nj8aw8M43sKBcWZgLIOWqOdJOncQ/uHHvVpOFuodUwjrTjv00TfZS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-04 23:01:03','2016-05-20 19:56:21',NULL,NULL),(223,'mandyholbrook mandyholbrook','mandyholbrook','mandyholbrook','2016-04-04 19:08:01','mandyholbrook@gmail.com','$2y$10$mZvh.LQGpkI7tP28Q1gkuOu7/TBPo174cQxDVmOns4j75BUBEj9jS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-04 23:03:37','2016-04-04 23:08:01',NULL,NULL),(224,'will mandy','will','mandy','2016-04-05 14:51:04','will.mandy.lanyon@gmail.com','$2y$10$.8uDu/XgfFqxI0WARxLQwOoYATr.TU.vnpA6Z6R0IxrdJobJKzpma',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-05 18:51:04','2016-04-05 18:51:04',NULL,NULL),(225,'Talana williams23','Talana','williams23','2016-04-27 14:51:50','Talana.williams23@gmail.com','$2y$10$S2fxxSemoWzp7v1MBUHsaeT9KZFT9NdXGHVNXttbj43V5VuxmHNBu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 18:35:11','2016-04-27 18:51:50',NULL,NULL),(226,'leximosley17 leximosley17','leximosley17','leximosley17','2016-04-27 14:51:26','leximosley17@yahoo.com','$2y$10$Nr7jVp/ZWN.VyC8YEMpJIu4l3UKJlTcgV1oKq8BWUMihXFryiIi0m',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 18:35:31','2016-04-27 18:51:27',NULL,NULL),(227,'Latrecebrimage Latrecebrimage','Latrecebrimage','Latrecebrimage','2016-04-14 18:00:24','Latrecebrimage@hotmail.com','$2y$10$H9KZ9uYd7GJiu4mlA4HhceBM0XNuftsUSqre0UA5wgJwGMjSzber6',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 18:35:33','2016-04-14 22:00:24',NULL,NULL),(228,'christina wright1770','christina','wright1770','2016-04-27 14:53:44','christina.wright1770@gmail.com','$2y$10$PMWvVYhosKSGkAj2qRwE0.1hP4I4nbhF47GRh.YW6pvQQRcU3YQSu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 18:36:27','2016-04-27 18:53:44',NULL,NULL),(229,'tanejabrown73 tanejabrown73','tanejabrown73','tanejabrown73','2016-04-27 14:56:18','tanejabrown73@gmail.com','$2y$10$NyooYQqufRblC1wZJfwqvOngEBNx/RBdyayVU/GVMQ65ad1ceqdRa',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 18:36:35','2016-04-27 18:56:18',NULL,NULL),(230,'jaleahwebb14 jaleahwebb14','jaleahwebb14','jaleahwebb14','2016-04-27 14:52:39','jaleahwebb14@gmail.com','$2y$10$hIJKUffmkS56TEtGyEkF4OteplaA7H95NI8cPe/vi5EAZlfogtGxK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 18:36:58','2016-04-27 18:52:39',NULL,NULL),(231,'teylormarie teylormarie','teylormarie','teylormarie','2016-04-27 14:53:28','teylormarie@me.com','$2y$10$E2WuLqm3uTE03V5luOIZsOOlFZat7mrFGA7wl5xA5fWP5d3AqiVV.',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 18:37:12','2016-04-27 18:53:28',NULL,NULL),(232,'taylorhines32 taylorhines32','taylorhines32','taylorhines32','2016-04-27 14:51:49','taylorhines32@gmail.com','$2y$10$iwNe/D2dceFMOp.JaDluoe6C3TF58S8ODv4UGpbsN4XeA03zBV3yu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 18:37:44','2016-04-27 18:51:49',NULL,NULL),(233,'nautika8872 nautika8872','nautika8872','nautika8872','2016-04-27 14:51:47','nautika8872@gmail.com','$2y$10$amoDTmC7/0bfeH3yA3x1EuXXdNhV7cb418/iTfkkm1KUDtmX8Sw8y',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 18:37:44','2016-04-27 18:51:47',NULL,NULL),(234,'jamyajohnsonT06 jamyajohnsonT06','jamyajohnsonT06','jamyajohnsonT06','2016-04-27 14:54:14','jamyajohnsonT06@gmail.com','$2y$10$.nEvL33xAInPmpeIh4Nox.7khzdLdmqgTRCdWhfOFgrqUh3YRgvAC',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 18:37:45','2016-04-27 18:54:14',NULL,NULL),(235,'karistanclay karistanclay','karistanclay','karistanclay','2016-04-27 17:48:47','karistanclay@hotmail.com','$2y$10$pXpEAA20erd.WwTINKnq0etlVi2PTxEJOVwEUkjpc3MpGMWe.G6HC',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 22:21:49','2016-04-27 21:48:47',NULL,NULL),(236,'dglenn815 dglenn815','dglenn815','dglenn815','2016-04-27 17:48:38','dglenn815@gmail.com','$2y$10$ux/8UN5KM7bL2q2MPri0v.mqq/QxICrDpLn1B7xBsPcC.BJlHZ5Z.',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 22:22:02','2016-04-27 21:48:38',NULL,NULL),(237,'ellamedina2000 ellamedina2000','ellamedina2000','ellamedina2000','2016-04-27 17:49:03','ellamedina2000@gmail.com','$2y$10$.9eZKrS.CGLneo3CmrteK.oR9297dM5jGwWle5Fp19cFkUDg9efmO',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 22:22:16','2016-04-27 21:49:03',NULL,NULL),(238,'litk always123','litk','always123','2016-04-27 17:48:44','litk.always123@gmail.com','$2y$10$cSO9yuHWci6Nz9NlZdk9EeABYnR9VAkrm7BBiV7H6Gmu73RUX8W.q',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 22:22:23','2016-04-27 21:48:44',NULL,NULL),(239,'corinnegamble corinnegamble','corinnegamble','corinnegamble','2016-04-06 18:23:05','corinnegamble@gmail.com','$2y$10$M.HDvsvRTs7EDhjiWQ4iteOzdktCzdnyiSwdCdonbY6sm/pXLL7Eu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 22:23:04','2016-04-06 22:23:05',NULL,NULL),(240,'glennt616 glennt616','glennt616','glennt616','2016-04-27 17:49:05','glennt616@gmail.com','$2y$10$gfPMeJopU3WrQzCxJJ2.eOine7Ww281d1a1vwPbuomA5w3I5PHB3u',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 22:23:18','2016-04-27 21:49:05',NULL,NULL),(241,'mollygoss mollygoss','mollygoss','mollygoss','2016-04-27 17:49:28','mollygoss@propelschools.org','$2y$10$xWJYdiVggXRvDrwICEXIm.AxTV2WRPqdxyhG71.lWH5OXROkG5oqm',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 22:23:29','2016-04-27 21:49:28',NULL,NULL),(242,'shelbysymone47 shelbysymone47','shelbysymone47','shelbysymone47','2016-04-27 17:53:51','shelbysymone47@gmail.com','$2y$10$f.2dmKbdzxVOq0VDUDmbaetdginjM5Ih59h1VD1dVmZ4y4peNC.ou',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 22:24:07','2016-04-27 21:53:51',NULL,NULL),(243,'jbrentley9 jbrentley9','jbrentley9','jbrentley9','2016-04-27 17:48:33','jbrentley9@gmail.com','$2y$10$hH028BlgAti/srUZDWNHReRQwoZgRZFrGTBkHV6HuuOWJzVfHVJVS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 22:24:12','2016-04-27 21:48:33',NULL,NULL),(244,'juliemarquez juliemarquez','juliemarquez','juliemarquez','2016-04-27 17:51:49','juliemarquez@propelschools.org','$2y$10$iGOMrCFayRg2UdggJJ9aJuXFBmqyOH5QVOTYII9rYvlLrhbqsLUom',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-06 22:24:16','2016-04-27 21:51:49',NULL,NULL),(245,'corinnegamble13 corinnegamble13','corinnegamble13','corinnegamble13','2016-04-27 17:53:28','corinnegamble13@gmail.com','$2y$10$OTBrkxRy0a0obVCKi6RCHOl0JQOo/MCCJcdPzY5Rvqnb8jiLDij4.',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-07 18:30:41','2016-04-27 21:53:28',NULL,NULL),(246,'khorowitz khorowitz','khorowitz','khorowitz','2016-04-11 19:15:20','khorowitz@ppwp.org','$2y$10$XWdRnpjChHuoMtKFHR1/FOZZxZZpaKePCPzYQqeL6rJ8nZjAyknSq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-11 22:33:28','2016-04-11 23:15:20',NULL,NULL),(247,'flatteringrose6506 flatteringrose6506','flatteringrose6506','flatteringrose6506','2016-04-11 22:12:53','flatteringrose6506@gmaio.com','$2y$10$J842AdUi66JniUQ3BCsQmOVkZ.SvIsMkFMQnafKAkYaeAnm0GjO3S',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-12 02:12:52','2016-04-12 02:12:53',NULL,NULL),(248,'kamieramoorefield kamieramoorefield','kamieramoorefield','kamieramoorefield','2016-05-23 21:51:27','kamieramoorefield@yahoo.com','$2y$10$s5SACglsTo9kc5G0qyS2Aumk6vVG4wj4juUjE8YQEJOo3laNJNyUy',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-12 02:12:59','2016-05-24 01:51:27',NULL,NULL),(249,'amarra jade19','amarra','jade19','2016-05-09 22:12:28','amarra.jade19@gmail.com','$2y$10$.fdSsKX1nOVZbjgljATc7uUV8UFa.h440Ksg4xfg2AtzMBKLxTr6m',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-12 02:13:05','2016-05-10 02:12:28',NULL,NULL),(250,'ashaqm ashaqm','ashaqm','ashaqm','2016-05-23 21:51:02','ashaqm@optonline.net','$2y$10$xq2nuve5KekubFRb2Gtv.Oo3w4GhuoXvrzDisvw2Gpk/GoJJ1bFPW',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-12 02:13:07','2016-05-24 01:51:02',NULL,NULL),(251,'destinyjames33 destinyjames33','destinyjames33','destinyjames33','2016-04-25 21:44:14','destinyjames33@yahoo.com','$2y$10$40OooC62yR1pHMZGQWoX1etHBmmevG0xBdJI9tQMhCVQwHpqTGv9m',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-12 02:13:09','2016-04-26 01:44:14',NULL,NULL),(252,'stkklink1 stkklink1','stkklink1','stkklink1','2016-05-23 21:51:11','stkklink1@ppsstudents.net','$2y$10$J7Tly67kKdRnCloMXwPNJu9jgtuXzNUmQjD0.g6XRNw8KgbGQY0fO',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-12 02:13:16','2016-05-24 01:51:11',NULL,NULL),(253,'channy_9 channy_9','channy_9','channy_9','2016-05-23 21:51:22','channy_9@icloud.com','$2y$10$tmpdOunjgF8vPTHz21AwW.PcHwu8mDtesVHGzYdMQuI9ewn3i4T6O',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-12 02:13:40','2016-05-24 01:51:22',NULL,NULL),(254,'afloyd15210 afloyd15210','afloyd15210','afloyd15210','2016-05-23 21:52:26','afloyd15210@gmail.com','$2y$10$ATPfKGG4fjCcEHk7Kf9N0OKeO2ZeaGsdeDQ/8nWmFgl/AfPMBRHsS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-12 02:14:59','2016-05-24 01:52:26',NULL,NULL),(267,'seventeendays.app+viewer@gmail.com','seventeendays.app+viewer','','0000-00-00 00:00:00','seventeendays.app+viewer@gmail.com','$2a$13$AXM6zcgC2HZyoNryPldLDezCXgwzKgs6tl26PQG4BP3nLFiJx3cbq',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:23','2016-04-14 21:25:23',107,NULL),(270,'suemed60','suemedrano','Medrano','0000-00-00 00:00:00','suemedrano@mac.com','$2a$13$lR9no0MVvPWCavc6DicF9eReUoEFeXRydccnekRgvAiTbeFNYH8FG',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:23','2016-04-14 21:25:23',107,NULL),(271,'cmuadmin','CMU','Admin','0000-00-00 00:00:00','seventeendays.app@gmail.com','$2a$13$GR102QwY9J2JILYpVkDkyuGE4kPZCb.tpo7oAxxTuVi5jKXY7gRAW',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:23','2016-04-14 21:25:23',107,NULL),(272,'ordonsd','ordonsd','','0000-00-00 00:00:00','ordonsd@gmail.com','$2a$13$CgmJ9b1/8ZPCMcWtVyicfevoiMfkSq00/MGp104fqSGKf944OMXfq',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:23','2016-04-14 21:25:23',107,NULL),(273,'srush','srush','','0000-00-00 00:00:00','srush@cmu.edu','$2a$13$vyq2MAX8OwRSa0EwP70tlupzPU3y.bvJWpMfSHnlu/BD2trdfTQIi',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:23','2016-04-14 21:25:23',107,NULL),(274,'fcwessels','fcwessels','','0000-00-00 00:00:00','fcwessels@icloud.com','$2a$13$C6p5wVPVf8k.5hhYIunFe../7Aw9qA7UwzRvDZbb5hrQaL9Jd0oX.',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:23','2016-04-14 21:25:23',107,NULL),(275,'snormal','snormal','','0000-00-00 00:00:00','snormal@gmail.com','$2a$13$Pc9.BJ8ulxtYk6whDpFXB.Q2DiC/DdKVXfWxRkSttKLOMyzzVGmie',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:23','2016-04-14 21:25:23',107,NULL),(278,'sbublick','sbublick','','0000-00-00 00:00:00','sbublick@gmail.com','$2a$13$h3CXtikJwHsdXfcMa6u4.uf2E4h4ReeowPphrNxUlNCjR7HOz7lxm',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:23','2016-04-14 21:25:23',107,NULL),(279,'pamelamurray','pamelamurray','','0000-00-00 00:00:00','pamelamurray@gmail.com','$2a$13$jw7Bgy6VNLWGbvIAvtXeZOZr85RWxgQzvnGas0.dkVaPsp7caTNTC',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:23','2016-04-14 21:25:23',107,NULL),(280,'lhmoya','lhmoya','','0000-00-00 00:00:00','lhmoya@gmail.com','$2a$13$DtiVlvJPmvQ/n10RgEBaKegR5SRgl003judN57mTRCpOSMaZDlMDa',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:23','2016-04-14 21:25:23',107,NULL),(284,'rdfinkel','rdfinkel','','0000-00-00 00:00:00','rdfinkel@yahoo.com','$2a$13$3y/RpNNVHUUAR1lIq9EOoeLtpC9zt.j2wl3KP/ShHvVvy/J7pqpmC',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(285,'jd_smith','jd_smith','','0000-00-00 00:00:00','jd_smith@verizon.net','$2a$13$MhaPk3KC.sAzpqYvryKvVu6Mq6.CX2xXYReip1KJS6C3lMYIfac.u',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(286,'deboraa','deboraa','','0000-00-00 00:00:00','deboraa@verizon.net','$2a$13$mupyO2RfR/54lLMsNMHQaOHbplyEhRJ1m3aZ3berzSAYi0OmZQray',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(287,'jsmith','jsmith','','0000-00-00 00:00:00','jsmith@ithappens.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(288,'jeffreyraich','jeffreyraich','','0000-00-00 00:00:00','jeffreyraich@yahoo.com','$2a$13$XQqzFs4zMwTZuzVSsXJ1qOyBU1Kfij5bIzky8YQEj0HJ.YioJtJN6',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(289,'laurel.edinburgh','laurel.edinburgh','','0000-00-00 00:00:00','laurel.edinburgh@childrensmn.org','$2a$13$s4wP87DvhbBUpNp5saILKu7LXCWblzFC5rFBEO6yvplIelzjMz6oy',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(290,'Laurel28','Laurel28','','0000-00-00 00:00:00','Laurel28@aol.com','$2a$13$EmIckXeN4SkcVQHB7dU0/eFbXktH8GAqbd7CWg97KNkhyoQSGAcaO',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(291,'quandra','quandra','','0000-00-00 00:00:00','quandra@icloud.com','$2a$13$pdEw2FGfiYNr7nk1ZbH3K.O7jXomrwGV6HrrsmUrZ9RFnhdF/u88.',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(292,'collinj8','collinj8','','0000-00-00 00:00:00','collinj8@hotmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(293,'cjohn','cjohn','','0000-00-00 00:00:00','cjohn@hsc.wvu.edu','$2a$13$mL3rzQkBPwsZrQ9ogJnHf.FtLXCZQ1WScUVEn88i8JF9Uu3dY/6vW',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(294,'jacob.pliska','jacob.pliska','','0000-00-00 00:00:00','jacob.pliska@gma.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(295,'dbschickele','dbschickele','','0000-00-00 00:00:00','dbschickele@gmail.com','$2a$13$yNGDwtqUhhnJHB1FhYWmguMu2BlAPaHjWUG.C3tooWu7WUxupbPEe',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(296,'bduma','bduma','','0000-00-00 00:00:00','bduma@comcast.net','$2a$13$91hYIg5SySdn1Q2Yo90zIOO6PZI6O3OJrmJCpczhAe0XC8Qf8.nKG',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(297,'jessemehle','jessemehle','','0000-00-00 00:00:00','jessemehle@yahoo.com','$2a$13$0BRGi02LSBYO/t4V9FTW2OP7jlRRqUQiVoVsiv8qiryOfnxKDEtU6',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(298,'archana.bharadwaj80','archana.bharadwaj80','','0000-00-00 00:00:00','archana.bharadwaj80@gmail.com','$2a$13$sd9g5kT3Lx5TAhCo3ho6JuU5jVk82fPj5BprdTWG/Svi9KePFPMIu',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(299,'pizzazzplayer25','pizzazzplayer25','','0000-00-00 00:00:00','pizzazzplayer25@aim.com','$2a$13$efFxzLSo0vXID1UkFozDmu5z75.w/2aPrClSPvJ5R0Yhn7Ss06Sie',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(300,'ytolugbade','ytolugbade','','0000-00-00 00:00:00','ytolugbade@gmail.com','$2a$13$8Y3y36DWaK7fervnHKTBOecLB0nk4DVkNsMsEWjLXZz2/wLWfbeyi',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(301,'genevievemartine','genevievemartine','','0000-00-00 00:00:00','genevievemartine@gmail.com','$2a$13$HRttfvmq2.pUhYImEXThauW.kxFKG64Vr4yh.xHnrzL4o9Idq7qOe',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(302,'pat.paluzzi','pat.paluzzi','','0000-00-00 00:00:00','pat.paluzzi@comcast.net',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(303,'sara.reeves','sara.reeves','','0000-00-00 00:00:00','sara.reeves@lifeworksaustin.org','$2a$13$OleShxtPz1kWvAW.ROMyX.Jx/96kfVnbWls1CCJuceeMWArsReXqS',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(304,'ksuellentrop','ksuellentrop','','0000-00-00 00:00:00','ksuellentrop@thenc.org','$2a$13$53L6jOzkvellymVGmyKmkOOEUNrIKR6lPRu2OUiNE8zcsKfJJi4/W',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(305,'kimstanek.nmasbhc','kimstanek.nmasbhc','','0000-00-00 00:00:00','kimstanek.nmasbhc@gmail.com','$2a$13$DWKh7DQwVrginBhPs8PU8e0.cT0ft0caCCAV4PKrjTEzMy1y1dQE2',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(306,'kjourdan','kjourdan','','0000-00-00 00:00:00','kjourdan@dhd10.org','$2a$13$ALmkOJKGJEiAwI9IYCPlQu/TP5AEGhvMeHpa5DgOjwsJ624n5nzIq',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(307,'tyang','tyang','','0000-00-00 00:00:00','tyang@neighb.org','$2a$13$VATSICPnwv1cCD9IOSlUB.dq/mZwCOxR5ct.tiYD1Uwrn8EzQ/4rG',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(308,'radicalbluemoon','radicalbluemoon','','0000-00-00 00:00:00','radicalbluemoon@hotmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(309,'michellyis','michellyis','','0000-00-00 00:00:00','michellyis@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(310,'wsu_ns','wsu_ns','','0000-00-00 00:00:00','wsu_ns@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(311,'robyn.lutz','robyn.lutz','','0000-00-00 00:00:00','robyn.lutz@ohiohealth.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(312,'vansciverdiver','vansciverdiver','','0000-00-00 00:00:00','vansciverdiver@msn.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(313,'tvillalobos','tvillalobos','','0000-00-00 00:00:00','tvillalobos@neighb.org','$2a$13$6dmyt07QP9yG7aPt9SEDauq/vCgGUhbjAmMdzZK9AcBQVcV70WBa.',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(314,'emlundberg.ll','emlundberg.ll','','0000-00-00 00:00:00','emlundberg.ll@gmail.com','$2a$13$AnIHQDMDrbof2cI5afhBdOp8lNsjZht5KBCe7wkYxwHsxL0S7t2ta',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(315,'leslie.montgomery','leslie.montgomery','','0000-00-00 00:00:00','leslie.montgomery@ppink.org','$2a$13$oBL7NTQJCJG.QGScA/2jJO9WYJPdAoAPOcXh11pIHeu2nIJiYM3t2',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(316,'lisameares','lisameares','','0000-00-00 00:00:00','lisameares@gmail.com','$2a$13$vfIY7WTy2j033gBTDxGjDu0l5mJ5ZvHjrXNOkUYfuxXm4WKVfMLgS',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(317,'williamsja','williamsja','','0000-00-00 00:00:00','williamsja@co.kern.ca.us','$2a$13$G0pZSxzyyePHIeX2CLNgmOijoj2nEYeGBhZq./RSBwvGUbtPaivNK',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(318,'janie.leary','janie.leary','','0000-00-00 00:00:00','janie.leary@fairmontstate.edu','$2a$13$osl2UHOBYHBMQS1ng9mFgOnCvPwESjm22PGeO6pQxZLpvlyu6FFxm',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(319,'katie.adamek','katie.adamek','','0000-00-00 00:00:00','katie.adamek@gmail.com','$2a$13$YEIbkuBrB5WD.hWYF6IoTO7oJsguM6ez0XVE.8zufW7JGG8ZQK0Wu',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(320,'ggaudet','ggaudet','','0000-00-00 00:00:00','ggaudet@ingham.org','$2a$13$niczQUDZ7S0l/EdyL0DZ2.UsLgO1tV723oz6ahbaY9USyQSMiW74e',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(321,'lfendorak','lfendorak','','0000-00-00 00:00:00','lfendorak@hhhn.org','$2a$13$bpLUSz4XLAcu86wlxGh8Wef2vz3Ub5djFuzn6sfcpdmBydsaOjsGS',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(322,'abelanne65','abelanne65','','0000-00-00 00:00:00','abelanne65@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(323,'koramarie.anderson','koramarie.anderson','','0000-00-00 00:00:00','koramarie.anderson@gmail.com','$2a$13$IV1Ila9OpGkhqSq8eBjVbuI4n6LtH1Ub0hE4/Nqct9DUkyiigBkWm',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(324,'aleon','aleon','','0000-00-00 00:00:00','aleon@tulsa-health.org','$2a$13$E4S8074jiMSjsaQH28tgsOADordkrErByMhwTzisvtfBCAG7DMp5i',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(325,'shelly.tjapkes','shelly.tjapkes','','0000-00-00 00:00:00','shelly.tjapkes@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(326,'allyson.nelson','allyson.nelson','','0000-00-00 00:00:00','allyson.nelson@bayer.com','$2a$13$J0NvdzcOLzuMeNuiqoPeHu71f2pJAlaKDI0Tl/GL3SgGGhz7rHxdS',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(327,'hopy301','hopy301','','0000-00-00 00:00:00','hopy301@gmail.com','$2a$13$d1tQGBNM/gk6iUNyGZYxU.NvmlS2Z0tj9gp8KEdOD4KN7gcj1gBwK',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(328,'ruthhumphrey_1','ruthhumphrey_1','','0000-00-00 00:00:00','ruthhumphrey_1@msn.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(329,'ragemaster89','ragemaster89','','0000-00-00 00:00:00','ragemaster89@gamil.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(330,'carolhaddix','Carol','Haddix','0000-00-00 00:00:00','carolhaddix@ymail.com','$2a$13$fkVuwz72sS1CNopzksIcJePcTL0NgEtjTWqkAPTuhiJSXUM4rx5fa',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(331,'lori.nieboer','lori.nieboer','','0000-00-00 00:00:00','lori.nieboer@gmail.com','$2a$13$iEX3DEm/e.NKrQmVO2WWoOpZpwbaQnWEkXb1WQrp6xa0zO59.a01i',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(332,'tashana.jaudon','tashana.jaudon','','0000-00-00 00:00:00','tashana.jaudon@vdh.virginia.gov','$2a$13$J8mvJNLyAs6WzSO4OGvl7.eCcOabHNSYUWXsWrSGnmMZUiNSCz.d.',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(333,'sonyachan','sonyachan','','0000-00-00 00:00:00','sonyachan@msn.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(334,'coastalcarolinaobgyn','coastalcarolinaobgyn','','0000-00-00 00:00:00','coastalcarolinaobgyn@icloud.com','$2a$13$SZH.nfzfEloEN/Z8gQy2V.F1.thfEdJwWQNH5ztYniB6ZjbMYWHP.',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(335,'rdixon','rdixon','','0000-00-00 00:00:00','rdixon@teenpregnancysc.org','$2a$13$nyssG7baGRm4IB.ofXSgLuutuxEehTe/KXogWfNVyajb/qzXBtq/y',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(336,'mserowo1','mserowo1','','0000-00-00 00:00:00','mserowo1@hfhs.org',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(337,'savannarfox','savannarfox','','0000-00-00 00:00:00','savannarfox@gmail.com','$2a$13$D5B5jUv49GhRl9OBhom3bOwEaPQVMX/p320899BRZUgqiJNnebTe6',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(338,'juku11','juku11','','0000-00-00 00:00:00','juku11@outlook.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:24','2016-04-14 21:25:24',107,NULL),(339,'kendra.webb115','kendra.webb115','','0000-00-00 00:00:00','kendra.webb115@yahoo.com','$2a$13$1KwNzJxZ6aImxNs8eAjAtukiG1IDMFusXJpfcAp8TJwx58KgQg/2S',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(340,'jenna.shea.thomas','jenna.shea.thomas','','0000-00-00 00:00:00','jenna.shea.thomas@gmail.com','$2a$13$v0S6DqVnktf7uOSPQ6SKuOGqUcwpXj2GoZOIgOfMtuo3carErrwxq',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(341,'butgaard','butgaard','','0000-00-00 00:00:00','butgaard@mt.gov','$2a$13$hjbNd59hFHHxRXEHRwBHcewZ4CqZfvKy9WoBQqbk/gT/xnk9idYPm',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(342,'taylud2008','taylud2008','','0000-00-00 00:00:00','taylud2008@gmail.com','$2a$13$BHvZlw/r5i4zAOruSmEDw.KcoRkDzmLeZ7cEFeV0ztG49xSp3sYSi',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(343,'chepanoske2000','chepanoske2000','','0000-00-00 00:00:00','chepanoske2000@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(344,'cchepano','cchepano','','2016-06-20 16:58:15','cchepano@andrew.cmu.edu','$2a$13$p/TSFbGkupDYPXgnFjoXJO3YvygO3icDAW0koZCGQUIyrOJoUAeHy',0,1,'','',NULL,'y',0,NULL,'','','2016-04-14 21:25:25','2016-06-20 20:58:15',107,NULL),(345,'duhmichelly','duhmichelly','','0000-00-00 00:00:00','duhmichelly@hotmail.com','$2a$13$77P7lKAX5GVnDWochGOvfOPHK/OP5qPR2.i5lYrYS0/8DV.RuHzK.',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(346,'lposs','lposs','','0000-00-00 00:00:00','lposs@andrew.cmu.edu','$2a$13$QSoU.kRASOT.LbEkIN7tiuKXMJKijiik/mqHe/nMyWj1KwFup8/z2',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(347,'gsyska','gsyska','','0000-00-00 00:00:00','gsyska@gmail.com','$2a$13$Z/eyxYd84WQm5A0HA16Mo.RAHyPvEJk01WHiRV9O82gWeLi9roHO2',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(348,'elisedevore','elisedevore','','0000-00-00 00:00:00','elisedevore@hotmail.com','$2a$13$fTO.vwQreAhC1qLhZjA0dO7AgnCdJvOabCp2p2.Be0sjBJwic0tsi',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(349,'lmposs24','lmposs24','','0000-00-00 00:00:00','lmposs24@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(350,'carolyn.cox','carolyn.cox','','0000-00-00 00:00:00','carolyn.cox@spart5.net','$2a$13$qkPM33CcBTdHhWsaInP4qOH2k7wr9VZV6CGbCnwkZhBO/rqk0MyC.',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(351,'baronbinns','baronbinns','','0000-00-00 00:00:00','baronbinns@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(352,'abbibinns','abbibinns','','0000-00-00 00:00:00','abbibinns@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(353,'binnsabbi','binnsabbi','','0000-00-00 00:00:00','binnsabbi@yahho.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(354,'987dabestmun','987dabestmun','','0000-00-00 00:00:00','987dabestmun@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(355,'987dabest','987dabest','','0000-00-00 00:00:00','987dabest@gmail.com','$2a$13$wkHIjhi7w/VLFzXbINJIJe420mTVkVqDiR2FRxj1TbW5TKECUFdzi',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(356,'erinnnnire','erinnnnire','','0000-00-00 00:00:00','erinnnnire@yahoo.com','$2a$13$gBocEdg/cx82.gIBoRpnR.iuGadvouwcX20zE6y17zUo1.Suc9jAe',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(357,'teipe001','teipe001','','0000-00-00 00:00:00','teipe001@umn.edu','$2a$13$7Oe2a3sCaVd4U4QGv1pTRuWWfbVLZUpRjEDxd9KLvMrkVe/fQ7FCm',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(358,'Carrie.E.Linn','Carrie.E.Linn','','0000-00-00 00:00:00','Carrie.E.Linn@wv.gov','$2a$13$hkxdhikm/hXiW/vPyMH5k.A8yFkEh5KFV71UqTNYINf9g0di2Whuu',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(359,'diva1030','diva1030','','0000-00-00 00:00:00','diva1030@optimum.net',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(360,'ahannah','ahannah','','0000-00-00 00:00:00','ahannah@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(361,'jlrb90','jlrb90','','0000-00-00 00:00:00','jlrb90@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(362,'scastellanos','scastellanos','','0000-00-00 00:00:00','scastellanos@zepfcenter.org',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(363,'huberts','huberts','','0000-00-00 00:00:00','huberts@aol.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(364,'Drew Pearce','Drew','Pearce','0000-00-00 00:00:00','drew@dreamfactory.com','$2a$13$8DAEGpTrS/9CBBEcI7e0GeBAz2eELRDwa8AltDWegvwiXTjnGF/RO',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(365,'aliciamillerlight','aliciamillerlight','','0000-00-00 00:00:00','aliciamillerlight@hotmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(366,'johnenef','johnenef','','0000-00-00 00:00:00','johnenef@yahoo.com','$2a$13$GKF.El3TPD6y5zT/B7rJOuxFg0bDm4F5M5G3l7l05A38x3Y4ACSFC',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(367,'mollywolfe667','mollywolfe667','','0000-00-00 00:00:00','mollywolfe667@gmail.com','$2a$13$EdP474NsnaLZqvwxddF9Me3169sd8eE554Rnun3mCo4AVns/yCkKO',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(368,'kayreed','kayreed','','0000-00-00 00:00:00','kayreed@dibbleinstitute.org','$2a$13$uz3O3g/SPrzciFuwN7VCKukKECYtLbwzGGysvQjjvemaHISHLyaGu',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(369,'light.ofdarkness','light.ofdarkness','','0000-00-00 00:00:00','light.ofdarkness@live.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(370,'pamelacan22','pamelacan22','','0000-00-00 00:00:00','pamelacan22@comcast.net',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(371,'summa92','summa92','','0000-00-00 00:00:00','summa92@hotmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(372,'brendaleecassidy','brendaleecassidy','','0000-00-00 00:00:00','brendaleecassidy@gmail.com','$2a$13$e1aFmhzOOQYIKDq1I2s//uiuS/X4hm1cMJezVIvFZdY68s5XM3dRG',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(373,'r.monique98','r.monique98','','0000-00-00 00:00:00','r.monique98@gmail.com','$2a$13$30J58JSZ5tI5tZQh9GPImOdT4xivHFMF7jK4FD/uFXecvMHb7zHDy',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(374,'corgilover26','corgilover26','','0000-00-00 00:00:00','corgilover26@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(375,'hress03','hress03','','0000-00-00 00:00:00','hress03@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(376,'ruthiemjohnson','ruthiemjohnson','','0000-00-00 00:00:00','ruthiemjohnson@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(377,'rmjpeace1','rmjpeace1','','0000-00-00 00:00:00','rmjpeace1@hotmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(378,'mallory.rowell','mallory.rowell','','0000-00-00 00:00:00','mallory.rowell@nationwidechildrens.org','$2a$13$BbxRZT5QCNXc85FBFFMIVebcwjzhW.ZP1Pv8UMdKf32tXV8E6j3GS',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(379,'jilani_1987','jilani_1987','','0000-00-00 00:00:00','jilani_1987@yahoo.com','$2a$13$3I/sdrRCndbKxUWXjL5jUOhZ4BUSW5r7vcVTK38H6yS40XGbV2y5y',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(380,'josirogers5','josirogers5','','0000-00-00 00:00:00','josirogers5@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(381,'ravenmarieluckett','ravenmarieluckett','','0000-00-00 00:00:00','ravenmarieluckett@outlook.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(382,'medinaverenice23','medinaverenice23','','0000-00-00 00:00:00','medinaverenice23@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(383,'ontiverorshector21','ontiverorshector21','','0000-00-00 00:00:00','ontiverorshector21@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(384,'jbell','jbell','','0000-00-00 00:00:00','jbell@beyondabuse.info','$2a$13$HRwa9z/b0/pSeHzhTkyDEuyyN5uzk5EXum/hgbosJo1BxCbDSxB2e',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(385,'klaudiat10','klaudiat10','','0000-00-00 00:00:00','klaudiat10@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(386,'sophianassar2','sophianassar2','','0000-00-00 00:00:00','sophianassar2@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(387,'sophia56','sophia56','','0000-00-00 00:00:00','sophia56@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(388,'princess8987.kc2','princess8987.kc2','','0000-00-00 00:00:00','princess8987.kc2@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(389,'princess8987.kc1','princess8987.kc1','','0000-00-00 00:00:00','princess8987.kc1@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(390,'princess8987.kc3','princess8987.kc3','','0000-00-00 00:00:00','princess8987.kc3@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(391,'krazyscorpion','krazyscorpion','','0000-00-00 00:00:00','krazyscorpion@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(392,'danaasay1224','danaasay1224','','0000-00-00 00:00:00','danaasay1224@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(393,'smiller1','smiller1','','0000-00-00 00:00:00','smiller1@fbhwa.org','$2a$13$U1NRTeCRuPnszv5901qIxeBqpOHkiF9wGDjAWTMgHP.bCdRKCOTb2',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(394,'stmillersh','stmillersh','','0000-00-00 00:00:00','stmillersh@msn.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(395,'kaylamartinez805','kaylamartinez805','','0000-00-00 00:00:00','kaylamartinez805@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:25','2016-04-14 21:25:25',107,NULL),(396,'chapman.alicemarie','chapman.alicemarie','','0000-00-00 00:00:00','chapman.alicemarie@gmail.com','$2a$13$F8maO23kHBIO0CIsSKMxhuuGF6RiL9kxGVk7pfYuy4erasfQHUw6q',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(397,'achapman','achapman','','0000-00-00 00:00:00','achapman@tulane.edu',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(398,'sierrahenderson95','sierrahenderson95','','0000-00-00 00:00:00','sierrahenderson95@yahoo.com','$2a$13$RNwKe2bxh.esE0wYDOmbB.Z2AmpU10RFHLFV2G5D3MAoIz1fuhKlq',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(399,'sierrahendy19','sierrahendy19','','0000-00-00 00:00:00','sierrahendy19@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(400,'meghan.sullivan5301','meghan.sullivan5301','','0000-00-00 00:00:00','meghan.sullivan5301@gmail.com','$2a$13$Mk3BuCNnTTJR9YfVyN6ef.MYE4F4OXBT3WyBoOjJAITfapsaiHxTC',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(401,'ethanscholtens','ethanscholtens','','0000-00-00 00:00:00','ethanscholtens@yahoo.com','$2a$13$.P3Pmb4fbHFfX.3edh/9COX4ypiCDy1L0uo5KTK9VVRgv7HdX4uoi',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(402,'paytonlila','paytonlila','','0000-00-00 00:00:00','paytonlila@gmail.com','$2a$13$O5kQdDPWFGyChaTKi9WSSei1ggdSpMa8NG5W8.IbwnMDaLItMxPoC',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(403,'meredithcerise','meredithcerise','','0000-00-00 00:00:00','meredithcerise@gmail.com','$2a$13$HZcjW2X1njcf0YbJQWT.VenFbZvaont9KXgbRKOLglHRkZQdbwRkm',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(404,'meredith_cerise','meredith_cerise','','0000-00-00 00:00:00','meredith_cerise@yahoo.com','$2a$13$oZCvPICbFtwEfD1a2RMWwejv7/NY8HOrZyakjxiw2rSaTXeqxAiHC',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(405,'eieio','eieio','','0000-00-00 00:00:00','eieio@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(406,'pretty','pretty','','0000-00-00 00:00:00','pretty@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(407,'jango','jango','','0000-00-00 00:00:00','jango@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(408,'hello','hello','','0000-00-00 00:00:00','hello@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(409,'boupadoo','boupadoo','','0000-00-00 00:00:00','boupadoo@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(410,'ashleytaylor1470','ashleytaylor1470','','0000-00-00 00:00:00','ashleytaylor1470@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(411,'maddierox','maddierox','','0000-00-00 00:00:00','maddierox@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(412,'maddiememe','maddiememe','','0000-00-00 00:00:00','maddiememe@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(413,'maggieismaggie','maggieismaggie','','0000-00-00 00:00:00','maggieismaggie@maggie.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(414,'mariahsimon','mariahsimon','','0000-00-00 00:00:00','mariahsimon@icloud.com','$2a$13$JBldnu1FQvFC72.LZZs5q.4QV7Qv2NcnVrf1JmXtN8/Lz2ba7Yfg2',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(415,'sammiclayton123','sammiclayton123','','0000-00-00 00:00:00','sammiclayton123@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(416,'oliviadeweese111','oliviadeweese111','','0000-00-00 00:00:00','oliviadeweese111@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(417,'feigeg','feigeg','','0000-00-00 00:00:00','feigeg@gmail.com','$2a$13$1Wlkbi1sNPrsFJ1zNbUgY.F2hqelAVO9tTw9OmdrwAV9y1axzFrnK',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(418,'edmondscl','edmondscl','','0000-00-00 00:00:00','edmondscl@googlemail.com','$2a$13$LJllVypFahILQwm5YvU6V.zVMayeMYNkCASbGjzQe/E32S0MIe.Si',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(419,'aaronplant','aaronplant','','0000-00-00 00:00:00','aaronplant@earthlink.net','$2a$13$SmpK4J5Yt1ZC3wfqKdDrXuoPiLjRwZKcMYMWnmyKGoWP85o0Z3fL6',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(420,'gabriella.galdamez','gabriella.galdamez','','0000-00-00 00:00:00','gabriella.galdamez@gmail.com','$2a$13$wr1G/JpgYkWDQ3JFkRR2TOX6F2b7rqDj.L3xBXCUeodfdpgHePfs2',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(421,'jessamyn.howe','jessamyn.howe','','0000-00-00 00:00:00','jessamyn.howe@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(422,'chloe_boog','chloe_boog','','0000-00-00 00:00:00','chloe_boog@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(423,'labrisaw','labrisaw','','0000-00-00 00:00:00','labrisaw@gmail.com','$2a$13$BJYC6SDTrE3aWM77NRYS7O34UT8/kHmWAa4gVOBjraXGxftN8xH1G',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(424,'chloehopemeredith','chloehopemeredith','','0000-00-00 00:00:00','chloehopemeredith@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(425,'cathie9','cathie9','','0000-00-00 00:00:00','cathie9@hotmail.com','$2a$13$EdIMCn2EjnUAp/E04JeHwOQIZiCkbHLqzZuPzpXoqH9OcsjVFbnTi',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(426,'lwilliams','LaBrisa','Williams','0000-00-00 00:00:00','lwilliams@tulsacampaign.org','$2a$13$I2JK1oheUJEpYNHKzpzXOO/p/Gv0PUF3DUlC.laCigghVt0VSQHmG',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(427,'rgold','rgold','','0000-00-00 00:00:00','rgold@tulsacampaign.org','$2a$13$ispE6lPE0SwryZIBEyKIb.z95BER7DOADUqNnw5c8YZxJHyFR1Nbq',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(428,'annacialone','annacialone','','0000-00-00 00:00:00','annacialone@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(429,'0004406stu','0004406stu','','0000-00-00 00:00:00','0004406stu@sdst.org',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(430,'lmoriarty','lmoriarty','','0000-00-00 00:00:00','lmoriarty@nvpca.org','$2a$13$eu7zG8HdjmjJJu.f3/MhVOP7EAK1.DWdbGPZxsPStGJhSYEiOgyaS',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(431,'bite.moon','bite.moon','','0000-00-00 00:00:00','bite.moon@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(432,'bitemoon98012','bitemoon98012','','0000-00-00 00:00:00','bitemoon98012@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(433,'seventeendays.app.testing','seventeendays.app.testing','','0000-00-00 00:00:00','seventeendays.app.testing@gmail.com','$2a$13$yydsAeXQu5wx8IhwFSAWiev5pEXB0XU02KVtjC3VA69J6H6KTD1Y6',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(434,'ccdaukas','ccdaukas','','0000-00-00 00:00:00','ccdaukas@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(435,'markaduffy+test111','markaduffy+test111','','0000-00-00 00:00:00','markaduffy+test111@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(436,'jackie204673','jackie204673','','0000-00-00 00:00:00','jackie204673@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(437,'ana93','ana93','','0000-00-00 00:00:00','ana93@sbcglobal.net',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(438,'hhboes','hhboes','','0000-00-00 00:00:00','hhboes@gmail.com','$2a$13$00/tmbpuRc5jcrsL/m997uXsOqqRaL7ypBT1l7XyB.HFdPwIRE7cK',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(439,'bill.taverner','bill.taverner','','0000-00-00 00:00:00','bill.taverner@ppgnnj.org','$2a$13$jXsi1kGMlU5D.EVsZQcdjusfqNXTWDLbSwgzPALnvV2ftDsNBCT4S',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(440,'cphalunas','cphalunas','','0000-00-00 00:00:00','cphalunas@comcast.net','$2a$13$3U76m8FKLAZpLPASWeKXOuyNkDm.1LtD.S0FYzgs2gk449ESoHKVK',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(441,'lucianbabe2000','lucianbabe2000','','0000-00-00 00:00:00','lucianbabe2000@hotmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(442,'nickyclermont','nickyclermont','','0000-00-00 00:00:00','nickyclermont@textnow.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(443,'love','love','','0000-00-00 00:00:00','love@ihd.org',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(444,'yoost','yoost','','0000-00-00 00:00:00','yoost@marshall.edu',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(445,'hope.pierstorff','hope.pierstorff','','0000-00-00 00:00:00','hope.pierstorff@gmail.com','$2a$13$ES4aZ.HMYpORo96KDkSjw.0nEDXYjefjopM.Q.3nqLj8d7wp.gbWa',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(446,'sentientap','sentientap','','0000-00-00 00:00:00','sentientap@yahoo.com','$2a$13$rwAuKSZuMsFI4sy/nxsea.7nSMjNacvinCQ1dudetCbvoIlzwEfHy',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(447,'megan.almeda','megan.almeda','','0000-00-00 00:00:00','megan.almeda@gmail.com','$2a$13$vy.d0twAB4BMFRrGVJOn9OkiNyvtEIkoFVC6EsxzdpHdSFAgITug2',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(448,'nunudoong','nunudoong','','0000-00-00 00:00:00','nunudoong@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(449,'prask','prask','','0000-00-00 00:00:00','prask@cox.net',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(450,'jmitchell','jmitchell','','0000-00-00 00:00:00','jmitchell@cox.net',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(451,'karen.maziarz','karen.maziarz','','0000-00-00 00:00:00','karen.maziarz@nextgenfl.org',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:26','2016-04-14 21:25:26',107,NULL),(452,'magdalen.korder','magdalen.korder','','0000-00-00 00:00:00','magdalen.korder@hennepin.us','$2a$13$69nvOTKHgJWJ0eyGKnpZGuI4SzT8OTqZDtdmfW.F7QLdDrjjXQ1iy',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(453,'steinwayc','steinwayc','','0000-00-00 00:00:00','steinwayc@email.chop.edu','$2a$13$b2FJGn3NDFAmQfh0YtckVOa1OL8gO9/bUDtj.HqLKXiO94Bw7nV9O',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(454,'dianalynn.ramos','dianalynn.ramos','','0000-00-00 00:00:00','dianalynn.ramos@hsd.cccounty.us','$2a$13$vZLcRJzNgtlNd3GBZkXf0uU4wo.ykMcM9.CyUqsIMSaE030/aK.uG',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(455,'atolliver','atolliver','','0000-00-00 00:00:00','atolliver@wvperinatal.org',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(456,'lovenikki510','lovenikki510','','0000-00-00 00:00:00','lovenikki510@gmail.com','$2a$13$K4lLXRL3DETxsysIzBoxIOqCSZFqx9YnojI1KFQS8IXuHrZxwy3K2',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(457,'brennarosemn','brennarosemn','','0000-00-00 00:00:00','brennarosemn@aol.com','$2a$13$kmxf2wFe2Y/Yr/BPPqzmu.5ap1pNlBmhwb3FUsWpPMwN1/GbxXk7u',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(458,'brennarosemm','brennarosemm','','0000-00-00 00:00:00','brennarosemm@aol.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(459,'rm4044','rm4044','','0000-00-00 00:00:00','rm4044@myslive.net',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(460,'lorenaolvera','lorenaolvera','','0000-00-00 00:00:00','lorenaolvera@gmail.com','$2a$13$VU2E6ioySHQqFBO6rhniVekpAiecWapc/xgiWB3JUMiRViWrJ9dtK',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(461,'amyntolliver','amyntolliver','','0000-00-00 00:00:00','amyntolliver@gmail.com','$2a$13$vqSORp8B6UzE7Dh/uk9pqO7niToqzyg92RDkrUxllWhQ53XeF2y0K',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(462,'hdashefsky','hdashefsky','','0000-00-00 00:00:00','hdashefsky@gmail.com','$2a$13$hv8VtuESWglwbIDBZOfkFe8vMwISo1NuFSELpDzeZOb4Xid3yBdrm',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(463,'thomasgill341','thomasgill341','','0000-00-00 00:00:00','thomasgill341@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(464,'thomasgillespi219','thomasgillespi219','','0000-00-00 00:00:00','thomasgillespi219@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(465,'forget_labels','forget_labels','','0000-00-00 00:00:00','forget_labels@yahoo.com','$2a$13$lZiTWEHJt3ieWY7aJxlqZuhGSaloBH/.zXv1hkMl2v6ySIlrHEcVy',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(466,'vira.david','vira.david','','0000-00-00 00:00:00','vira.david@baltimorecity.gov','$2a$13$3L.PmD2HLHENFTAYdWkaIOuI7lhxv74OESI0LWAHLOufmrrjgDmvW',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(467,'hellahope2257','hellahope2257','','0000-00-00 00:00:00','hellahope2257@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(468,'ramchandak','ramchandak','','0000-00-00 00:00:00','ramchandak@email.chop.edu','$2a$13$CumDRhXn3lR5LAPqf3NHb.wBN2UpElnP/rPBfUq10T/6nVfbE1QQS',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(469,'poojapat','poojapat','','0000-00-00 00:00:00','poojapat@nursing.upenn.edu','$2a$13$Zwispb1UGrkXbiTx3HtaDel5eaHlTe4su0C8JpGhJtvNQsM1yNPhW',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(470,'alexisdaniels13','alexisdaniels13','','0000-00-00 00:00:00','alexisdaniels13@gmail.com','$2a$13$8WcPVtw5TC0y4BjvN34bVeU64Ftci/QzT3V25ZeXbeuSQsWLBUADu',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(471,'lmack5','lmack5','','0000-00-00 00:00:00','lmack5@schools.nyc.gov','$2a$13$cRUuc.Lvf4MMvNwJo5J4z.CMmRuiDQmn6CxSqT5QGqt6zUj7NlrIO',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(472,'pritchaj','pritchaj','','0000-00-00 00:00:00','pritchaj@nths.net','$2a$13$gQh2AD00Sfu8NWxr.VY3Ge.E8MXR.IiqBpqhy9elcR2Bz66sreoFS',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(473,'donaldsonls','donaldsonls','','0000-00-00 00:00:00','donaldsonls@mac.com','$2a$13$JK6yAulbTrB7I1Mz1yeXF.SVH48J120.23rWvDzfTx/VyYQTVWNvO',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(474,'kcagle','kcagle','','0000-00-00 00:00:00','kcagle@lrmcenter.com','$2a$13$PCEpeQF57pazRyGUI.zouek1FwTe2T92k5kkrlguBfAlU0cS47GR6',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(475,'lizcchen','lizcchen','','0000-00-00 00:00:00','lizcchen@live.unc.edu','$2a$13$4wAQDUfb5asVkB5K0s4fx.vzVi0MSG9K2Lrsvi9UhJe1e4o5q93Zq',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(476,'rmayanja','rmayanja','','0000-00-00 00:00:00','rmayanja@health.nyc.gov','$2a$13$imWjccbKHbgNKK9CLn6SYe/hz6ZQD.4tPsthljKI15bVMaTdQnn5.',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(477,'mattc','mattc','','0000-00-00 00:00:00','mattc@etr.org','$2a$13$tR8UgK3j1WLFUj8ZXQ8va.asR/XTug5UIpBIUEGGgz3hypnfxjDza',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(478,'karen.rayne','karen.rayne','','0000-00-00 00:00:00','karen.rayne@gmail.com','$2a$13$hrsbTtCCOCuiPil9eqGqNuACbj1LPNn9PYpwmMDewh6imtCl.czjK',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(479,'ginaginamarie','ginaginamarie','','0000-00-00 00:00:00','ginaginamarie@sbcglobal.net','$2a$13$0iLrCtnClBjvKQxt64a2DelC3KQYUGqPWDUgbqFVPpJZzCAH2uUz6',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(480,'tamararain','tamararain','','0000-00-00 00:00:00','tamararain@att.net','$2a$13$0lHst6UBJEUn5Sipc6E.OemoZVHKpv6uatwqCW8NqR6/y8PHKyF0m',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(481,'anuka.nizharadze','anuka.nizharadze','','0000-00-00 00:00:00','anuka.nizharadze@rambler.ru',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(482,'jgodwin','jgodwin','','0000-00-00 00:00:00','jgodwin@nvpca.org','$2a$13$OCoSm5IFh8dcpu50EEnDUuZoGVeOzeRZG/W0G2lXOUBjCsbheiUkW',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(483,'bakingmemoriesforever','bakingmemoriesforever','','0000-00-00 00:00:00','bakingmemoriesforever@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(484,'csloveskittens','csloveskittens','','0000-00-00 00:00:00','csloveskittens@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(485,'josilyn4665','josilyn4665','','0000-00-00 00:00:00','josilyn4665@mcsgmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(486,'janice.williams','janice.williams','','0000-00-00 00:00:00','janice.williams@baltimorecity.gov','$2a$13$aDJfXDDqeC478/ORUuDVne2dx7S2u9hx1ClxTzb6Erj/b0ZVv6R52',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(487,'mchevalier','mchevalier','','0000-00-00 00:00:00','mchevalier@carrington.edu','$2a$13$BpdcQURXaDFWENTDAG8.NuEOtJZIDpYbUAcEgB3RRSJxVb94mblKO',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(488,'mlangley','mlangley','','0000-00-00 00:00:00','mlangley@comporium.net',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(489,'kelsowin','kelsowin','','0000-00-00 00:00:00','kelsowin@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(490,'info.seventeen.days','info.seventeen.days','','0000-00-00 00:00:00','info.seventeen.days@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(491,'seveteendays.anna','seveteendays.anna','','0000-00-00 00:00:00','seveteendays.anna@gmail.com','$2a$13$rRzkhXzdsVlMKxe9Oz1TkOjs6DPQ5SZ4ErqNcr9Z5aXs8MyQmWLzy',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(492,'seventeendays.anna','seventeendays.anna','','0000-00-00 00:00:00','seventeendays.anna@gmail.com','$2a$13$SXHE8PfTPxiXrEa19rqjXuEbXqxQt1kvQuRYt8qeV3GuOmdJ8hmsy',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(493,'jill.dishion','jill.dishion','','0000-00-00 00:00:00','jill.dishion@gmail.com','$2a$13$Fjdm0kX6SEnefBlF6oQeee.kU6v3kYIMKIV.U2p0pAG8oOnI5Qg6i',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(494,'kr1613','kr1613','','0000-00-00 00:00:00','kr1613@bcfs.net','$2a$13$QwIfdWJ1MWq8pe63c3EziOQkf1nArGqskL5S3ibyI9yK1NvopAXWy',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(495,'katrice','katrice','','0000-00-00 00:00:00','katrice@ktc.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(496,'ahfd926','ahfd926','','0000-00-00 00:00:00','ahfd926@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(497,'kimia_abidi11','kimia_abidi11','','0000-00-00 00:00:00','kimia_abidi11@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(498,'bmarsha2','bmarsha2','','0000-00-00 00:00:00','bmarsha2@jhu.edu','$2a$13$Om2IdLa/bZ43KAxDyTL9jeVRYccD7jCMaNtqb2HREsX789KUdRNRu',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(499,'girlsinc','girlsinc','','0000-00-00 00:00:00','girlsinc@ywcadayton.org','$2a$13$dUQsusXwZHt.2mxI6Eyu8ujAezwiu7SVG.CmD/gekUO06xD/JBHQq',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(500,'garciaa9','garciaa9','','0000-00-00 00:00:00','garciaa9@hotmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(501,'angela_garcia21','angela_garcia21','','0000-00-00 00:00:00','angela_garcia21@hotmail.com','$2a$13$esJIfkhJG/vUNQ.RNkuAIutkhloff0.T8ylKf6O.2rCbbKLWW.htS',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(502,'lowed','lowed','','0000-00-00 00:00:00','lowed@uthscsa.edu','$2a$13$8qqvfo7Off2k2G9aVViJge3vfNGrtVpHQWPagHjIHgWJm9OuyGkZi',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(503,'sandburg','William','Sandburg','0000-00-00 00:00:00','sandburg@uthscsa.edu','$2a$13$97o.SsodLdI41icu9XH1NeE77Ph9LckSQ0oSicIsiQqtKfS1mnMr6',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(504,'lmlbpereira','lmlbpereira','','0000-00-00 00:00:00','lmlbpereira@gmail.com','$2a$13$b5NJ.6J0kNsmAsmfKjw/0.cIh1JoJ4uE6SVGPGQOOWaaSKzZTDP/W',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(505,'Cooleostemost','Cooleostemost','','0000-00-00 00:00:00','Cooleostemost@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(506,'riosambar1981','riosambar1981','','0000-00-00 00:00:00','riosambar1981@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:27',107,NULL),(507,'sarah.harmon','sarah.harmon','','0000-00-00 00:00:00','sarah.harmon@santacruzcounty.us','$2a$13$nYdeeOjDeNhjEmAlv.v8muyov7b0fQ/2642yHTSVmR4HZMnIjkNbG',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:27','2016-04-14 21:25:28',107,NULL),(508,'ehuman14','ehuman14','','0000-00-00 00:00:00','ehuman14@adams14.org','$2a$13$oogEoSclZ1B8XzvlJcgxQ.XvJdkqwF33QaI1NxIne/Jx1yup2X4LW',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(509,'ehuman','ehuman','','0000-00-00 00:00:00','ehuman@adams14.org',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(510,'delallog','delallog','','0000-00-00 00:00:00','delallog@uthscsa.edu','$2a$13$u4rs.CalV3b2cgmD.WKUMuqCrKsmwZiMai23qopSFva09b4P0qCOi',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(511,'slosoya','slosoya','','0000-00-00 00:00:00','slosoya@yahoo.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(512,'nunofamily2010','nunofamily2010','','0000-00-00 00:00:00','nunofamily2010@gmail.com','$2a$13$Hs0M4jkln5bAe0A7jo4T/u/YChY4yiRfavEjW6dglAyWG3F1qCeVu',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(513,'melissarodriguez11.mr','melissarodriguez11.mr','','0000-00-00 00:00:00','melissarodriguez11.mr@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(514,'melissa_rodriguez82','melissa_rodriguez82','','0000-00-00 00:00:00','melissa_rodriguez82@yahoo.com','$2a$13$UMR6yub2q/ZWOis4NYeCDuLv6nk.GyfI4RYbc0k1imZEgusWN/aNO',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(515,'tcoombs1206','tcoombs1206','','0000-00-00 00:00:00','tcoombs1206@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(516,'jodigoodman','jodigoodman','','0000-00-00 00:00:00','jodigoodman@att.net','$2a$13$arK4sLTOY4Y/VoYMG6LkSeTRH8yy9bO0LyZKjt.YFe7oBN/02CfaG',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(517,'phyllis.priess','phyllis.priess','','0000-00-00 00:00:00','phyllis.priess@uhs-sa.com','$2a$13$W/vMEBlL7wgRVJWW8ZM4Ru2Zl3BoILQZOE7NQ4GKZ6OOpck5mjVde',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(518,'raefinch68','raefinch68','','0000-00-00 00:00:00','raefinch68@gmail.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(519,'adbarfield','adbarfield','','0000-00-00 00:00:00','adbarfield@hotmail.com','$2a$13$x5SrAuPJ02kQcJeSDQB5nundASqSEumDLCL7k4VMEX4K96jfDXbSK',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(520,'recatra','recatra','','0000-00-00 00:00:00','recatra@gmail.com','$2a$13$.UJ24OXmQctreQXwlo7ycemhy41vmhPWv2MmWJa9bx6nNdLl6Jx7m',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(521,'rsmith','rsmith','','0000-00-00 00:00:00','rsmith@lrmcenter.com',NULL,0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(526,'hh','hh','','0000-00-00 00:00:00','hh@hh.com','$2a$13$DSlFK63djwqy4drcxPOLq.gQj1ug4Tzdc0DexhzX5svovWYDxvXAu',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(529,'madeline.g.travers','madeline.g.travers','','0000-00-00 00:00:00','madeline.g.travers@gmail.com','$2a$13$VQF16hdIDylcdoCiTar3ZOowASrFabaRuNcJSck/FC9DUyWxeJu36',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(530,'nsecworkshop7','nsecworkshop7','','0000-00-00 00:00:00','nsecworkshop7@gmail.com','$2a$13$k4GMBjQDCY2IxDVakVmxNuXb8guBcrQ/IM/ZoMuXtCELgBxMHG032',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(531,'nsecworkshop2','nsecworkshop2','','0000-00-00 00:00:00','nsecworkshop2@gmail.com','$2a$13$6knTeSO1z8AzZ0Q90SiiD.VAnbBOYdj4RTGtTb/iEP24tTkCDjIVm',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(532,'phyllismorreale','phyllismorreale','','0000-00-00 00:00:00','phyllismorreale@hhhn.org','$2a$13$zXQ7aKwD..rAss0hWTDQkevLBk7Gu8XrAhzfRP6yexYPIsxpUz2Mu',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(533,'lpalmer','lpalmer','','0000-00-00 00:00:00','lpalmer@shiftnc.org','$2a$13$FrPxATxiqPY54wgkoLm1M.klS9oMkAUNgNTQy6u/.MA8tzfXSNr/G',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(534,'nsecworkshop10','nsecworkshop10','','0000-00-00 00:00:00','nsecworkshop10@gmail.com','$2a$13$fCXmyBhbADx1EfFace/OZu0ZYVTx.7tobeXQughB6qt77cqIv.6Gi',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(535,'dadan','dadan','','0000-00-00 00:00:00','dadan@chshel.org','$2a$13$tK7AdbEDbJ0R0Ok5Jz3vquVc2Xk.NIn6k13RODy0pRhxuSt2QS1IW',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(536,'lvaliente','lvaliente','','0000-00-00 00:00:00','lvaliente@chshel.org','$2a$13$d9fuseoNsHv57SDCUpSLoOT.NqfWIpsR7uUkDgfE1Hba2NTYUX1dy',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(537,'farispe','farispe','','0000-00-00 00:00:00','farispe@chshel.org','$2a$13$JKG8YJbVcgc4SsbSLRs65OXZIne8GciNI.0fhscO/7qZR80tuWYuS',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(538,'ginger_villa','ginger_villa','','0000-00-00 00:00:00','ginger_villa@yahoo.com','$2a$13$/b3BfaMVok0DJBcv1l5nmuTkGho6SsRgeIjD1UyYdMI.zGhjHFaBS',0,1,'','',NULL,NULL,0,NULL,'','','2016-04-14 21:25:28','2016-04-14 21:25:28',107,NULL),(539,'kiarra montgomery','kiarra','montgomery','2016-06-02 19:51:44','kiarra.montgomery@outlook.com','$2y$10$uqnGojFXh7/KjuXJBP3Nwe1nk/wXP9L7U0jucbNJIz59k/aM/J/wO',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-15 01:14:55','2016-06-02 23:51:44',NULL,NULL),(540,'laikenbrown laikenbrown','laikenbrown','laikenbrown','2016-06-02 19:51:57','laikenbrown@ymail.com','$2y$10$2nHZ/usFeP3Q0934aJO9QuwJcdrogA8Y0jRQpTXkUNqrUCkbEygPC',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-15 01:14:59','2016-06-02 23:51:57',NULL,NULL),(541,'jaleenathomas jaleenathomas','jaleenathomas','jaleenathomas','2016-06-02 19:52:49','jaleenathomas@yahoo.com','$2y$10$/rSbV8IEjktSx7XTX5Hf5OmaisTqiypuQRiLgZ0r6KnnqWXJ0nnPS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-15 01:15:02','2016-06-02 23:52:49',NULL,NULL),(542,'thomastahjale thomastahjale','thomastahjale','thomastahjale','2016-05-19 21:01:04','thomastahjale@gmail.com','$2y$10$oVK39PokMzSbU.3UcfaZNujQh20HCOKPsjgL3SmP3LKVuOnEJt.le',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-15 01:15:14','2016-05-20 01:01:04',NULL,NULL),(543,'nelcia mcclain','nelcia','mcclain','2016-05-12 20:45:19','nelcia.mcclain@gmail.com','$2y$10$X7uwl2k5SzvVIT1T2Y26nuyLIGUNR.cLNtPY806i5ojSJ8n/cybL2',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-15 01:15:30','2016-05-13 00:45:19',NULL,NULL),(544,'tinab2104 tinab2104','tinab2104','tinab2104','2016-06-02 19:52:44','tinab2104@gmail.com','$2y$10$NBK89pDbPWlDPVV9AzLFkeAex1cvKx29ZKoUDSAspFrE/IoumLq.C',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-15 01:15:40','2016-06-02 23:52:44',NULL,NULL),(545,'test test','test','test','2016-06-17 13:47:24','test@gmail.com','$2y$10$ieZiKJAfVqLXcmLaNKI1Le94UWmPoIw65zbhvmAJD9RSM0dJgoFam',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-18 19:05:19','2016-06-17 17:47:24',NULL,NULL),(546,'serwah_18 serwah_18','serwah_18','serwah_18','2016-04-20 11:08:08','serwah_18@yahoo.com','$2y$10$G9TjiLjNxO2QiKw9fgzfPeBDb8HC/lHHuF05EnJ/qKgDrWb2/0NiK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-20 15:08:08','2016-04-20 15:08:08',NULL,NULL),(547,'indiasims1300 indiasims1300','indiasims1300','indiasims1300','2016-06-02 19:51:17','indiasims1300@yahoo.com','$2y$10$83RAO8b/IfXevedzNDpcMeSffnHX4hvrxuvzN/jyI8uJrawv.smzu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-22 00:54:36','2016-06-02 23:51:17',NULL,NULL),(548,'flatteringrose6506 flatteringrose6506','flatteringrose6506','flatteringrose6506','2016-05-23 21:51:26','flatteringrose6506@gmail.com','$2y$10$oDXUMwovUecS5jTQBV7qM.Wl/jPePd.9gDF2Av7gb5tX6fS9a1SDy',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-26 01:40:46','2016-05-24 01:51:26',NULL,NULL),(549,'bertix bertix','bertix','bertix','2016-04-27 19:24:07','bertix@cmu.edu','$2y$10$.u/QDv9bN0ut4GOLAAroiuyuAtdU9LOLeZme9BroTAJo9Yzq8n8fa',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-26 23:49:36','2016-04-27 23:24:07',NULL,NULL),(550,'maganjordan13 maganjordan13','maganjordan13','maganjordan13','2016-04-27 19:18:03','maganjordan13@gmail.com','$2y$10$bhTEV7M0Encgi7HYTBlxDO8b.VH/dk0YuXZoTf38cGIq8BAjbhML.',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-04-27 23:18:02','2016-04-27 23:18:03',NULL,NULL),(551,'destyinyjames33 destyinyjames33','destyinyjames33','destyinyjames33','2016-05-02 21:58:10','destyinyjames33@yahoo.com','$2y$10$QOhOloajGEuv7K9LP6jN4.FQ/5ufAsVwjZfYgLvLkXS5tsU825W56',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-03 01:58:10','2016-05-03 01:58:10',253,NULL),(552,'jrusley1 jrusley1','jrusley1','jrusley1','2016-05-06 16:04:35','jrusley1@jhmi.edu','$2y$10$aew554PcW.b3qWPdeyp15Omuw9jDttKsugmAGnUhLwpvnzYbEgbEK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-06 20:04:35','2016-05-06 20:04:35',NULL,NULL),(553,'magdalenasaldana32 magdalenasaldana32','magdalenasaldana32','magdalenasaldana32','2016-05-09 23:16:51','magdalenasaldana32@yahoo.com','$2y$10$9P7lPKEh1LFqYR4D4QizQO6duJAbZi8QJW49bXPBu9xutV23sq9mu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-09 22:49:59','2016-05-10 03:16:51',NULL,NULL),(554,'arikagunther3 arikagunther3','arikagunther3','arikagunther3','2016-05-09 23:29:24','arikagunther3@gmail.com','$2y$10$f9FFEwg4v3Cqft3GoUQwXewZ2q8vTE/.zyfB9BHERdO721cUkyK7u',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-10 00:25:07','2016-05-10 03:29:24',NULL,NULL),(555,'ellen ellen','ellen','ellen','2016-05-09 22:13:17','ellen@roomone.org','$2y$10$G4oy7zZlNB61zaDEysgPUeoXBTRmgF1yOjZYj4HbjdSMIytWkmWFu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-10 02:13:17','2016-05-10 02:13:17',NULL,NULL),(556,'arikagunther arikagunther','arikagunther','arikagunther','2016-05-10 16:33:13','arikagunther@nvhealthcenters.org','$2y$10$cSUr3T3M798QFj6EDEP8yOi1pW1uiwSdMymxhaMbn47aRtbW/mBKW',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-10 03:45:05','2016-05-10 20:33:13',NULL,NULL),(558,'msaldana msaldana','msaldana','msaldana','2016-05-09 23:45:45','msaldana@nvhealthcenters.org','$2y$10$i1ZKGktCzq/KmixBn4keU.qtaBLIsa4NL2JhuROPCXcSojoJcH4rS',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-10 03:45:45','2016-05-10 03:45:45',553,NULL),(560,'mills knm','mills','knm','2016-05-10 01:40:43','mills.knm@gmail.com','$2y$10$JK7WjKQ1P9Ei6zCRrIHVeuU1FIA7FmhvH58.GmpHOIBSsw4hTmh92',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-10 05:40:43','2016-05-10 05:40:43',NULL,NULL),(561,'test01 test01','test01','test01','2016-05-12 03:56:05','test01@gmail.com','$2y$10$DnzEQpM0KgM3IHtgT7a/ieXVYY4oF5DD.bWZSScK/ovGxaLqbtky2',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-12 07:54:03','2016-05-12 07:56:05',NULL,NULL),(562,'jeremywells jeremywells','jeremywells','jeremywells','2016-06-14 16:10:40','jeremywells@openarc.net','$2y$10$DIimFVGITSqv8gCzLK3p5ukKV0smuVcb7EPXJFVB818/BwhqPebKe',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-13 22:55:56','2016-06-14 20:10:40',NULL,NULL),(563,'holbrook holbrook','holbrook','holbrook','2016-05-17 18:15:16','holbrook@andrew.cmu.edu','$2y$10$LQBo7anvqP7LITc5GJzEYeqchbGa2aT3wkyrY2ipFnfSQRha66y6q',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-17 21:58:49','2016-05-17 22:15:16',NULL,NULL),(564,'mgbarnes mgbarnes','mgbarnes','mgbarnes','2016-05-23 12:58:51','mgbarnes@andrew.cmu.edu','$2y$10$cw655GYnbtpLSGYpEkJUfufTJegZkux/fgO9Zn4dClyklte6Vt/ae',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-17 22:17:07','2016-05-23 16:58:51',NULL,NULL),(565,'jmw jeremymwells','jmw','jeremymwells','2016-05-19 02:03:26','jmw.jeremymwells@gmail.com','$2y$10$3r7/sIH6bTyH66dB3HzYKefoGI15lP4hIDW9zN4JShHHKeWIYSp/C',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-19 06:02:25','2016-05-19 06:03:26',NULL,NULL),(566,'thomas nakrosis','thomas','nakrosis','2016-06-03 16:32:28','thomas.nakrosis@yahoo.com','$2y$10$lEPbqTmin3Ve5PnpyQZZnuGBferG.uawEIraow1bsUZB3Fw/HCXaO',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-24 19:19:02','2016-06-03 20:32:28',NULL,NULL),(567,'ryanmilstead2 ryanmilstead2','ryanmilstead2','ryanmilstead2','2016-05-26 17:20:12','ryanmilstead2@gmail.com','$2y$10$RqF2bpb3Z0X3T9pGmhnQcOYxCm3ACgpDXdHVda24vF9op6Lo9hUvG',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-05-26 21:20:12','2016-05-26 21:20:12',NULL,NULL),(568,'stevengu stevengu','stevengu','stevengu','2016-06-06 17:42:43','stevengu@andrew.cmu.edu','$2y$10$HDSPDVdXnvbO1lo3b9I/p.DTp1INS43llwR2aQ4Xs79sXPxeT1KZi',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-02 18:21:25','2016-06-06 21:42:43',NULL,NULL),(569,'mcphailpc mcphailpc','mcphailpc','mcphailpc','2016-06-06 18:59:01','mcphailpc@wofford.edu','$2y$10$DFFx10B6BhpSnmUYiuroYurQfcmZV4.2KdC330oeF1JB3yg98A8K.',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-06 22:59:00','2016-06-06 22:59:01',NULL,NULL),(570,'nielsenbc nielsenbc','nielsenbc','nielsenbc','2016-06-06 19:05:32','nielsenbc@email.wofford.edu','$2y$10$.3j.4NfPUWSC3epGkeTnVOJTAUvZOpc0JGL.1zILGBxpMSo0cnNdu',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-06 23:04:18','2016-06-06 23:05:32',NULL,NULL),(571,'pollyedwardspadgett pollyedwardspadgett','pollyedwardspadgett','pollyedwardspadgett','2016-06-14 17:06:44','pollyedwardspadgett@gmail.com','$2y$10$lZjFRdkjCWMd/1ofN.bnsOOktR4KCT3PpzqEO1rSKHvWynX77WlT6',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-06 23:13:52','2016-06-14 21:06:44',NULL,NULL),(573,'downs-test downs-test','downs-test','downs-test','2016-06-06 19:16:53','downs-test@cmu.edu','$2y$10$Wh2afzO3VPH7LYrBdMhJd.jeSUGJIGVPCXAyqFZESsGpck.JckYF.',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-06 23:16:52','2016-06-06 23:16:53',NULL,NULL),(574,'ledfordel ledfordel','ledfordel','ledfordel','2016-06-09 15:40:32','ledfordel@email.wofford.edu','$2y$10$CM6LjD6CFDiKbFcOu5ujnOZD4l1nW5WRMsNFuPSwBYIKCIBvsHFr.',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-07 00:02:54','2016-06-09 19:40:32',NULL,NULL),(575,'parnellaa parnellaa','parnellaa','parnellaa','2016-06-09 13:17:40','parnellaa@email.wofford.edu','$2y$10$JyOMRVMOhP0VNKHKfqRe4e/sP/NrcICwgLCo7VYUgkhUjscETSt.O',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-07 00:17:56','2016-06-09 17:17:40',NULL,NULL),(576,'jasperw jasperw','jasperw','jasperw','2016-06-06 22:02:19','jasperw@andrew.cmu.edu','$2y$10$UzRUoSULKr6nhDcEatyob.wyBzXxLPU4wgu6VG48SHPo3zRzKuQy6',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-07 02:02:19','2016-06-07 02:02:19',NULL,NULL),(577,'cierranicholekaiser cierranicholekaiser','cierranicholekaiser','cierranicholekaiser','2016-06-08 00:08:19','cierranicholekaiser@gmail.com','$2y$10$oCR6aq6xMwr2RojD1W87M.rrMEqIoj/gwsehXqG7ptGYju4YLby4y',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-07 06:59:53','2016-06-08 04:08:19',NULL,NULL),(578,'boydnc boydnc','boydnc','boydnc','2016-06-11 02:41:16','boydnc@email.wofford.edu','$2y$10$QXhk6ZZRmIRAooEbiSV5N.rgh3NDZ43Bg08kQTZM/aPx4NzboXCnK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-07 06:59:53','2016-06-11 06:41:16',NULL,NULL),(579,'jonescg jonescg','jonescg','jonescg','2016-06-09 16:41:02','jonescg@email.wofford.edu','$2y$10$xRq2DXAq6shXpPYjnnZvXuok6zVmsrpwrm2ztE553059ACWj7v.sK',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-09 20:10:28','2016-06-09 20:41:02',NULL,NULL),(580,'probertson1997 probertson1997','probertson1997','probertson1997','2016-06-14 00:15:13','probertson1997@gmail.com','$2y$10$9bB6wwGwVGofhmt6zYmV..DIiv9XRQBDSeFPnPRFV/xtjkGkjPZ5i',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-14 03:43:03','2016-06-14 04:15:13',NULL,NULL),(581,'johnsonta1 johnsonta1','johnsonta1','johnsonta1','2016-06-14 00:12:21','johnsonta1@email.wofford.edu','$2y$10$VSWfr/yVfXbQDhg1qPEix.nGJkwhVkE2Ccg3TyGW20R0pKJ2s7sku',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-14 03:52:14','2016-06-14 04:12:21',NULL,NULL),(582,'bryantdb bryantdb','bryantdb','bryantdb','2016-06-14 00:04:32','bryantdb@email.wofford.edu','$2y$10$gC5irXK2zCOfj/t0fDewR.84Z2leJWaDVfAWVP91YkoKusCbBbKZO',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-14 04:04:32','2016-06-14 04:04:32',NULL,NULL),(583,'morristm morristm','morristm','morristm','2016-06-14 00:10:12','morristm@email.wofford.edu','$2y$10$ps0fqlXrpumsLHND6fGRU.7XntjYH7Kok6Y6ml6CDTCeFmK..ZRWm',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-14 04:05:01','2016-06-14 04:10:12',NULL,NULL),(584,'rmccants50 rmccants50','rmccants50','rmccants50','2016-06-14 00:11:12','rmccants50@yahoo.com','$2y$10$BRcrKLBFIa9neR.gwdv6MOiiq3b.spZIY2bfBX1Hp37r0IVP2k9Ku',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-14 04:05:47','2016-06-14 04:11:12',NULL,NULL),(585,'aparler aparler','aparler','aparler','2016-06-14 00:06:41','aparler@gmail.com','$2y$10$tApX9KxCEC0P7XNC7Q52yuIJR7LqG7khTORf48OX/raAEwkxb22fm',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-14 04:06:41','2016-06-14 04:06:41',NULL,NULL),(586,'tylerv101 tylerv101','tylerv101','tylerv101','2016-06-14 00:14:55','tylerv101@gmail.com','$2y$10$zsGwxAOu4K3GteEVIKOn5OMF6J.S6DvCpMQtoje6GNfJj.8D32KUq',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-14 04:14:55','2016-06-14 04:14:55',582,NULL),(587,'michaelquach+44 michaelquach+44','michaelquach+44','michaelquach+44','2016-06-15 20:11:15','michaelquach+44@openarc.net','$2y$10$qXr5ZvhSFnUP/5TxD8PXWe6K9OmeycdJZlxZz0ltepyd5Crvx4Qj6',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-16 00:07:01','2016-06-16 00:11:15',NULL,NULL),(588,'joshw joshw','joshw','joshw','2016-06-20 18:03:11','joshw@openarc.net','$2y$10$Vic2Uz6gMp5hnSLmCucTDeEn6.PgBFFMWPik8auteR6RcpWZ3c9z2',0,1,NULL,NULL,NULL,'y',NULL,NULL,NULL,NULL,'2016-06-20 22:03:10','2016-06-20 22:03:11',NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_config`
--

DROP TABLE IF EXISTS `user_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_config` (
  `service_id` int(10) unsigned NOT NULL,
  `allow_open_registration` tinyint(1) NOT NULL DEFAULT '0',
  `open_reg_role_id` int(10) unsigned DEFAULT NULL,
  `open_reg_email_service_id` int(10) unsigned DEFAULT NULL,
  `open_reg_email_template_id` int(10) unsigned DEFAULT NULL,
  `invite_email_service_id` int(10) unsigned DEFAULT NULL,
  `invite_email_template_id` int(10) unsigned DEFAULT NULL,
  `password_email_service_id` int(10) unsigned DEFAULT NULL,
  `password_email_template_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`service_id`),
  CONSTRAINT `user_config_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_config`
--

LOCK TABLES `user_config` WRITE;
/*!40000 ALTER TABLE `user_config` DISABLE KEYS */;
INSERT INTO `user_config` VALUES (6,1,1,NULL,2,5,1,11,3);
/*!40000 ALTER TABLE `user_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_custom`
--

DROP TABLE IF EXISTS `user_custom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_custom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_custom_user_id_foreign` (`user_id`),
  KEY `user_custom_created_by_id_foreign` (`created_by_id`),
  KEY `user_custom_last_modified_by_id_foreign` (`last_modified_by_id`),
  CONSTRAINT `user_custom_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `user_custom_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `user_custom_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_custom`
--

LOCK TABLES `user_custom` WRITE;
/*!40000 ALTER TABLE `user_custom` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_custom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_lookup`
--

DROP TABLE IF EXISTS `user_lookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_lookup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned DEFAULT NULL,
  `last_modified_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_lookup_user_id_foreign` (`user_id`),
  KEY `user_lookup_created_by_id_foreign` (`created_by_id`),
  KEY `user_lookup_last_modified_by_id_foreign` (`last_modified_by_id`),
  KEY `user_lookup_name_index` (`name`),
  CONSTRAINT `user_lookup_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `user_lookup_last_modified_by_id_foreign` FOREIGN KEY (`last_modified_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `user_lookup_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_lookup`
--

LOCK TABLES `user_lookup` WRITE;
/*!40000 ALTER TABLE `user_lookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_lookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_to_app_to_role`
--

DROP TABLE IF EXISTS `user_to_app_to_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_to_app_to_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `app_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_to_app_to_role_user_id_foreign` (`user_id`),
  KEY `user_to_app_to_role_app_id_foreign` (`app_id`),
  KEY `user_to_app_to_role_role_id_foreign` (`role_id`),
  CONSTRAINT `user_to_app_to_role_app_id_foreign` FOREIGN KEY (`app_id`) REFERENCES `app` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_to_app_to_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_to_app_to_role_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1436 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_to_app_to_role`
--

LOCK TABLES `user_to_app_to_role` WRITE;
/*!40000 ALTER TABLE `user_to_app_to_role` DISABLE KEYS */;
INSERT INTO `user_to_app_to_role` VALUES (16,10,1,1),(17,10,2,1),(18,10,3,1),(19,10,4,1),(20,10,6,1),(26,12,1,1),(27,12,2,1),(28,12,3,1),(29,12,4,1),(30,12,6,1),(31,13,1,1),(32,13,2,1),(33,13,3,1),(34,13,4,1),(35,13,6,1),(36,14,1,1),(37,14,2,1),(38,14,3,1),(39,14,4,1),(40,14,6,1),(41,15,1,1),(42,15,2,1),(43,15,3,1),(44,15,4,1),(45,15,6,1),(46,16,1,1),(47,16,2,1),(48,16,3,1),(49,16,4,1),(50,16,6,1),(51,17,1,1),(52,17,2,1),(53,17,3,1),(54,17,4,1),(55,17,6,1),(56,18,1,1),(57,18,2,1),(58,18,3,1),(59,18,4,1),(60,18,6,1),(61,19,1,1),(62,19,2,1),(63,19,3,1),(64,19,4,1),(65,19,6,1),(66,20,1,1),(67,20,2,1),(68,20,3,1),(69,20,4,1),(70,20,6,1),(71,21,1,1),(72,21,2,1),(73,21,3,1),(74,21,4,1),(75,21,6,1),(86,24,1,1),(87,24,2,1),(88,24,3,1),(89,24,4,1),(90,24,6,1),(91,25,1,1),(92,25,2,1),(93,25,3,1),(94,25,4,1),(95,25,6,1),(96,26,1,1),(97,26,2,1),(98,26,3,1),(99,26,4,1),(100,26,6,1),(101,27,1,1),(102,27,2,1),(103,27,3,1),(104,27,4,1),(105,27,6,1),(106,28,1,1),(107,28,2,1),(108,28,3,1),(109,28,4,1),(110,28,6,1),(111,29,1,1),(112,29,2,1),(113,29,3,1),(114,29,4,1),(115,29,6,1),(116,30,1,1),(117,30,2,1),(118,30,3,1),(119,30,4,1),(120,30,6,1),(121,31,1,1),(122,31,2,1),(123,31,3,1),(124,31,4,1),(125,31,6,1),(126,32,1,1),(127,32,2,1),(128,32,3,1),(129,32,4,1),(130,32,6,1),(171,41,1,1),(172,41,2,1),(173,41,3,1),(174,41,4,1),(175,41,6,1),(176,42,1,1),(177,42,2,1),(178,42,3,1),(179,42,4,1),(180,42,6,1),(181,43,1,1),(182,43,2,1),(183,43,3,1),(184,43,4,1),(185,43,6,1),(186,44,1,1),(187,44,2,1),(188,44,3,1),(189,44,4,1),(190,44,6,1),(201,47,1,1),(202,47,2,1),(203,47,3,1),(204,47,4,1),(205,47,6,1),(246,56,1,1),(247,56,2,1),(248,56,3,1),(249,56,4,1),(250,56,6,1),(251,57,1,1),(252,57,2,1),(253,57,3,1),(254,57,4,1),(255,57,6,1),(256,58,1,1),(257,58,2,1),(258,58,3,1),(259,58,4,1),(260,58,6,1),(266,60,1,1),(267,60,2,1),(268,60,3,1),(269,60,4,1),(270,60,6,1),(281,63,1,1),(282,63,2,1),(283,63,3,1),(284,63,4,1),(285,63,6,1),(291,65,1,1),(292,65,2,1),(293,65,3,1),(294,65,4,1),(295,65,6,1),(296,66,1,1),(297,66,2,1),(298,66,3,1),(299,66,4,1),(300,66,6,1),(301,67,1,1),(302,67,2,1),(303,67,3,1),(304,67,4,1),(305,67,6,1),(311,69,1,1),(312,69,2,1),(313,69,3,1),(314,69,4,1),(315,69,6,1),(331,73,1,1),(332,73,2,1),(333,73,3,1),(334,73,4,1),(335,73,6,1),(336,74,1,1),(337,74,2,1),(338,74,3,1),(339,74,4,1),(340,74,6,1),(346,76,1,1),(347,76,2,1),(348,76,3,1),(349,76,4,1),(350,76,6,1),(351,77,1,1),(352,77,2,1),(353,77,3,1),(354,77,4,1),(355,77,6,1),(376,82,1,1),(377,82,2,1),(378,82,3,1),(379,82,4,1),(380,82,6,1),(381,83,1,1),(382,83,2,1),(383,83,3,1),(384,83,4,1),(385,83,6,1),(386,84,1,1),(387,84,2,1),(388,84,3,1),(389,84,4,1),(390,84,6,1),(391,85,1,1),(392,85,2,1),(393,85,3,1),(394,85,4,1),(395,85,6,1),(396,86,1,1),(397,86,2,1),(398,86,3,1),(399,86,4,1),(400,86,6,1),(411,89,1,1),(412,89,2,1),(413,89,3,1),(414,89,4,1),(415,89,6,1),(416,90,1,1),(417,90,2,1),(418,90,3,1),(419,90,4,1),(420,90,6,1),(421,91,1,1),(422,91,2,1),(423,91,3,1),(424,91,4,1),(425,91,6,1),(426,92,1,1),(427,92,2,1),(428,92,3,1),(429,92,4,1),(430,92,6,1),(431,93,1,1),(432,93,2,1),(433,93,3,1),(434,93,4,1),(435,93,6,1),(436,94,1,1),(437,94,2,1),(438,94,3,1),(439,94,4,1),(440,94,6,1),(441,95,1,1),(442,95,2,1),(443,95,3,1),(444,95,4,1),(445,95,6,1),(446,96,1,1),(447,96,2,1),(448,96,3,1),(449,96,4,1),(450,96,6,1),(451,97,1,1),(452,97,2,1),(453,97,3,1),(454,97,4,1),(455,97,6,1),(456,98,1,1),(457,98,2,1),(458,98,3,1),(459,98,4,1),(460,98,6,1),(461,99,1,1),(462,99,2,1),(463,99,3,1),(464,99,4,1),(465,99,6,1),(466,100,1,1),(467,100,2,1),(468,100,3,1),(469,100,4,1),(470,100,6,1),(476,102,1,1),(477,102,2,1),(478,102,3,1),(479,102,4,1),(480,102,6,1),(486,104,1,1),(487,104,2,1),(488,104,3,1),(489,104,4,1),(490,104,6,1),(491,105,1,1),(492,105,2,1),(493,105,3,1),(494,105,4,1),(495,105,6,1),(496,106,1,1),(497,106,2,1),(498,106,3,1),(499,106,4,1),(500,106,6,1),(501,108,1,1),(502,108,2,1),(503,108,3,1),(504,108,4,1),(505,108,6,1),(506,109,1,1),(507,109,2,1),(508,109,3,1),(509,109,4,1),(510,109,6,1),(511,110,1,1),(512,110,2,1),(513,110,3,1),(514,110,4,1),(515,110,6,1),(516,111,1,1),(517,111,2,1),(518,111,3,1),(519,111,4,1),(520,111,6,1),(521,112,1,1),(522,112,2,1),(523,112,3,1),(524,112,4,1),(525,112,6,1),(526,113,1,1),(527,113,2,1),(528,113,3,1),(529,113,4,1),(530,113,6,1),(531,114,1,1),(532,114,2,1),(533,114,3,1),(534,114,4,1),(535,114,6,1),(536,115,1,1),(537,115,2,1),(538,115,3,1),(539,115,4,1),(540,115,6,1),(541,116,1,1),(542,116,2,1),(543,116,3,1),(544,116,4,1),(545,116,6,1),(546,117,1,1),(547,117,2,1),(548,117,3,1),(549,117,4,1),(550,117,6,1),(551,118,1,1),(552,118,2,1),(553,118,3,1),(554,118,4,1),(555,118,6,1),(556,119,1,1),(557,119,2,1),(558,119,3,1),(559,119,4,1),(560,119,6,1),(561,120,1,1),(562,120,2,1),(563,120,3,1),(564,120,4,1),(565,120,6,1),(566,121,1,1),(567,121,2,1),(568,121,3,1),(569,121,4,1),(570,121,6,1),(571,122,1,1),(572,122,2,1),(573,122,3,1),(574,122,4,1),(575,122,6,1),(576,123,1,1),(577,123,2,1),(578,123,3,1),(579,123,4,1),(580,123,6,1),(581,124,1,1),(582,124,2,1),(583,124,3,1),(584,124,4,1),(585,124,6,1),(586,125,1,1),(587,125,2,1),(588,125,3,1),(589,125,4,1),(590,125,6,1),(591,126,1,1),(592,126,2,1),(593,126,3,1),(594,126,4,1),(595,126,6,1),(596,127,1,1),(597,127,2,1),(598,127,3,1),(599,127,4,1),(600,127,6,1),(601,128,1,1),(602,128,2,1),(603,128,3,1),(604,128,4,1),(605,128,6,1),(606,129,1,1),(607,129,2,1),(608,129,3,1),(609,129,4,1),(610,129,6,1),(611,130,1,1),(612,130,2,1),(613,130,3,1),(614,130,4,1),(615,130,6,1),(616,131,1,1),(617,131,2,1),(618,131,3,1),(619,131,4,1),(620,131,6,1),(621,132,1,1),(622,132,2,1),(623,132,3,1),(624,132,4,1),(625,132,6,1),(626,133,1,1),(627,133,2,1),(628,133,3,1),(629,133,4,1),(630,133,6,1),(631,134,1,1),(632,134,2,1),(633,134,3,1),(634,134,4,1),(635,134,6,1),(636,135,1,1),(637,135,2,1),(638,135,3,1),(639,135,4,1),(640,135,6,1),(641,136,1,1),(642,136,2,1),(643,136,3,1),(644,136,4,1),(645,136,6,1),(646,137,1,1),(647,137,2,1),(648,137,3,1),(649,137,4,1),(650,137,6,1),(651,138,1,1),(652,138,2,1),(653,138,3,1),(654,138,4,1),(655,138,6,1),(656,139,1,1),(657,139,2,1),(658,139,3,1),(659,139,4,1),(660,139,6,1),(661,140,1,1),(662,140,2,1),(663,140,3,1),(664,140,4,1),(665,140,6,1),(666,141,1,1),(667,141,2,1),(668,141,3,1),(669,141,4,1),(670,141,6,1),(671,142,1,1),(672,142,2,1),(673,142,3,1),(674,142,4,1),(675,142,6,1),(676,143,1,1),(677,143,2,1),(678,143,3,1),(679,143,4,1),(680,143,6,1),(681,144,1,1),(682,144,2,1),(683,144,3,1),(684,144,4,1),(685,144,6,1),(686,145,1,1),(687,145,2,1),(688,145,3,1),(689,145,4,1),(690,145,6,1),(691,146,1,1),(692,146,2,1),(693,146,3,1),(694,146,4,1),(695,146,6,1),(696,147,1,1),(697,147,2,1),(698,147,3,1),(699,147,4,1),(700,147,6,1),(701,148,1,1),(702,148,2,1),(703,148,3,1),(704,148,4,1),(705,148,6,1),(706,149,1,1),(707,149,2,1),(708,149,3,1),(709,149,4,1),(710,149,6,1),(711,150,1,1),(712,150,2,1),(713,150,3,1),(714,150,4,1),(715,150,6,1),(716,151,1,1),(717,151,2,1),(718,151,3,1),(719,151,4,1),(720,151,6,1),(721,152,1,1),(722,152,2,1),(723,152,3,1),(724,152,4,1),(725,152,6,1),(726,153,1,1),(727,153,2,1),(728,153,3,1),(729,153,4,1),(730,153,6,1),(731,154,1,1),(732,154,2,1),(733,154,3,1),(734,154,4,1),(735,154,6,1),(736,155,1,1),(737,155,2,1),(738,155,3,1),(739,155,4,1),(740,155,6,1),(741,156,1,1),(742,156,2,1),(743,156,3,1),(744,156,4,1),(745,156,6,1),(746,157,1,1),(747,157,2,1),(748,157,3,1),(749,157,4,1),(750,157,6,1),(751,158,1,1),(752,158,2,1),(753,158,3,1),(754,158,4,1),(755,158,6,1),(756,159,1,1),(757,159,2,1),(758,159,3,1),(759,159,4,1),(760,159,6,1),(761,160,1,1),(762,160,2,1),(763,160,3,1),(764,160,4,1),(765,160,6,1),(766,161,1,1),(767,161,2,1),(768,161,3,1),(769,161,4,1),(770,161,6,1),(771,162,1,1),(772,162,2,1),(773,162,3,1),(774,162,4,1),(775,162,6,1),(776,163,1,1),(777,163,2,1),(778,163,3,1),(779,163,4,1),(780,163,6,1),(781,164,1,1),(782,164,2,1),(783,164,3,1),(784,164,4,1),(785,164,6,1),(786,165,1,1),(787,165,2,1),(788,165,3,1),(789,165,4,1),(790,165,6,1),(791,166,1,1),(792,166,2,1),(793,166,3,1),(794,166,4,1),(795,166,6,1),(796,167,1,1),(797,167,2,1),(798,167,3,1),(799,167,4,1),(800,167,6,1),(801,168,1,1),(802,168,2,1),(803,168,3,1),(804,168,4,1),(805,168,6,1),(806,169,1,1),(807,169,2,1),(808,169,3,1),(809,169,4,1),(810,169,6,1),(811,170,1,1),(812,170,2,1),(813,170,3,1),(814,170,4,1),(815,170,6,1),(816,171,1,1),(817,171,2,1),(818,171,3,1),(819,171,4,1),(820,171,6,1),(821,172,1,1),(822,172,2,1),(823,172,3,1),(824,172,4,1),(825,172,6,1),(826,173,1,1),(827,173,2,1),(828,173,3,1),(829,173,4,1),(830,173,6,1),(831,174,1,1),(832,174,2,1),(833,174,3,1),(834,174,4,1),(835,174,6,1),(836,175,1,1),(837,175,2,1),(838,175,3,1),(839,175,4,1),(840,175,6,1),(841,176,1,1),(842,176,2,1),(843,176,3,1),(844,176,4,1),(845,176,6,1),(846,177,1,1),(847,177,2,1),(848,177,3,1),(849,177,4,1),(850,177,6,1),(851,178,1,1),(852,178,2,1),(853,178,3,1),(854,178,4,1),(855,178,6,1),(856,179,1,1),(857,179,2,1),(858,179,3,1),(859,179,4,1),(860,179,6,1),(861,180,1,1),(862,180,2,1),(863,180,3,1),(864,180,4,1),(865,180,6,1),(866,181,1,1),(867,181,2,1),(868,181,3,1),(869,181,4,1),(870,181,6,1),(871,182,1,1),(872,182,2,1),(873,182,3,1),(874,182,4,1),(875,182,6,1),(876,183,1,1),(877,183,2,1),(878,183,3,1),(879,183,4,1),(880,183,6,1),(881,184,1,1),(882,184,2,1),(883,184,3,1),(884,184,4,1),(885,184,6,1),(886,185,1,1),(887,185,2,1),(888,185,3,1),(889,185,4,1),(890,185,6,1),(891,186,1,1),(892,186,2,1),(893,186,3,1),(894,186,4,1),(895,186,6,1),(896,187,1,1),(897,187,2,1),(898,187,3,1),(899,187,4,1),(900,187,6,1),(901,188,1,1),(902,188,2,1),(903,188,3,1),(904,188,4,1),(905,188,6,1),(906,189,1,1),(907,189,2,1),(908,189,3,1),(909,189,4,1),(910,189,6,1),(911,190,1,1),(912,190,2,1),(913,190,3,1),(914,190,4,1),(915,190,6,1),(916,191,1,1),(917,191,2,1),(918,191,3,1),(919,191,4,1),(920,191,6,1),(921,192,1,1),(922,192,2,1),(923,192,3,1),(924,192,4,1),(925,192,6,1),(926,193,1,1),(927,193,2,1),(928,193,3,1),(929,193,4,1),(930,193,6,1),(931,194,1,1),(932,194,2,1),(933,194,3,1),(934,194,4,1),(935,194,6,1),(936,195,1,1),(937,195,2,1),(938,195,3,1),(939,195,4,1),(940,195,6,1),(941,197,1,1),(942,197,2,1),(943,197,3,1),(944,197,4,1),(945,197,6,1),(946,198,1,1),(947,198,2,1),(948,198,3,1),(949,198,4,1),(950,198,6,1),(951,199,1,1),(952,199,2,1),(953,199,3,1),(954,199,4,1),(955,199,6,1),(956,201,1,1),(957,201,2,1),(958,201,3,1),(959,201,4,1),(960,201,6,1),(961,202,1,1),(962,202,2,1),(963,202,3,1),(964,202,4,1),(965,202,6,1),(966,203,1,1),(967,203,2,1),(968,203,3,1),(969,203,4,1),(970,203,6,1),(971,204,1,1),(972,204,2,1),(973,204,3,1),(974,204,4,1),(975,204,6,1),(976,205,1,1),(977,205,2,1),(978,205,3,1),(979,205,4,1),(980,205,6,1),(981,206,1,1),(982,206,2,1),(983,206,3,1),(984,206,4,1),(985,206,6,1),(986,207,1,1),(987,207,2,1),(988,207,3,1),(989,207,4,1),(990,207,6,1),(991,208,1,1),(992,208,2,1),(993,208,3,1),(994,208,4,1),(995,208,6,1),(996,209,1,1),(997,209,2,1),(998,209,3,1),(999,209,4,1),(1000,209,6,1),(1001,210,1,1),(1002,210,2,1),(1003,210,3,1),(1004,210,4,1),(1005,210,6,1),(1006,211,1,1),(1007,211,2,1),(1008,211,3,1),(1009,211,4,1),(1010,211,6,1),(1011,212,1,1),(1012,212,2,1),(1013,212,3,1),(1014,212,4,1),(1015,212,6,1),(1016,213,1,1),(1017,213,2,1),(1018,213,3,1),(1019,213,4,1),(1020,213,6,1),(1021,219,1,1),(1022,219,2,1),(1023,219,3,1),(1024,219,4,1),(1025,219,6,1),(1026,220,1,1),(1027,220,2,1),(1028,220,3,1),(1029,220,4,1),(1030,220,6,1),(1031,221,1,1),(1032,221,2,1),(1033,221,3,1),(1034,221,4,1),(1035,221,6,1),(1036,222,1,1),(1037,222,2,1),(1038,222,3,1),(1039,222,4,1),(1040,222,6,1),(1041,223,1,1),(1042,223,2,1),(1043,223,3,1),(1044,223,4,1),(1045,223,6,1),(1046,224,1,1),(1047,224,2,1),(1048,224,3,1),(1049,224,4,1),(1050,224,6,1),(1051,225,1,1),(1052,225,2,1),(1053,225,3,1),(1054,225,4,1),(1055,225,6,1),(1056,226,1,1),(1057,226,2,1),(1058,226,3,1),(1059,226,4,1),(1060,226,6,1),(1061,227,1,1),(1062,227,2,1),(1063,227,3,1),(1064,227,4,1),(1065,227,6,1),(1066,228,1,1),(1067,228,2,1),(1068,228,3,1),(1069,228,4,1),(1070,228,6,1),(1071,229,1,1),(1072,229,2,1),(1073,229,3,1),(1074,229,4,1),(1075,229,6,1),(1076,230,1,1),(1077,230,2,1),(1078,230,3,1),(1079,230,4,1),(1080,230,6,1),(1081,231,1,1),(1082,231,2,1),(1083,231,3,1),(1084,231,4,1),(1085,231,6,1),(1086,232,1,1),(1087,232,2,1),(1088,232,3,1),(1089,232,4,1),(1090,232,6,1),(1091,233,1,1),(1092,233,2,1),(1093,233,3,1),(1094,233,4,1),(1095,233,6,1),(1096,234,1,1),(1097,234,2,1),(1098,234,3,1),(1099,234,4,1),(1100,234,6,1),(1101,235,1,1),(1102,235,2,1),(1103,235,3,1),(1104,235,4,1),(1105,235,6,1),(1106,236,1,1),(1107,236,2,1),(1108,236,3,1),(1109,236,4,1),(1110,236,6,1),(1111,237,1,1),(1112,237,2,1),(1113,237,3,1),(1114,237,4,1),(1115,237,6,1),(1116,238,1,1),(1117,238,2,1),(1118,238,3,1),(1119,238,4,1),(1120,238,6,1),(1121,239,1,1),(1122,239,2,1),(1123,239,3,1),(1124,239,4,1),(1125,239,6,1),(1126,240,1,1),(1127,240,2,1),(1128,240,3,1),(1129,240,4,1),(1130,240,6,1),(1131,241,1,1),(1132,241,2,1),(1133,241,3,1),(1134,241,4,1),(1135,241,6,1),(1136,242,1,1),(1137,242,2,1),(1138,242,3,1),(1139,242,4,1),(1140,242,6,1),(1141,243,1,1),(1142,243,2,1),(1143,243,3,1),(1144,243,4,1),(1145,243,6,1),(1146,244,1,1),(1147,244,2,1),(1148,244,3,1),(1149,244,4,1),(1150,244,6,1),(1151,245,1,1),(1152,245,2,1),(1153,245,3,1),(1154,245,4,1),(1155,245,6,1),(1156,246,1,1),(1157,246,2,1),(1158,246,3,1),(1159,246,4,1),(1160,246,6,1),(1161,247,1,1),(1162,247,2,1),(1163,247,3,1),(1164,247,4,1),(1165,247,6,1),(1166,248,1,1),(1167,248,2,1),(1168,248,3,1),(1169,248,4,1),(1170,248,6,1),(1171,249,1,1),(1172,249,2,1),(1173,249,3,1),(1174,249,4,1),(1175,249,6,1),(1176,250,1,1),(1177,250,2,1),(1178,250,3,1),(1179,250,4,1),(1180,250,6,1),(1181,251,1,1),(1182,251,2,1),(1183,251,3,1),(1184,251,4,1),(1185,251,6,1),(1186,252,1,1),(1187,252,2,1),(1188,252,3,1),(1189,252,4,1),(1190,252,6,1),(1191,253,1,1),(1192,253,2,1),(1193,253,3,1),(1194,253,4,1),(1195,253,6,1),(1196,254,1,1),(1197,254,2,1),(1198,254,3,1),(1199,254,4,1),(1200,254,6,1),(1201,539,1,1),(1202,539,2,1),(1203,539,3,1),(1204,539,4,1),(1205,539,6,1),(1206,540,1,1),(1207,540,2,1),(1208,540,3,1),(1209,540,4,1),(1210,540,6,1),(1211,541,1,1),(1212,541,2,1),(1213,541,3,1),(1214,541,4,1),(1215,541,6,1),(1216,542,1,1),(1217,542,2,1),(1218,542,3,1),(1219,542,4,1),(1220,542,6,1),(1221,543,1,1),(1222,543,2,1),(1223,543,3,1),(1224,543,4,1),(1225,543,6,1),(1226,544,1,1),(1227,544,2,1),(1228,544,3,1),(1229,544,4,1),(1230,544,6,1),(1231,545,1,1),(1232,545,2,1),(1233,545,3,1),(1234,545,4,1),(1235,545,6,1),(1236,546,1,1),(1237,546,2,1),(1238,546,3,1),(1239,546,4,1),(1240,546,6,1),(1241,547,1,1),(1242,547,2,1),(1243,547,3,1),(1244,547,4,1),(1245,547,6,1),(1246,548,1,1),(1247,548,2,1),(1248,548,3,1),(1249,548,4,1),(1250,548,6,1),(1251,549,1,1),(1252,549,2,1),(1253,549,3,1),(1254,549,4,1),(1255,549,6,1),(1256,550,1,1),(1257,550,2,1),(1258,550,3,1),(1259,550,4,1),(1260,550,6,1),(1261,551,1,1),(1262,551,2,1),(1263,551,3,1),(1264,551,4,1),(1265,551,6,1),(1266,552,1,1),(1267,552,2,1),(1268,552,3,1),(1269,552,4,1),(1270,552,6,1),(1271,553,1,1),(1272,553,2,1),(1273,553,3,1),(1274,553,4,1),(1275,553,6,1),(1276,554,1,1),(1277,554,2,1),(1278,554,3,1),(1279,554,4,1),(1280,554,6,1),(1281,555,1,1),(1282,555,2,1),(1283,555,3,1),(1284,555,4,1),(1285,555,6,1),(1286,556,1,1),(1287,556,2,1),(1288,556,3,1),(1289,556,4,1),(1290,556,6,1),(1291,558,1,1),(1292,558,2,1),(1293,558,3,1),(1294,558,4,1),(1295,558,6,1),(1296,560,1,1),(1297,560,2,1),(1298,560,3,1),(1299,560,4,1),(1300,560,6,1),(1301,561,1,1),(1302,561,2,1),(1303,561,3,1),(1304,561,4,1),(1305,561,6,1),(1306,562,1,1),(1307,562,2,1),(1308,562,3,1),(1309,562,4,1),(1310,562,6,1),(1311,563,1,1),(1312,563,2,1),(1313,563,3,1),(1314,563,4,1),(1315,563,6,1),(1316,564,1,1),(1317,564,2,1),(1318,564,3,1),(1319,564,4,1),(1320,564,6,1),(1321,565,1,1),(1322,565,2,1),(1323,565,3,1),(1324,565,4,1),(1325,565,6,1),(1326,566,1,1),(1327,566,2,1),(1328,566,3,1),(1329,566,4,1),(1330,566,6,1),(1331,567,1,1),(1332,567,2,1),(1333,567,3,1),(1334,567,4,1),(1335,567,6,1),(1336,568,1,1),(1337,568,2,1),(1338,568,3,1),(1339,568,4,1),(1340,568,6,1),(1341,569,1,1),(1342,569,2,1),(1343,569,3,1),(1344,569,4,1),(1345,569,6,1),(1346,570,1,1),(1347,570,2,1),(1348,570,3,1),(1349,570,4,1),(1350,570,6,1),(1351,571,1,1),(1352,571,2,1),(1353,571,3,1),(1354,571,4,1),(1355,571,6,1),(1356,573,1,1),(1357,573,2,1),(1358,573,3,1),(1359,573,4,1),(1360,573,6,1),(1361,574,1,1),(1362,574,2,1),(1363,574,3,1),(1364,574,4,1),(1365,574,6,1),(1366,575,1,1),(1367,575,2,1),(1368,575,3,1),(1369,575,4,1),(1370,575,6,1),(1371,576,1,1),(1372,576,2,1),(1373,576,3,1),(1374,576,4,1),(1375,576,6,1),(1376,577,1,1),(1377,577,2,1),(1378,577,3,1),(1379,577,4,1),(1380,577,6,1),(1381,578,1,1),(1382,578,2,1),(1383,578,3,1),(1384,578,4,1),(1385,578,6,1),(1386,579,1,1),(1387,579,2,1),(1388,579,3,1),(1389,579,4,1),(1390,579,6,1),(1391,580,1,1),(1392,580,2,1),(1393,580,3,1),(1394,580,4,1),(1395,580,6,1),(1396,581,1,1),(1397,581,2,1),(1398,581,3,1),(1399,581,4,1),(1400,581,6,1),(1401,582,1,1),(1402,582,2,1),(1403,582,3,1),(1404,582,4,1),(1405,582,6,1),(1406,583,1,1),(1407,583,2,1),(1408,583,3,1),(1409,583,4,1),(1410,583,6,1),(1411,584,1,1),(1412,584,2,1),(1413,584,3,1),(1414,584,4,1),(1415,584,6,1),(1416,585,1,1),(1417,585,2,1),(1418,585,3,1),(1419,585,4,1),(1420,585,6,1),(1421,586,1,1),(1422,586,2,1),(1423,586,3,1),(1424,586,4,1),(1425,586,6,1),(1426,587,1,1),(1427,587,2,1),(1428,587,3,1),(1429,587,4,1),(1430,587,6,1),(1431,588,1,1),(1432,588,2,1),(1433,588,3,1),(1434,588,4,1),(1435,588,6,1);
/*!40000 ALTER TABLE `user_to_app_to_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-29  8:36:18
