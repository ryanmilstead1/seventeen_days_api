
namespace :environment do

  desc 'setup remote server deployment user'
  task :setup_deploy_user do
    # sudo ln -s /home/#{Figaro.env.deploy_user}
    # echo '#{Figaro.env.deploy_password}' | su - #{Figaro.env.deploy_user}
    # mkdir -p /home/#{Figaro.env.deploy_user}/.ssh
    # echo #{Figaro.env.deploy_key} >> /home/#{Figaro.env.deploy_user}/.ssh/id_rsa.pub
    # cat /home/#{Figaro.env.deploy_user}/.ssh/id_rsa.pub
    su = "sudo bash -c 'echo \'#{Figaro.env.deploy_password}\' | su - #{Figaro.env.deploy_user} -c"
    `ssh -T -i ./bin/seventeen_days_api.pem ubuntu@#{Figaro.env.web_host} <<EOF
    sudo apt-get update && sudo apt-get -y upgrade
    sudo useradd -d /home/#{Figaro.env.deploy_user} -m #{Figaro.env.deploy_user}
    echo 'deploy:#{Figaro.env.deploy_password}' | sudo chpasswd
    sudo bash -c 'echo "#{Figaro.env.deploy_user} ALL=(ALL:ALL) ALL" | (EDITOR="tee -a" visudo)'
   
    https://www.brightbox.com/blog/2016/01/06/ruby-2-3-ubuntu-packages/
    #{su} mkdir -p seventeen_days_api/shared/config'
    #{su} rvm install ruby
    #{su} rvm use ruby
    #{su} gem install bundler --no-ri --no-rdoc
    sudo apt-get install build-essential
    sudo echo #{Figaro.env.deploy_key} >> ~/.ssh/authorized_keys
    
    sudo apt-get install nginx-extras passenger
    sudo apt-get install libmysqlclient-dev
    apt-get install libsqlite3-dev
    sudo rm -rf /etc/nginx/sites-available/default

    `

  end



end