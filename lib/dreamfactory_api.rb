module DreamfactoryApi
  require 'net/http'

  @@root_url = SeventeenDaysApi::Application.config.dreamfactory_url
  @@dsp_application_name = SeventeenDaysApi::Application.config.dsp_application_name
  @@dsp_api_key = SeventeenDaysApi::Application.config.dsp_api_key
  @@auth_path = '/rest/user/session'

  def self.authenticate(user)
    return self.post(@@auth_path, user) if Rails.env.production?
    user
  end

  private 

  def self.post(path, payload)
    uri = URI.parse("#{@@root_url}#{path}")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri, initheader = { 'Content-Type' => 'application/json' })    
    request['X-DreamFactory-Application-Name'] = @@dsp_application_name
    request['X-DreamFactory-Api-Key'] = @@dsp_api_key
    request.body = payload.to_json
    http.request(request)
  end

end