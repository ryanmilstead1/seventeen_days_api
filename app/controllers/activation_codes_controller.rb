class ActivationCodesController < ApplicationController

  def show
    render json: get_activation_codes(params[:code], params[:user_id])
  end

  def create
    render json: activate_code_for_user(params[:code], params[:user_id])
  rescue => e
    render json: { exception: e }, status: 422
  end

  private

  def activate_code_for_user(code, user_id)
    code_obj = ActivationCode.find_by_code(code)

    if code_obj.nil?
      raise 'Activation Code does not exist'
    elsif code_obj.activated
      raise 'Activation Code is already in use'
    end

    code_obj.activate(user_id)
    code_obj
  end

  def get_activation_codes(code, user_id)
    if code.present? and user_id.present?
      return ActivationCode.where(user_id: user_id, code: code)
    elsif code.present?
      return ActivationCode.where(code: code)
    elsif user_id.present?
      return ActivationCode.where(user_id: user_id)
    end

    ActivationCode.all
  end

  # rescue_from ActivationCodesController::create' do |exception|
  #   pp '================================================foo foo============================'
  # end
end
